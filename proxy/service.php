<?php
require_once("../../../config.php");
require_once("$CFG->dirroot/local/badiunet/lib/response.php");
require_once("$CFG->dirroot/local/badiunet/lib/conn.php");;
class local_badiup_roxy_service {

    private $response = null;
   private $param = null;
    function __construct() {
        $this->response = new local_badiunet_response();
        $this->param=null;
    }

    public function shortnamerole() {
        
        
         if (!isset($this->param['userid'])) {$this->response->danied('badiu.moodle.proxy.error.param.userid.nodefined', 'Param userid is required');}
         if (empty($this->param['userid'])) {$this->response->danied('badiu.moodle.proxy.error.param.userid.empty', 'Param userid can not be null');}
        
         if (!isset($this->param['uri'])) {$this->response->danied('badiu.moodle.proxy.error.param.uri.nodefined', 'Param uri is required');}
         if (empty($this->param['uri'])) {$this->response->danied('badiu.moodle.proxy.error.param.uri.empty', 'Param uri can not be null');}
        
       $userid =$this->param['userid'];
        $uri =$this->param['uri'];

        $role = "";
        try {
            //is admin
            $isadmin = $this->is_admin($userid);

            if ($isadmin) {
                $role = "amdin";
                 $this->response->accept($role);
            }

            //course context
            $courseid = $this->get_courseid($uri);
            if (!empty($courseid) && $courseid > 1) {
                $role = $this->get_course_shortnamerole($userid, $courseid);
                if (!empty($role)) {
                    return $role;
                }
            }
            //get global role
            $role = $this->get_global_shortnamerole($userid);
             $this->response->accept($role);
            
        } catch (Exception $ex) {
            $this->response->danied('badiu.moodle.proxy.error.general',$ex);
        }
        $this->response->accept($role);

    }

      public function profileproxy() {
         if (!isset($this->param['userid'])) {$this->response->danied('badiu.moodle.proxy.error.param.userid.nodefined', 'Param userid is required');}
         if (empty($this->param['userid'])) {$this->response->danied('badiu.moodle.proxy.error.param.userid.empty', 'Param userid can not be null');}
         $userid =$this->param['userid'];
         $list = null;
        try {
           
            $list= $this->get_data_by_profile($userid);
            $this->response->accept($list);
            
        } catch (Exception $ex) {
            $this->response->danied('badiu.moodle.proxy.error.general',$ex);
        }
        $this->response->accept($list);

    }
    private function get_global_shortnamerole($userid) {
        global $DB, $CFG;
        if (empty($userid)) {
            return null;
        }
        $sql = "SELECT DISTINCT r.shortname FROM {$CFG->prefix}role r INNER JOIN {$CFG->prefix}role_assignments rs ON r.id=rs.roleid WHERE rs.userid=$userid";
        $rows = $DB->get_records_sql($sql);

        $cont = 0;
        $result = null;
        foreach ($rows as $row) {
            if ($cont == 0) {
                $result = $row->shortname;
            } else {
                $result.="," . $row->shortname;
            }
            $cont++;
        }
        return $result;
    }

    private function get_course_shortnamerole($userid, $courseid) {
        global $DB, $CFG;
        if (empty($courseid)) {
            return null;
        }
        if (empty($userid)) {
            return null;
        }
        $sql = "SELECT DISTINCT r.shortname FROM {$CFG->prefix}role r INNER JOIN {$CFG->prefix}role_assignments rs ON r.id=rs.roleid  INNER JOIN {$CFG->prefix}context e ON rs.contextid=e.id WHERE e.contextlevel=50 AND  e.instanceid =$courseid AND rs.userid=$userid";
        $rows = $DB->get_records_sql($sql);

        $cont = 0;
        $result = null;
        foreach ($rows as $row) {
            if ($cont == 0) {
                $result = $row->shortname;
            } else {
                $result.="," . $row->shortname;
            }
            $cont++;
        }
        return $result;
    }

    private function is_admin($userid) {
        global $DB, $CFG;
        $sql = "SELECT value FROM {$CFG->prefix}config  WHERE name='siteadmins'";
        $row = $DB->get_record_sql($sql);
        $value = null;
        $result = false;
        if (!empty($row)) {
            $value = $row->value;
        }

        if (empty($value)) {
            return $result;
        }

        $pos = stripos($value, ",");
        if ($pos === false) {
            if ($userid == $value) {
                $result = true;
            }
        } else {
            $luser = explode(",", $value);
            foreach ($luser as $uid) {
                if ($uid == $userid) {
                    $result = true;
                    break;
                }
            }
        }
        return $result;
    }

    private function get_courseid($uri) {
        $courseid = null;

//course
        $pos = stripos($uri, "/course/view.php?id=");
        if ($pos !== false) {
            $courseid = optional_param('id', NULL, PARAM_INT);
        }
        if (!empty($courseid)) {
            return $courseid;
        }

        //activities
        $suri = explode("/", $uri);

        if (isset($suri[1]) && $suri[1] == 'mod' && isset($suri[3])) {
            $pos = stripos($suri[3], "view.php?id=");
            if ($pos !== false) {
                $cmid = optional_param('id', NULL, PARAM_INT);
                $courseid = $this->get_courseid_by_cmid($cmid);
            }
        }
        return $courseid;
    }

    private function get_courseid_by_cmid($cmid) {
        global $DB, $CFG;

        if (empty($cmid)) {
            return null;
        }
        $sql = "SELECT course FROM {$CFG->prefix}course_modules WHERE id=$cmid";
        $row = $DB->get_record_sql($sql);
        if (empty($row)) {
            return null;
        }
        return $row->course;
    }

     private function get_data_by_profile($userid) {
          global $DB, $CFG;
          if (empty($userid)) {
            return null;
        }
         $sql="SELECT d.id,f.shortname,d.data  FROM {$CFG->prefix}user_info_data d INNER JOIN {$CFG->prefix}user_info_field f ON d.fieldid=f.id WHERE f.shortname LIKE 'badiuproxyprofile%' AND d.userid= $userid ";
          $rows= $DB->get_records_sql($sql);
         return $rows;
    }
    function getResponse() {
        return $this->response;
    }

    function getParam() {
        return $this->param;
    }

    function setResponse($response) {
        $this->response = $response;
    }

    function setParam($param) {
        $this->param = $param;
    }


    
}
$proxyservice=new local_badiup_roxy_service();
$connlib=new local_badiunet_conn();
$param=file_get_contents('php://input');
$param=$connlib->convertData($param,true);
$proxyservice->setParam($param); 
$response = new local_badiunet_response();
if(!isset($param['key'])){$response->danied('badiu.moodle.proxy.error.param.key.notdefined', 'Param key is required');}
if(empty($param['key'])){$response->danied('badiu.moodle.proxy.error.param.key.empty', 'Param key can not be null');}
$key=$param['key'];

if(!method_exists($proxyservice, $key)){$response->danied('badiu.moodle.proxy.error.param.key.notvilid', 'Param key not exist');}

$proxyservice->$key();

?>