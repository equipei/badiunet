<?php 
require_once("$CFG->dirroot/local/badiunet/proxy/lib.php");
require_once("$CFG->dirroot/local/badiunet/app/sserver/dblib.php"); 
class local_badiunet_proxy_local_gc2_lib extends local_badiunet_proxy_lib {
 
  function __construct() {
            parent::__construct();
         }
		 
     public function run(){
		 global $DB;
		 if(!isset($DB)){return null;}
		 if (!$DB->get_manager()->table_exists("local_badiunet_sserver")) {return null;}
		$this->role();
	 }
	 
	 
	public function role(){ 
		global $CFG, $USER;
		
		$dblib = new local_badiunet_app_sserver_dblib();
		
		$roles=null;
		$gc2url="";
		$fparam=array('defaultsystem'=>1,'dtype'=>'badiugc2','proxyenable'=>1);
		$countdp=$dblib->count_default_proxy_dtype($fparam);
		
		if($countdp==1){
			$pdto=$dblib->get_default_proxy_dtype($fparam);
			$roles=$pdto->proxyrole;
			$gc2url=$pdto->serviceurl;
		}
		if(empty($roles)){return null;}
		if(empty($gc2url)){return null;}
		
		$cisloggedin=$this->isloggedin();
		
		$sysroleshortname="";
		if($cisloggedin){
			$uri= $this->clean_uri();
			$userid = $USER->id;
			$sysroleshortname=$this->get_global_shortnamerole($userid);
			
		}

		$curenturl="";
		$alternateloginurl="$gc2url/auth/core/login?_exturlgoback={CURRENTURL}&_extbaseurl={BASEURL}";
		if(isset($_SESSION['SESSION']->wantsurl)){$curenturl=$_SESSION['SESSION']->wantsurl;}
		$alternateloginurl=str_replace("{CURRENTURL}", urlencode($curenturl),$alternateloginurl);
		$alternateloginurl=str_replace("{BASEURL}", urlencode($CFG->wwwroot),$alternateloginurl);
		
		$roles=str_replace("BADIU_GC2_URL",$gc2url,$roles);
		$roles=str_replace("MOODLE_SYSTEM_MY_ROLES_SHORTNAME",$sysroleshortname,$roles);
		$roles=str_replace("BADIU_GC2_LOGIN_URL",$alternateloginurl,$roles);
		
		
		$roles=str_replace("BADIU_GC2_URL",$gc2url,$roles);
		$roles=str_replace("MOODLE_SYSTEM_MY_ROLES_SHORTNAME",$sysroleshortname,$roles);
		$roles=str_replace("BADIU_GC2_URL",$gc2url,$roles);
		
		$roles=$this->getUtil()->getJson($roles,true);
		
		foreach ($roles as $role) {
				$this->mredirect($role);
		}
		
	}
	
	
}
