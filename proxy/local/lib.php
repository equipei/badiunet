<?php 
require_once("$CFG->dirroot/local/badiunet/proxy/lib.php"); 
class local_badiunet_proxy_local_lib extends local_badiunet_proxy_lib {
 
  function __construct() {
            parent::__construct();
         }
		 
     public function run(){
		if(!$this->isloggedin()){return null;}
		if(!$this->check_usertarget()){return null;}
		$this->role();
	 }
	 
	 
	 public function check_usertarget(){
		   global $USER;
			//if($USER->auth=='badiuauth'){ return true;}
			$showamstrail=$this->in_cohort('showabadiumstraildashboard');
			if($showamstrail){ return true;}
		   return false;
	 }
	 
	public function role(){ 
		global $CFG;
		$currenturi= $this->clean_uri();
		$ref = $_SERVER['REQUEST_URI'];
		
		$refmy = strpos($currenturi, '/my/');
		$frontpage = strpos($currenturi, '/?redirect=0');
		
		$key= optional_param('_key', null, PARAM_TEXT);
		
		$filterkey=false;
		if($key=='badiu.admin.client.client.frontpage' || $key=='badiu.system.core.core.frontpage'){$filterkey=1;}
		
		if($refmy === 0 || $frontpage  === 0 || $filterkey  ===1){
			$url    = "$CFG->wwwroot/local/badiunet/fcservice/index.php?_key=badiu.ams.my.studentoffercurriculum.dashboard";
			redirect($url);
		}
	}
	
  function in_cohort($cohortidnumber) {
    global $DB, $CFG, $USER;
    $userid = $USER->id;
    if (empty($userid)) {
        return false;
    }
	if (empty($cohortidnumber)) {
        return false;
    }
    $sql = "SELECT COUNT(m.id) AS countrecord FROM {$CFG->prefix}cohort_members m INNER JOIN {$CFG->prefix}cohort c ON c.id=m.cohortid WHERE m.userid=:userid AND c.idnumber=:cohortidnumber";
    $fparam=array('userid'=>$userid,'cohortidnumber'=>$cohortidnumber);
	$result = $DB->get_record_sql($sql,$fparam);

    if (!empty($result)) {
        $result = $result->countrecord;
    }
    return $result;
}	
}