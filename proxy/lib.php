<?php 
require_once("$CFG->dirroot/local/badiunet/lib/util.php");
require_once("$CFG->dirroot/local/badiunet/lib/netlib.php");
require_once("$CFG->dirroot/local/badiunet/lib/httpquerystring.php");
require_once("$CFG->dirroot/local/badiunet/fcservice/maccess.php");
require_once("$CFG->dirroot/local/badiunet/fcservice/factorycontent.php");
class local_badiunet_proxy_lib{
 
    private $util = null;

    function __construct() {
        $this->util=new local_badiunet_util();
     }
    public function run(){
  
       
        if(!$this->isloggedin()){return null;}
       
        $uri= $this->clean_uri();
       
        if($this->exception_proxy($uri)){return null;}
 
        $cached=$this->chekcache();
      
       //  echo "cache: ".$cached;exit;
        if($cached){return null;}
       
        $listprofileproxy=$this->get_profile_proxy();
       
        $context='system'; 
      
        $role=$this->get_shortnamerole($uri);
       if($role==-1){return null;}
       
        global $USER;
        global $CFG;
        $httpqs=new local_badiunet_httpquerystring();
        $httpqs->add('url',$CFG->wwwroot);
        $httpqs->add('uri',$uri);
        $httpqs->add('userid',$USER->id);
        $httpqs->add('username',$USER->username);
        $httpqs->add('idnumber',$USER->idnumber);
        $httpqs->add('email',$USER->email);
        $httpqs->add('context',$context);
        $httpqs->add('shortnamerole',$role);
        $httpqs->add('isloggedin',1);
        $httpqs->add('time',time());
        $httpqs->add('_key','badiu.admin.proxy.check.service');
        $httpqs->add('_kfunction','sservice');  
        if(is_array($listprofileproxy)){
           foreach ($listprofileproxy as $row) {
               $httpqs->add($row['shortname'],$row['data']);
           }
        }
        
    
       
        $factorycontent=new local_badiunet_factorycontent('notformated');
        
        $httpqs->add('sservicid',$factorycontent->getNetlib()->getModuleInstance()); 
        $factorycontent->setService('badiu.system.core.functionality.exec.service');
        $uparam=$httpqs->getParam();
        //$factorycontent->setParamsytemstart($uparam);
        $factorycontent->setParamsytemdefault($uparam); 
       
        $factorycontent->init();
        $response=$factorycontent->getContent();
      
       // print_r( $response);exit;
        if(!$factorycontent->getUtil()->isResponseError($response)){
            $message=$factorycontent->getUtil()->getVlueOfArray($response, 'message');
            $status= $factorycontent->getUtil()->getVlueOfArray($response, 'status');
            $message=$factorycontent->getTcript()->decode($message);   
            if($status=='accept' && !empty($message)){ $this->lock($message);}
          
        }else{$this->addCache();}
      
       return null;
      
    }
    
    public function get_shortnamerole($uri){
        
        #$connlib=new report_badiunet_conn();
        $util=new local_badiunet_util();
        global $CFG;
        global $USER;
        
        $host=$CFG->wwwroot.'/local/badiunet/proxy/service.php';
        $param=array('key'=>'shortnamerole','userid'=>$USER->id,'uri'=>$uri);
        //$result=$connlib->request($host,$param,true);

        $response=$util->request($host,$param);
        $status= $util->getVlueOfArray($response, 'status');
        $info= $util->getVlueOfArray($response, 'info');
        $message=$util->getVlueOfArray($response, 'message');
        $result=-1;
        if($status=='accept' && !empty($message)){
            $result=$message;
         } else { $result=-1;}
        return $result;
    }
     public function get_profile_proxy(){
        global $CFG;
        global $USER;
        
        
        $host=$CFG->wwwroot.'/local/badiunet/proxy/service.php';
        $param=array('key'=>'profileproxy','userid'=>$USER->id);
        $response=$this->util->request($host,$param);
        
        $status= $this->util->getVlueOfArray($response, 'status');
        $info= $this->util->getVlueOfArray($response, 'info');
        $message=$this->util->getVlueOfArray($response, 'message');
       
        if($status=='accept' && !empty($message)){
            $result=$message;
          } else { $result=-1;}
        return $result;
    }
     public function clean_uri(){
         $uri=$_SERVER['REQUEST_URI'];
         global $CFG;
         $host=$CFG->wwwroot;
         $p=explode("//",$host);
         if(!isset($p[1])){ return $uri;}
         $shost=explode("/",$p[1]); 
         
         $suri=explode("/",$uri);
         
         $cont=0;
         $newuri="";
         foreach ($suri as $key => $value) {
            
                $phost=null;
                if(isset($shost[$key])){$phost=$shost[$key];}
                if($key > 0 ){
                    if(empty($value)){$newuri.='/';$cont++;}
                    else if($phost!=$value){
                        if($newuri!='/'){$newuri.='/'.$value;$cont++;}
                    }
                    
                    
                }
                    
                
            
           
         }
         if($cont==0){ return $uri;}
		
        return $newuri;
     }
   public function lock($proxy){
      
       if(empty($proxy)){$this->addCache();return null;}
       
       if(isset($proxy['exec']) && $proxy['exec']==1){
          $msg="Acesso bloqueado";
           // print_r($proxy);exit;
           if(isset($proxy['proxy']['showmessage']) && !empty($proxy['proxy']['showmessage'])){$msg=$proxy['proxy']['showmessage'];}
           $this->addCache($msg);
           $this->lockshowmessage($msg);
       }else{$this->addCache();}
    } 
   public function lockshowmessage($msg){
          global $OUTPUT;
           echo $OUTPUT->header();
            echo "<div class=\"alert alert-danger\" role=\"alert\">$msg</div>";
            echo $OUTPUT->footer();
           exit;
   }
   
   public function addCache($message=null){
      
        $dto=new stdClass;
        $dto->time=time();
        $dto->locked=false;
        if(!empty($message)){ $dto->locked=true;}
        $dto->message=$message;
        $list=array();
        if(isset($_SESSION['_local_badiunet_proxy_cache'])){$list=$_SESSION['_local_badiunet_proxy_cache'];}
       // $uri=$_SERVER['REQUEST_URI'];
       // $list[$uri]=$dto;
        
        $_SESSION['_local_badiunet_proxy_cache']=$dto;
       
    }

 public function chekcache(){
     $result=false;
      //check if cache exist
    if(!isset($_SESSION['_local_badiunet_proxy_cache'])){return $result;}
    if(empty($_SESSION['_local_badiunet_proxy_cache'])){return $result;}
    
   // $list=$_SESSION['_local_badiunet_proxy_cache'];
   // $uri=$_SERVER['REQUEST_URI'];
    //$dto=null;
   // if(isset($list[$uri])){$dto=$list[$uri];}
  
    $dto=$_SESSION['_local_badiunet_proxy_cache'];
     $limit=time()-(5*60);
   
    if(!empty($dto) && $dto->time > $limit) {
        $result=true;
       if($dto->locked && !empty($dto->message)){
            $msg=$dto->message;
            $this->lockshowmessage($msg);
        }
    }
    
    return $result;
} 
  
  public function isloggedin(){
      global $USER;

     if(!isset($USER)){ return false; }
     if($USER->id==0 ||  $USER->id==1){return false;}
     if($USER->id >= 2 ){return true;}
     return false;
  }
  
   public function exception_proxy($uri){
       $excepton=false;
        
       $pos = stripos($uri, "/local/badiunet/proxy/");
       $pos1 = stripos($uri, "/local/badiunet/synchttp/");
       $pos2 = stripos($uri, "/local/badiuws/");
       $pos3 = stripos($uri, "/login/");
       if ($pos !== false) { $excepton=true;}
       else if ($pos1 !== false) { $excepton=true;}
       else if ($pos2 !== false) { $excepton=true;}
       else if ($pos3 !== false) { $excepton=true;}
       return  $excepton;
   }
   
   public function mredirect($param){
	   global $USER;
	  
	   $statuslogin= $this->util->getVlueOfArray($param, 'statuslogin');
	   $roleshortname= $this->util->getVlueOfArray($param, 'roleshortname');
	   $authmethod= $this->util->getVlueOfArray($param, 'authmethod');
	   $urisource= $this->util->getVlueOfArray($param, 'urisource');
	   $urisources= $this->util->getVlueOfArray($param, 'urisources');
	   $urltarget= $this->util->getVlueOfArray($param, 'urltarget');
	   $operation= $this->util->getVlueOfArray($param, 'operation');
	   $sysroleshortname= $this->util->getVlueOfArray($param, 'sysroleshortname');
	   
	   //redirect
	   if($operation=='redirect' && empty($urltarget)){return null;}
	   if($operation=='redirect' && !filter_var($urltarget, FILTER_VALIDATE_URL)){return null;}
	  
	  if($operation=='redirecttologin' && empty($urltarget)){return null;}
	  if($operation=='redirecttologin' && !filter_var($urltarget, FILTER_VALIDATE_URL)){return null;}
	  
	  
	   //loggued
	   $isloggedin=$this->isloggedin();
	   if($statuslogin=='loggedoff' && $isloggedin){return null;}
	   if($statuslogin=='loggedin' && !$isloggedin){return null;}
	  
	    $currenturi= $this->clean_uri();
		$sizeurisource=0;
		if(!empty($urisource)){$sizeurisource=strlen($urisource);}

	  //logguedoff
	   if($statuslogin=='loggedoff' && !$isloggedin){
		 
		   if(!empty($urisource) && $sizeurisource > 1){
			$ckuri = strpos($currenturi, $urisource); 
			if( $ckuri === 0 && $operation=='redirect'){redirect($urltarget);}
			if( $ckuri === 0 && $operation=='redirecttologin'){
				$forcelogin     = optional_param('_badiuforcelogin', null, PARAM_TEXT);
				$username     = optional_param('username', null, PARAM_TEXT);
				if($forcelogin==1){return null;}
				if(!empty($username)){return null;}
				redirect($urltarget);
			}
		 }else if(!empty($urisource) && $sizeurisource == 1){
			$ckuri = false;
			if ($currenturi==$urisource){$ckuri = true;} 
			if($ckuri && $operation=='redirect'){redirect($urltarget);}
			if($ckuri && $operation=='redirecttologin'){
				$forcelogin     = optional_param('_badiuforcelogin', null, PARAM_TEXT);
				$username     = optional_param('username', null, PARAM_TEXT);
				if($forcelogin==1){return null;}
				if(!empty($username)){return null;}
				redirect($urltarget);
			}
		 }
	   }
	   //roleprofile
	   $listroleshortname=$this->util->castStringToArray($sysroleshortname);
	  
	   
	   if(!is_array($listroleshortname)){return null;}
	   if(empty($roleshortname)){return null;}
	   if(!in_array( $roleshortname,$listroleshortname) ) {return null;}
	   
	   //methodaut
	   if(!empty($authmethod) && $USER->auth != $authmethod){return null;}
	   
	   
	
	   if(!empty($urisource)){
		   $ckuri = strpos($currenturi, $urisource); 
		   if( $ckuri === 0 && $operation=='redirect'){redirect($urltarget);}
	   }
	 
	  if(is_array($urisources)){
		  foreach ($urisources as $vuri) {
			  $sizeurisource=0;
			  if(!empty($vuri)){$sizeurisource=strlen($vuri);}
			  if($sizeurisource > 1){
				  $ckuri = strpos($currenturi, $vuri); 
				  if( $ckuri === 0 && $operation=='redirect'){redirect($urltarget);}
			  }else  if($sizeurisource == 1){
				 $ckuri = false;
				 if($currenturi==$vuri){$ckuri = true;} 
				 if($ckuri && $operation=='redirect'){redirect($urltarget);} 
			  }
		  }
	  }
	   
   }
   
   
   
	public function get_global_shortnamerole($userid) {
        global $DB, $CFG;
	
		if (empty($userid)) {
			return ""; 
		}
		
		$isadmin=$this->is_useradmin($userid);
        if ($isadmin) {return "admin";}
		
        $sql = "SELECT DISTINCT r.shortname FROM {$CFG->prefix}role r INNER JOIN {$CFG->prefix}role_assignments rs ON r.id=rs.roleid WHERE rs.userid=$userid";
        $rows = $DB->get_records_sql($sql);

        $cont = 0;
        $result = null;
        foreach ($rows as $row) {
            if ($cont == 0) {
                $result = $row->shortname;
            } else {
                $result.="," . $row->shortname;
            }
            $cont++;
        }
        return $result;
    }
	
	public function is_useradmin($userid) {
        global $DB, $CFG;
        $sql = "SELECT value FROM {$CFG->prefix}config  WHERE name='siteadmins'";
        $row = $DB->get_record_sql($sql);
		
        $value = null;
        $result = false;
        if (!empty($row)) {
            $value = $row->value;
        }

        if (empty($value)) {
            return $result;
        }

        $pos = stripos($value, ",");
        if ($pos === false) {
            if ($userid == $value) {
                $result = true;
            }
        } else {
            $luser = explode(",", $value);
            foreach ($luser as $uid) {
                if ($uid == $userid) {
                    $result = true;
                    break;
                }
            }
        }
        return $result;
    }
	
	public function in_cohort($userid,$cohortidnumber) {
		global $DB, $CFG, $USER;
    
		if (empty($userid)) {
			return false;
		}
		if (empty($cohortidnumber)) {
			return false;
		}
		$sql = "SELECT COUNT(m.id) AS countrecord FROM {$CFG->prefix}cohort_members m INNER JOIN {$CFG->prefix}cohort c ON c.id=m.cohortid WHERE m.userid=:userid AND c.idnumber=:cohortidnumber";
		$fparam=array('userid'=>$userid,'cohortidnumber'=>$cohortidnumber);
		$result = $DB->get_record_sql($sql,$fparam);

		if (!empty($result)) {
			$result = $result->countrecord;
		}
		return $result;
     }
    public function getUtil(){
		 return $this->util;
   } 
}