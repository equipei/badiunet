<?php
/**
 * Library of interface functions and constants for module badiunet
 *
 * @package     local_badiunet
 * @copyright   2024 Lino Vaz Moniz <linovazmoniz@gmail.com>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();
require_once("$CFG->dirroot/local/badiunet/lib/pluginconfig.php");
require_once("$CFG->dirroot/local/badiunet/lib/util.php");
require_once("$CFG->dirroot/local/badiunet/chat/lib.php");
function local_badiunet_extend_navigation($navigation) {
    
}

function local_badiunet_extend_settings_navigation($settingsnav, $context) {
    
}

function local_badiunet_before_footer() {
    global $PAGE, $OUTPUT,$CFG;

	$chatlib=new local_badiunet_chat_lib();
	$util=new local_badiunet_util();
	$uri= $util->clean_uri();
	$pos = stripos($uri, "/local/badiunet/");
	if($pos!== false){return null;}
	

	
	$context = $util->get_dynamic_context();
	//$context = context_system::instance(); 
	if (!has_capability('local/badiunet:usechat', $context)) {return null;}


	//get param config
	$plugin=new local_badiunet_pluginconfig('local_badiunet'); 
    $pconfig=$plugin->getAll();	
	
	//$serviceenable=$util->getVlueOfArray($pconfig,'serviceenable');
	//if(empty($serviceenable)){return null;}
	
	$enablechat=$util->getVlueOfArray($pconfig,'enablechat');
	if(empty($enablechat)){return null;}
	
	$showchatinallpages=$util->getVlueOfArray($pconfig,'showchatinallpages');
	if(empty($showchatinallpages)){return null;}
	
	if($showchatinallpages){
		$showchatoutsidecourseforroles=$util->getVlueOfArray($pconfig,'showchatoutsidecourseforroles');
		if(empty($showchatoutsidecourseforroles)){return null;}
		else{
			$hasroletoaccess=$chatlib->hasRoleToAccess($showchatoutsidecourseforroles);
			if(!$hasroletoaccess){return null;}
		}
	}
	
	
	$serviceurl=$util->getVlueOfArray($pconfig,'serviceurl');
	if(empty($serviceurl)){return null;}
	
	$moduleinstance=null;
	$servicetoken=$util->getVlueOfArray($pconfig,'servicetoken');
	
	if(empty($servicetoken)){return null;}
	$p=explode("|",$servicetoken);
	if(isset($p[4])){$moduleinstance=$p[4];}
    if(empty($moduleinstance)){return null;}
	
	$badiucontextdata=$chatlib->makeContextData();
	if(empty($badiucontextdata)){$badiucontextdata='""';}
	else if(is_array($badiucontextdata)){
		$badiucontextdata=json_encode($badiucontextdata);
	}else{$badiucontextdata='""';}
	
	
	$levelshare=$util->getVlueOfArray($pconfig,'levelchatagent');
	$localendoipoint=$CFG->wwwroot.'/local/badiunet/chat/sync.php';

	//$chatbot='<script id="chatScript"  src="'.$serviceurl.'/ai/assistant/component/chat/load/default.js" data-params=\'{"client": {"modulekey":"badiu.moodle.mreport","moduleinstance":"'.$moduleinstance.'","localendoipoint": "'.$localendoipoint.'","levelshare": "'.$levelshare.'","context": '. $badiucontextdata.'}}\'></script> '; 
	$jsparam='{"client": {"modulekey":"badiu.moodle.mreport","moduleinstance":"'.$moduleinstance.'","localendoipoint": "'.$localendoipoint.'","levelshare": "'.$levelshare.'","context": '. $badiucontextdata.'}}';
	$appchatjscode=$serviceurl.'/ai/assistant/component/chat/load/defaultextlib';
	$loadappchat ="";
	$ch = curl_init($appchatjscode);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$loadappchat = curl_exec($ch);
	curl_close($ch);
	
	$loadappchat=str_replace("_BADIU_AI_ASSISTANT_CHATDEFAULT_COMPTUNTET_PARAM",$jsparam,$loadappchat);
  echo $loadappchat; 

}

