<?php 
require_once("$CFG->dirroot/local/badiunet/lib/webservicerole.php");
require_once("$CFG->dirroot/local/badiunet/lib/sessionlib.php");
require_once("$CFG->dirroot/local/badiunet/lib/util.php");
class local_badiunet_webservice_localbadiunet_general extends local_badiunet_webservicerole {
    
        
    function __construct() {
          parent::__construct();
          
         
    }
    public function existrkey(){
        $result=0;
		global $DB;
        $rkey= $this->getUtildata()->getVaueOfArray($this->getParam(),'rkey');
        $sessionid=$this->getUtildata()->getVaueOfArray($this->getParam(),'sessionid');
		
        if(empty($rkey)){ $this->getResponse()->danied('badiu.moodle.ws.error.param.rkey.undefined');}
		if(empty($sessionid)){ $this->getResponse()->danied('badiu.moodle.ws.error.param.sessionid.undefined');}
		
		if(!$DB->record_exists('local_badiunet_session', array('name' => $sessionid))){ $this->getResponse()->danied('badiu.moodle.ws.error.param.sessionidnotexist',$sessionid.' not exist in database in the table local_badiunet_session');}
		$util=new local_badiunet_util();
        $sessionlib=new local_badiunet_sessionlib();
        $data=$sessionlib->getValue($sessionid);
		$value=$util->getJson($data, true);
		$value=$util->getVlueOfArray($value,'rkeys');
		$rkexist=in_array($rkey, $value);
		if($rkexist){return 1;}
        return 0;
    }
    
   
}
$badiunetws=new local_badiunet_webservice_localbadiunet_general();


?>
