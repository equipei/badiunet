<?php 
require_once("$CFG->dirroot/local/badiunet/lib/webservicerole.php");
require_once("$CFG->dirroot/local/badiunet/lib/dblocallib.php");
class local_badiunet_webservice_mdata_dbfactory extends local_badiunet_webservicerole {
    
    private $reportexec;
    
    function __construct() {
          parent::__construct();
          
         
    }
    public function insert(){
        
        $result=0;

        $tbl= $this->getUtildata()->getVaueOfArray($this->getParam(),'mdata.tbl',true);
        $keyu =$this->getUtildata()->getVaueOfArray($this->getParam(),'mdata.ku',true);
        $row =$this->getUtildata()->getVaueOfArray($this->getParam(),'mdata.row',true);
        $dblocalib=new local_badiunet_dblocallib($tbl,$keyu);
        $result=$dblocalib->saverow($row);
        $this->setError($dblocalib->getError());
        return $result;
    }
    public function edit(){
        
        $result=0;

        $tbl= $this->getUtildata()->getVaueOfArray($this->getParam(),'mdata.tbl',true);
        $keyu =$this->getUtildata()->getVaueOfArray($this->getParam(),'mdata.ku',true);
        $row =$this->getUtildata()->getVaueOfArray($this->getParam(),'mdata.row',true);
        $dblocalib=new local_badiunet_dblocallib($tbl,$keyu);
        $result=$dblocalib->editrow($row);
        $this->setError($dblocalib->getError());
        return $result;
    }
    public function addrow(){
        
        $result=0;

        $tbl= $this->getUtildata()->getVaueOfArray($this->getParam(),'mdata.tbl',true);
        $keyu =$this->getUtildata()->getVaueOfArray($this->getParam(),'mdata.ku',true);
        $row =$this->getUtildata()->getVaueOfArray($this->getParam(),'mdata.row',true);

        $dblocalib=new local_badiunet_dblocallib($tbl,$keyu);
        try {
            $result=$dblocalib->saverow($row);
           }
        catch (Exception $e) {
              if(empty($dblocalib->getError())){
                $result= $dblocalib->add($row);
               }
              
            }
            $this->setError($dblocalib->getError());
        return $result;
    }

    public function addorupdaterrow(){
        
        $result=0;

        $tbl= $this->getUtildata()->getVaueOfArray($this->getParam(),'mdata.tbl',true);
        $keyu =$this->getUtildata()->getVaueOfArray($this->getParam(),'mdata.ku',true);
        $row =$this->getUtildata()->getVaueOfArray($this->getParam(),'mdata.row',true);

        $dblocalib=new local_badiunet_dblocallib($tbl,$keyu);
        $result= $dblocalib->add($row);
        $this->setError($dblocalib->getError());
        return $result;
    }

   public function addrows(){
        $result=0;
        $tbl= $this->getUtildata()->getVaueOfArray($this->getParam(),'mdata.tbl',true);
        $keyu =$this->getUtildata()->getVaueOfArray($this->getParam(),'mdata.ku',true);
        $rows =$this->getUtildata()->getVaueOfArray($this->getParam(),'mdata.rows',true);

        $dblocalib=new local_badiunet_dblocallib($tbl,$keyu);
     
        $resulteachtitem=0;
       
        try {
            $dblocalib->saverows($rows);
            
            }
        catch (Exception $e) {
              if(empty($dblocalib->getError())){
                foreach ($rows as $row) {
                    $resultei= $dblocalib->add($row);
                    if($resultei){$resulteachtitem++;}
                   }
              }
              
            }
        
        if($resulteachtitem){$result=$resulteachtitem;}
        else {$result=sizeof($rows);}
        $this->setError($dblocalib->getError());
         return $result;
    }
    public function addorupdaterows(){
        $result=0;
        $tbl= $this->getUtildata()->getVaueOfArray($this->getParam(),'mdata.tbl',true);
        $keyu =$this->getUtildata()->getVaueOfArray($this->getParam(),'mdata.ku',true);
        $rows =$this->getUtildata()->getVaueOfArray($this->getParam(),'mdata.rows',true);

        $dblocalib=new local_badiunet_dblocallib($tbl,$keyu);
        
        foreach ($rows as $row) {
            $resultei= $dblocalib->add($row);
            if($resultei){$result++;}
           }
        $this->setError($dblocalib->getError());
         return $result;
    }

    public function deleterows(){
        
        $result=0;

        $tbl= $this->getUtildata()->getVaueOfArray($this->getParam(),'mdata.tbl',true);
        $row =$this->getUtildata()->getVaueOfArray($this->getParam(),'mdata.row',true);
        $dblocalib=new local_badiunet_dblocallib($tbl);
        $result=$dblocalib->deleterows($row);
        $this->setError($dblocalib->getError());
        return $result;
    }
    public function deleteall(){
        
        $result=0;

        $tbl= $this->getUtildata()->getVaueOfArray($this->getParam(),'mdata.tbl',true);
        $dblocalib=new local_badiunet_dblocallib($tbl);
        $result=$dblocalib->deleteall();
        $this->setError($dblocalib->getError());
        return $result;
    }
}
$badiunetws=new local_badiunet_webservice_mdata_dbfactory();


?>
