<?php 
require_once("$CFG->dirroot/local/badiunet/webservice/core/message/lib/messagelib.php");
require_once("$CFG->dirroot/local/badiunet/lib/webservicerole.php");
class local_badiunet_webservice_core_message  extends local_badiunet_webservicerole  {
    
    private $lib;
	function __construct() {
          parent::__construct();
      $this->lib=new local_badiunet_lib_coremessage();
    }

   public function send(){
	   $result=array();
		
		$useridto=$this->getUtildata()->getVaueOfArray($this->getParam(),'useridto');
		$useremailto=$this->getUtildata()->getVaueOfArray($this->getParam(),'useremailto');
		
        if(empty($useridto) && empty($useremailto)){
			 $this->getResponse()->danied('badiu.moodle.ws.error.param.userto.notdefined.fillfielduseridtooruseremailto');
		}
		 $message=$this->getUtildata()->getVaueOfArray($this->getParam(),'message');
         if(empty($message)){ $this->getResponse()->danied('badiu.moodle.ws.error.param.message.empty');}
      
	  try {
            
          $result=$this->lib->send($this->getParam());
          
       } catch (Exception $ex) {
            $this->getResponse()->danied('badiu.moodle.ws.error.general',$ex);
			
       }
      return $result;
    }
	
	 public function notification(){
	   $result=array();
		
		$useridto=$this->getUtildata()->getVaueOfArray($this->getParam(),'useridto');
		$useremailto=$this->getUtildata()->getVaueOfArray($this->getParam(),'useremailto');
		
        if(empty($useridto) && empty($useremailto)){
			 $this->getResponse()->danied('badiu.moodle.ws.error.param.userto.notdefined.fillfielduseridtooruseremailto');
		}
		 $message=$this->getUtildata()->getVaueOfArray($this->getParam(),'message');
         if(empty($message)){ $this->getResponse()->danied('badiu.moodle.ws.error.param.message.empty');}
      
	  try {
            
          $result=$this->lib->notification($this->getParam());
          
       } catch (Exception $ex) {
            $this->getResponse()->danied('badiu.moodle.ws.error.general',$ex);
			
       }
      return $result;
    }
}
$badiunetws=new local_badiunet_webservice_core_message();


?>
