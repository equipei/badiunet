<?php
require_once("$CFG->dirroot/local/badiunet/lib/webservicelib.php");
class local_badiunet_lib_coremessage extends local_badiunet_webservicelib{

  function send($param){
	  global $DB;
	$result=array();
	$hashkey=$this->getUtildata()->getVaueOfArray($param,'hashkey');
	$useridfrom=$this->getUtildata()->getVaueOfArray($param,'useridfrom');
	$useridto=$this->getUtildata()->getVaueOfArray($param,'useridto');
	$useremailto=$this->getUtildata()->getVaueOfArray($param,'useremailto');
	$subject=$this->getUtildata()->getVaueOfArray($param,'subject');
	$message=$this->getUtildata()->getVaueOfArray($param,'message');
	$format=$this->getUtildata()->getVaueOfArray($param,'format');
	
	if(empty($useridto) && !empty($useremailto)){
		$useridto=$DB->get_field('user', 'id', array('email'=>$useremailto));
	}
	if(empty($useridto)){return null;}
	
	if(empty($useridfrom)){$useridfrom=2;}
	if(empty($hashkey)){$hashkey='badiumreport.'.$useridfrom.'.'.$useridto.'.'.time();}
	if(empty($format)){$format=0;}
	if(empty($subject)){$subject='Badiu MReport Message';}
	
	$cparam= new stdClass();
	$cparam->type=1;
	$cparam->convhash=$hashkey;
	$cparam->enabled=1;
	$cparam->component='badiumreport';
	$cparam->timecreated=time();
	$cparam->timemodified=time();
	
	$conversationsid=$DB->insert_record('message_conversations', $cparam);
	$result['conversationsid']=$conversationsid;
	
	$cmparam= new stdClass();
	$cmparam->userid=$useridto;
	$cmparam->conversationid=$conversationsid;
	$cmparam->timecreated=time();
	$conversationmembersid=$DB->insert_record('message_conversation_members', $cmparam);
	$result['conversationmembersid']=$conversationmembersid;

	
	$mparam= new stdClass();
	$mparam->useridfrom=$useridfrom;
	$mparam->conversationid=$conversationsid;
	$mparam->subject=$subject;
	$mparam->fullmessage=$message;
	$mparam->fullmessageformat=$format;
	$mparam->fullmessagehtml='';
	$mparam->customdata='';
	$mparam->timecreated=time();
	$messagesid=$DB->insert_record('messages', $mparam);
	$result['messagesid']=$messagesid;
	return $result;
    
}   
 
  function notification($param){
	  global $DB;
	$result=array();
	
	$useridfrom=$this->getUtildata()->getVaueOfArray($param,'useridfrom');
	$useridto=$this->getUtildata()->getVaueOfArray($param,'useridto');
	$useremailto=$this->getUtildata()->getVaueOfArray($param,'useremailto');
	$subject=$this->getUtildata()->getVaueOfArray($param,'subject');
	$message=$this->getUtildata()->getVaueOfArray($param,'message');
	$format=$this->getUtildata()->getVaueOfArray($param,'format');
	$eventtype=$this->getUtildata()->getVaueOfArray($param,'eventtype');
	
	if(empty($useridto) && !empty($useremailto)){
		$useridto=$DB->get_field('user', 'id', array('email'=>$useremailto));
	}
	if(empty($useridto)){return null;}
	
	if(empty($useridfrom)){$useridfrom=2;}
	if(empty($format)){$format=2;}
	if(empty($subject)){$subject='Badiu MReport Message';}
	if(empty($eventtype)){$eventtype='default';}
	
		
	$mparam= new stdClass();
	$mparam->useridfrom=$useridfrom;
	$mparam->useridto=$useridto;
	$mparam->subject=$subject;
	$mparam->fullmessage=$message;
	$mparam->fullmessageformat=$format;
	$mparam->fullmessagehtml=$message;
	$mparam->component='mreport';
	$mparam->eventtype=$eventtype;
	$mparam->timecreated=time();
	$notificationsid=$DB->insert_record('notifications', $mparam);
	$result['notificationid']=$notificationsid;
	
	$mpparam= new stdClass();
	$mpparam->notificationid=$notificationsid;
	
	$popnotificationsid=$DB->insert_record('message_popup_notifications', $mpparam);
	$result['messagepopupnotificationid']=$popnotificationsid;
	return $result;
    
}   
}

?>
