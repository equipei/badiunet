<?php
require_once("$CFG->dirroot/local/badiunet/lib/webservicelib.php");
class local_badiunet_lib_coursecontent extends local_badiunet_webservicelib{

  function get_modules_course($courseid){
        global $CFG,$DB;
		$sql="SELECT DISTINCT m.name FROM  {$CFG->prefix}course_modules cm INNER JOIN {$CFG->prefix}modules m ON m.id=cm.module  WHERE cm.course=:courseid ";
        $fparam=array('courseid'=>$courseid);
		$result=$DB->get_records_sql($sql,$fparam);
        return $result;
    
}   
 
 function search_course_instance_module($courseid,$modulename,$text){
        global $CFG,$DB;
		$text=strtolower($text);
		$sql="SELECT cm.id,i.name FROM {$CFG->prefix}$modulename i INNER JOIN {$CFG->prefix}course_modules cm ON cm.instance=i.id INNER JOIN {$CFG->prefix}modules m ON m.id=cm.module WHERE cm.course=i.course AND cm.course=:courseid AND m.name=:modulename AND LOWER(CONCAT(cm.id,i.name)) LIKE :text ";
		$fparam=array('courseid'=>$courseid,'modulename'=>$modulename,'text'=>"%$text%");
		$result=$DB->get_records_sql($sql,$fparam);
        return $result;
     
}  

 function search_course_instances_modules($courseid,$text){
	
	    if(empty($courseid)){return null;}
		 if(empty($text)){return null;}
        $listmodules=$this->get_modules_course($courseid);
		if(!is_array($listmodules)){return null;}
		$list=array();
		foreach($listmodules as $row) {
			$modulename=$row->name;
			$instancemdlist=$this->search_course_instance_module($courseid,$modulename,$text);
			if(is_array($instancemdlist)){
				foreach($instancemdlist as $irow) {
					array_push($list,array('id'=>$irow->id,'name'=>$irow->name));
				}
			}
			
		}
		
     return $list;
}


 function get_course_modules($courseid){
	    if(empty($courseid)){return null;}
		$listmodules=$this->get_modules_course($courseid);
		if(!is_array($listmodules)){return null;}
		$list=array();
		foreach($listmodules as $row) {
			$modulename=$row->name;
			$instancemdlist=$this->get_course_instance_module($courseid,$modulename);
			if(is_array($instancemdlist)){
				foreach($instancemdlist as $irow) {
					array_push($list,array('id'=>$irow->id,'name'=>$irow->name));
				}
			}
			
		}
		
     return $list;
}

 function get_course_instance_module($courseid,$modulename){
        global $CFG,$DB;
		$sql="SELECT cm.id,i.name FROM {$CFG->prefix}$modulename i INNER JOIN {$CFG->prefix}course_modules cm ON cm.instance=i.id INNER JOIN {$CFG->prefix}modules m ON m.id=cm.module WHERE cm.course=i.course AND cm.course=:courseid AND m.name=:modulename ";
		$fparam=array('courseid'=>$courseid,'modulename'=>$modulename);
		$result=$DB->get_records_sql($sql,$fparam);
        return $result;
     
}  
function get_instance_name($cmid){
        global $CFG,$DB;
		if(empty($cmid)){return null;}
		$modulename=$this->get_module_name($cmid);
		$sql="SELECT i.name FROM {$CFG->prefix}$modulename i INNER JOIN {$CFG->prefix}course_modules cm ON cm.instance=i.id INNER JOIN {$CFG->prefix}modules m ON m.id=cm.module WHERE cm.id=:cmid AND m.name=:modulename ";
		 $fparam=array('cmid'=>$cmid,'modulename'=>$modulename);
		$result=$DB->get_record_sql($sql,$fparam);
		
        if(isset($result->name)){return $result->name;}
        return null;
     
} 

 function get_module_name($cmid){
        global $CFG,$DB;
		$sql="SELECT m.name FROM  {$CFG->prefix}course_modules cm INNER JOIN {$CFG->prefix}modules m ON m.id=cm.module  WHERE cm.id=:cmid ";
        $fparam=array('cmid'=>$cmid);
		$result=$DB->get_record_sql($sql,$fparam);
		if(isset($result->name)){return $result->name;}
        return null;
     
}  

   /**
 * Retrieves the IDs of course modules based on the specified course and module name.
 * 
 * This function constructs a SQL query to select the IDs of course modules from the Moodle database.
 * It joins the `course_modules` table with the `modules` table to filter records by the provided
 * course ID and module name. It utilizes the global configuration and database objects provided by Moodle.
 * 
 * @param int $courseid The ID of the course for which modules are to be fetched.
 * @param string $modulename The name of the module to fetch IDs for.
 * @return array An array of stdClass objects representing the rows fetched from the database. Each object contains the ID of a course module.
 * 
 * @global stdClass $CFG Global configuration object, used here to access table prefix.
 * @global moodle_database $DB Global database object used for executing SQL queries.
 * 
 * Usage:
 * $module_ids = get_course_modules_id(123, 'forum');
 * foreach ($module_ids as $module) {
 *     echo $module->id;
 * }
 */
  function get_course_modules_id($courseid,$modulename){
	global $CFG,$DB;
	$fparam =array('modulename'=>$modulename,'courseid'=>$courseid);
	$sql ="SELECT cm.id  FROM {$CFG->prefix}course_modules cm INNER JOIN {$CFG->prefix}modules m ON m.id=cm.module  WHERE cm.course=:courseid AND m.name=:modulename ";
	$rows = $DB->get_records_sql($sql,$fparam);
	return $rows;
 }
 
 /**
 * Updates the visibility of a specific course module in Moodle.
 *
 * This function is designed to change the visibility status of a course module identified by its ID.
 * It updates the 'visible', 'visibleold', and 'timemodified' fields in the 'course_modules' table.
 * The function uses Moodle's database abstraction layer to perform the update operation.
 *
 * @param int $cmid The ID of the course module to be updated.
 * @param int $visible The new visibility status of the course module (0 for hidden, 1 for visible).
 * @return bool Returns true if the record is successfully updated, false otherwise.
 *
 * @global moodle_database $DB Global database object used for executing SQL queries.
 *
 * Usage:
 * $success = change_visible_course_module(101, 1);
 * if ($success) {
 *     echo 'Visibility updated successfully.';
 * } else {
 *     echo 'Failed to update visibility.';
 * }
 */
  function change_visible_course_module($cmid,$visible){
	 global $DB;	
	$dto= new stdClass();
     $dto->id=$cmid;
     $dto->visible=$visible;
	 $dto->visibleold=$visible;
	 $dto->timemodified=time();
     $result=$DB->update_record('course_modules', $dto);
	 return $result;
 }
 
 /**
 * Updates the visibility of multiple course modules based on a provided list of course module IDs.
 *
 * This function processes a list of course module objects, updating the visibility for each module where possible.
 * The function relies on `change_visible_course_module` to update each individual course module's visibility.
 * If the provided list is not an array, the function returns 0 immediately. It returns the count of successfully
 * updated course modules otherwise.
 *
 * @param array $list An array of objects, each containing at least an 'id' property representing the course module ID.
 * @param int $visible The new visibility status to be applied to all listed course modules (0 for hidden, 1 for visible).
 * @return int The number of course modules successfully updated to the new visibility status.
 *
 * Usage:
 * $modules = [
 *     (object) ['id' => 101],
 *     (object) ['id' => 102],
 *     (object) ['id' => 103]
 * ];
 * $updatedCount = change_visible_course_modules($modules, 1);
 * echo "Number of modules updated: $updatedCount";
 */
   function change_visible_course_modules($list,$visible){
	  if(!is_array($list)){return 0;}
	  $cont=0;// Return 0 immediately if input is not an array.
	 foreach ($list as $item){
		 if(isset($item->id)){ // Check if the item has an ID property.
			$result=$this->change_visible_course_module($item->id,$visible);  // Call the function to change visibility of the module.
			if($result){$cont++;} // Increment count if the visibility was successfully updated.
		 }
	 }
	 return $cont;// Return the number of successful updates.
 }    
}

?>
