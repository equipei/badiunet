<?php
require_once("$CFG->dirroot/local/badiunet/lib/webservicelib.php");

class local_badiunet_lib_course extends local_badiunet_webservicelib{
function __construct() {
		parent::__construct();
	}
   function get_list($name,$categoryid=null,$offset,$limit) {
         global $DB;
        global $CFG;
        $wsql="";
          if(!empty($categoryid)){
          $wsql=" AND category= $categoryid ";
        }
        if(!empty($name)){
            $wsql.=" AND LOWER(CONCAT(id,fullname,shortname))  LIKE '%".strtolower($name)."%'";
        }
        $sql = "SELECT  id,fullname  AS name FROM {$CFG->prefix}course  WHERE  id > 1 $wsql";
     
        $r = $DB->get_records_sql($sql,null,$offset,$limit);
        return $r;
    }
    
   function get_name($id) {
         global $DB;
        global $CFG;
        $sql = "SELECT  fullname AS name FROM {$CFG->prefix}course WHERE id=$id ";
        $r = $DB->get_record_sql($sql);
        return $r->name;
    }
    
    function get_info($id) {
         global $DB;
        global $CFG;
        $sql = "SELECT  c.fullname AS name,c.shortname,ct.name AS category,ct.id AS  categoryid FROM {$CFG->prefix}course c  INNER JOIN {$CFG->prefix}course_categories ct ON c.category=ct.id WHERE c.id=$id ";
        $r = $DB->get_record_sql($sql);
        return $r;
    }
    
     function create($param) {
         $course['fullname'] = $param['fullname'];
         $course['shortname'] =$param['shortname'];
         $course['categoryid'] =$param['categoryid'];
       
         $courses = array($course);
         $result = core_course_external::create_courses($courses);
         $result=$result[0]['id'];
        return $result;
    }
	function update($param) {
			$id=$this->getUtildata()->getVaueOfArray($param,'id');
			$fullname=$this->getUtildata()->getVaueOfArray($param,'fullname');
			if(empty($id)){return null;}
			$aparam=new stdClass();
			$aparam->id=$id;
			$aparam->fullname=$fullname;
		    $aparam->timemodified=time();
			global $DB;
			$result=$DB->update_record('course', $aparam);
			return $result;
    } 
	
	function update_offer_status($param) {
			$id=$this->getUtildata()->getVaueOfArray($param,'id');
			$visible=$this->getUtildata()->getVaueOfArray($param,'visible');
			$startdate=$this->getUtildata()->getVaueOfArray($param,'startdate');
			$enddate=$this->getUtildata()->getVaueOfArray($param,'enddate');
			if(empty($id)){return null;}
			$execupdate=false;
			$aparam=new stdClass();
			$aparam->id=$id;
			if (is_numeric($visible) && $visible >= 0) {$aparam->visible=$visible;$execupdate=true;}
			if (is_numeric($startdate) && $startdate >= 0) {$aparam->startdate=$startdate;$execupdate=true;}
			if (is_numeric($enddate) && $enddate >= 0) {$aparam->enddate=$enddate;$execupdate=true;}
			
		    $aparam->timemodified=time();
			global $DB;
			$result=0;
			if($execupdate){$result=$DB->update_record('course', $aparam);}
			
			return $result;
    }
	
	function add_enrol_method_manual($param) {
			$courseid=$this->getUtildata()->getVaueOfArray($param,'courseid');
			if(empty($courseid)){return null;}
			global $DB;
			global $CFG;
			$sql = "SELECT  COUNT(id) AS countrecord FROM {$CFG->prefix}enrol WHERE enrol=:enrol AND courseid=:courseid ";
			$fparam=array('enrol'=>'manual','courseid'=>$courseid);
			$result=$DB->get_record_sql($sql,$fparam);
			if(isset($result) && isset($result->countrecord) && $result->countrecord > 0){return -1; }
			
			//get default role
			$roleid=5;
			$sql = "SELECT id FROM {$CFG->prefix}role WHERE shortname=:shortname ";
			$rparam=array('shortname'=>'student');
			$rresult=$DB->get_record_sql($sql,$rparam);
			if(isset($rresult) && isset($rresult->id)){$roleid=$rresult->id; }
			
			$aparam=new stdClass();
			$aparam->courseid=$courseid;
			$aparam->enrol='manual';
			$aparam->status=0;
			$aparam->roleid=$roleid;
			$aparam->timecreated=time();
		    $iresult=$DB->insert_record('enrol', $aparam);
			
			return $iresult;
    }
	
		function remove_duplicate_gradeitemtypecourse($param) {
			$courseid=$this->getUtildata()->getVaueOfArray($param,'courseid');
			if(empty($courseid)){return null;}
			global $DB;
			global $CFG;
			$sql = "SELECT  COUNT(id) AS countrecord FROM {$CFG->prefix}grade_items WHERE itemtype=:itemtype AND courseid=:courseid ";
			$fparam=array('itemtype'=>'course','courseid'=>$courseid);
			$result1=$DB->get_record_sql($sql,$fparam);
			if(isset($result1) && isset($result1->countrecord) && $result1->countrecord == 1){
				
				//check duplicate grade_categories 
					$sql = "SELECT  id FROM {$CFG->prefix}grade_items WHERE itemtype=:itemtype AND courseid=:courseid ";
					$fparam=array('itemtype'=>'course','courseid'=>$courseid);
					$result1=$DB->get_record_sql($sql,$fparam);
					$itemid=0;
					if(isset($result1) && isset($result1->id)){$itemid=$result1->id;}
					if(!empty($itemid)){
						//delete duplicate grade_categories
						$fcparam=array('itemid'=>$itemid,'courseid'=>$courseid);
						$sql = "DELETE FROM {$CFG->prefix}grade_categories WHERE courseid=:courseid AND id NOT IN (SELECT iteminstance FROM {$CFG->prefix}grade_items WHERE id=:itemid)";
						$result=$DB->execute($sql,$fcparam);
					}
				
				return 1;
			
			}
			
			//keep firt item
			$sql = "SELECT  MIN(id) AS firstid FROM {$CFG->prefix}grade_items WHERE itemtype=:itemtype AND courseid=:courseid ";
			$result2=$DB->get_record_sql($sql,$fparam);
			$firstid=null;
			if(isset($result2) && isset($result2->firstid)){$firstid= $result2->firstid;}
			if(empty($firstid)){return -2; }
			if(!is_numeric($firstid)){return -3; }
			
			$sql = "DELETE FROM {$CFG->prefix}grade_items WHERE itemtype=:itemtype AND courseid=:courseid AND id != :itemid ";
			$fparam['itemid']=$firstid;
			$result=$DB->execute($sql,$fparam);
			
			//delete duplicate grade_categories
			$sql = "DELETE FROM {$CFG->prefix}grade_categories WHERE courseid=:courseid AND id NOT IN (SELECT iteminstance FROM {$CFG->prefix}grade_items WHERE id=:itemid)";
			$result=$DB->execute($sql,$fparam);
			
			$fresult=$result1->countrecord-1;
			return $fresult; 
    }
}

?>
