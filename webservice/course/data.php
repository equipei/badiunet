<?php 
require_once("$CFG->dirroot/local/badiunet/lib/webservicerole.php");
require_once("$CFG->dirroot/local/badiunet/lib/syskey.php");
require_once("$CFG->dirroot/course/externallib.php");
require_once($CFG->dirroot . '/backup/util/includes/backup_includes.php');

class local_badiunet_webservice_course_data extends local_badiunet_webservicerole {
    
 
    function __construct() {
          parent::__construct();
          
         
    }
   

   public function backup(){
       
        $paramfile=array();
        $courseid= $this->getUtildata()->getVaueOfArray($this->getParam(),'courseid');
        $userinteraction= $this->getUtildata()->getVaueOfArray($this->getParam(),'userinteraction');
        $useridexec=$this->getUtildata()->getVaueOfArray($this->getParam(),'useridexec');
        if(empty($useridexec)){$useridexec=2;}
        global $DB;
        $luser = $DB->get_record('user', array('id'=>$useridexec)); 
        complete_user_login($luser);

        $interationtype=backup::INTERACTIVE_NO;
        if($userinteraction){$interationtype=backup::INTERACTIVE_YES;}
        $bc = new backup_controller(backup::TYPE_1COURSE, $courseid, backup::FORMAT_MOODLE,$interationtype, backup::MODE_SAMESITE, $useridexec);
        $bc->set_status(backup::STATUS_AWAITING);
        $bc->execute_plan();
        $result = $bc->get_results();
        if (isset($result['backup_destination']) && $result['backup_destination']) {
            $file = $result['backup_destination'];
            $context = context_course::instance($courseid);
            $fs = get_file_storage();
            $timestamp = time();

            $filerecord = array(
                'contextid' => $context->id,
                'component' => 'local_badiunet',
                'filearea' => 'backup',
                'itemid' => $timestamp,
                'filepath' => '/',
                'filename' => 'backup_course_'.$courseid,
                'timecreated' => $timestamp,
                'timemodified' => $timestamp
            );
            $storedfile = $fs->create_file_from_storedfile($filerecord, $file);
            $file->delete();

            // Make the link.
            
            $paramfile['contextid']=$storedfile->get_contextid();
            $paramfile['component']=$storedfile->get_component();
            $paramfile['filearea']= $storedfile->get_filearea();
            $paramfile['itemid']=$storedfile->get_itemid();
            $paramfile['filepath']=$storedfile->get_filepath();
            $paramfile['filename']=$storedfile->get_filename();
            
            
        } else {
            $this->setError(array('info'=>'badiu.moodle.net.error.coursebackupfailure','message'=>'Backup of course failure'));
            
        }
        return $paramfile;
       
    }

    public function restore(){
        
        $source= $this->getUtildata()->getVaueOfArray($this->getParam(),'source');
        $target= $this->getUtildata()->getVaueOfArray($this->getParam(),'target');

        $userinteraction= $this->getUtildata()->getVaueOfArray($this->getParam(),'userinteraction');
        $useridexec=$this->getUtildata()->getVaueOfArray($this->getParam(),'useridexec');
        if(empty($useridexec)){$useridexec=2;}
        global $DB,$CFG;
        $luser = $DB->get_record('user', array('id'=>$useridexec)); 
        complete_user_login($luser);

        $interationtype=backup::INTERACTIVE_NO;
        if($userinteraction){$interationtype=backup::INTERACTIVE_YES;}
        
        $storedfile=$this->downloadfile();
      return $storedfile;
        $fcp= $storedfile->get_contenthash();
        $pathdatroot =$CFG->dataroot;
        $f1=substr($fcp,0,2);
        $f2=substr($fcp,2,2);

        $fpath="$pathdatroot/filedir/$f1/$f2/$fcp";
       
        $syskey=new local_badiunet_syskey();
        $timestamp = time();
        $tempfolder=$syskey->make_hash(25);
        $tempfolder=$tempfolder.'.'.$timestamp;
        $targetpath="$pathdatroot/temp/backup/$tempfolder";

        if (!file_exists($targetpath)) {
            mkdir($targetpath, 0777, true);
        }
        return $targetpath;
         $resullt= $storedfile->extract_to_pathname(get_file_packer('application/vnd.moodle.backup'),$targetpath);
        //extract backup
        

        //create course to restore
        $coursename='Badiu 21';//;$this->getUtildata()->getVaueOfArray($this->getParam(),'target.newcourse.name',true);
        $courseshotname="b2122";//$this->getUtildata()->getVaueOfArray($this->getParam(),'target.newcourse.shortname',true);
        $coursecategoryid=5;//$this->getUtildata()->getVaueOfArray($this->getParam(),'target.newcourse.categoryid',true);
       
        $newcourseid = restore_dbops::create_new_course($coursename, $courseshotname, $coursecategoryid);
        return 122; 
        $rc = new restore_controller($targetpath, $newcourseid,$interationtype, backup::MODE_GENERAL, $useridexec, backup::TARGET_NEW_COURSE);

        $rc->execute_precheck();
        $rc->execute_plan();
        $rc->destroy();

        return $newcourseid;
       
    }

    private function downloadfile(){
        $keyauth = $this->getUtildata()->getVaueOfArray($this->getParam(),'source.keys._keyautdbcheckk.name',true);
        $valueauth = $this->getUtildata()->getVaueOfArray($this->getParam(),'source.keys._keyautdbcheckk.value',true);
        $keyauthadtional =$this->getUtildata()->getVaueOfArray($this->getParam(),'source.keys._keyautdbcheckkadtional.name',true); 
        $valueauthadtional = $this->getUtildata()->getVaueOfArray($this->getParam(),'source.keys._keyautdbcheckkadtional.value',true); 
        $keygetfileconfig = $this->getUtildata()->getVaueOfArray($this->getParam(),'source.keys._keyautdbcheckkfconfig.name',true); 
        $fileurl = $this->getUtildata()->getVaueOfArray($this->getParam(),'source.url',true);

        $url="$fileurl/local/badiunet/synchttp/expfile.php?_keyautdbcheckk=$keyauth&_valueautdbcheckv=$valueauth&_keyautdbcheckkadtional=$keyauthadtional&_valueautdbcheckvadtional=$valueauthadtional&_keyautdbcheckkfconfig=$keygetfileconfig";
  return $url;
        //downlaod backup
        $timestamp = time();
        $filerecord = array(
        'contextid' => 1,
        'component' => 'local_badiunet',
        'filearea'  => 'restoure',
        'itemid'    => $timestamp,
        'filepath'  => '/',
        'filename'  => 'course_moodle_2_'.$timestamp,
        'timecreated' => $timestamp,
        'timemodified' => $timestamp
        );
        $fs = get_file_storage();
 
        $storedfile = $fs->create_file_from_url($filerecord,$url, null, true);
        return  $storedfile;
    }
    
}
$badiunetws=new local_badiunet_webservice_course_data();


?>
