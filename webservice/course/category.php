<?php
require_once("$CFG->dirroot/course/externallib.php");
require_once("$CFG->dirroot/local/badiunet/webservice/course/lib/categorylib.php");
require_once("$CFG->dirroot/local/badiunet/lib/webservicerole.php");
class local_badiunet_coursecategory  extends local_badiunet_webservicerole  {
    
    private $lib;
    function __construct() {
          parent::__construct();
       $this->lib=new local_badiunet_lib_coursecategory();
    }
   
   public function create() {
        global $DB;
         if(!isset($this->getParam()['name'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.name.undefined');}
         if(empty($this->getParam()['name'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.name.empty');}
         if(!is_int((int)$this->getParam()['parentid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.parentid.isnotnumber');}
         
         
        
         //check permission review permission
         $this->login();
        $permission=has_capability('moodle/user:create',context_system::instance());
        if(!$permission){ $this->getResponse()->danied('badiu.moodle.ws.error.nopermission','moodle/user:create');}
        
         
         $paramcat['name'] = $this->getParam()['name'];
         if(!empty($this->getParam()['idnumber'])){
             $paramcat['idnumber'] = $this->getParam()['idnumber'];
             if ($DB->record_exists('course_categories', array('idnumber' => $this->getParam()['idnumber']))) {
                $this->getResponse()->danied('badiu.moodle.ws.error.param.idnumberjustexist', $this->getParam()['idnumber'] . ' just exist in database in the table course_categories');
            }
         }
         
         $paramcat['parent']=0;
         if((int)$this->getParam()['parentid'] > 0){$paramcat['parent'] = $this->getParam()['parentid'];}
         
           try {
               $category = array($paramcat);
              $result = core_course_external::create_categories($category);
           } catch (Exception $ex) {
                $this->getResponse()->danied('badiu.moodle.ws.error.general',$ex);
           }
         $result=$result[0]['id'];
        return $result;
    }
	
    public function update() {
		
        global $DB;
		$id=$this->getUtildata()->getVaueOfArray($this->getParam(),'id');
		$name=$this->getUtildata()->getVaueOfArray($this->getParam(),'name');
		
		if(empty($id)){ $this->getResponse()->danied('badiu.moodle.ws.error.param.id.isrequired');}
		if(!is_int((int)$id)){ $this->getResponse()->danied('badiu.moodle.ws.error.param.id.isnotnumber');}
		
        if(empty($name)){ $this->getResponse()->danied('badiu.moodle.ws.error.param.name.empty');}
        
		$aparam=array();
		$aparam['id']=$id;
		$aparam['name']=$name;
		$result=null;
        try {
              $result=$this->lib->update($aparam);
           } catch (Exception $ex) {
                $this->getResponse()->danied('badiu.moodle.ws.error.general',$ex);
           }
        
        return $result;
    }
  
  
   public function getlisttree() {
	 
       $result=array();
       $id=null;
       $name=null;
       global $DB;
        if(isset($this->getParam()['id'])){
             if(!is_int((int)$this->getParam()['id'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.id.isnotnumber');}
             if($this->getParam()['id'] > 0 && !$DB->record_exists('course_categories', array('id' => $this->getParam()['id']))){ $this->getResponse()->danied('badiu.moodle.ws.error.param.coursecategoryidnotexist',$this->getParam()['id'].' not exist in database in the table course_categories');}
          $id=$this->getParam()['id'];
          
        }
       $name=$this->getParam()['name'];
	   try {
            
          $list=$this->lib->get_tree($id,$name);
           foreach ($list as $key => $value) {
              array_push($result,array('id'=>$key,'name'=>$value));
           }
       } catch (Exception $ex) {
            $this->getResponse()->danied('badiu.moodle.ws.error.general',$ex);
       }
      return $result;
   }
 
  public function getname() {
       $result=array();
       $id=null;
     
       global $DB;
            if(!isset($this->getParam()['id'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.id.undefined');}
            if(empty($this->getParam()['id'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.id.empty');}
           if(!is_int((int)$this->getParam()['id'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.id.isnotnumber');}
           if(!$DB->record_exists('course_categories', array('id' => $this->getParam()['id']))){ $this->getResponse()->danied('badiu.moodle.ws.error.param.coursecategoryidnotexist',$this->getParam()['id'].' not exist in database in the table course_categories');}
           $id=$this->getParam()['id'];
          
       try {
            
           $name=$this->lib->get_name($id);
           
       } catch (Exception $ex) {
            $this->getResponse()->danied('badiu.moodle.ws.error.general',$ex);
       }
      return $name;
   }
     public function getnametree() {
      $result=array();
       $id=null;
      
        global $DB;
        global $CFG;
            if(!isset($this->getParam()['id'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.id.undefined');}
            if(empty($this->getParam()['id'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.id.empty');}
           if(!is_int((int)$this->getParam()['id'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.id.isnotnumber');}
           if(!$DB->record_exists('course_categories', array('id' => $this->getParam()['id']))){ $this->getResponse()->danied('badiu.moodle.ws.error.param.coursecategoryidnotexist',$this->getParam()['id'].' not exist in database in the table course_categories');}
           $id=$this->getParam()['id'];
       try {
           $name=null; 
            require_once("$CFG->dirroot/lib/coursecatlib.php");
            $displaylist = coursecat::make_categories_list();
            if (array_key_exists($id,$displaylist)){$name=$displaylist[$id];}
            if(empty($name)){ $name=$this->lib->get_name($id);}
           $result=$name;
          
       } catch (Exception $ex) {
            $this->getResponse()->danied('badiu.moodle.ws.error.general',$ex);
       }
      return $result;
   }
 
}
$badiunetws=new local_badiunet_coursecategory();

?>
