<?php
require_once("$CFG->dirroot/course/externallib.php");
require_once("$CFG->dirroot/local/badiunet/webservice/course/lib/contentlib.php");
require_once("$CFG->dirroot/local/badiunet/lib/webservicerole.php");
class local_badiumnet_coursecontent  extends local_badiunet_webservicerole  {
    
    private $lib;
    function __construct() {
          parent::__construct();
       $this->lib=new local_badiunet_lib_coursecontent();
    }
 
  
  
   public function search() {
	 
       $result=array();
       $id=null;
       $name=null;
       global $DB;
	   if(empty($this->getParam()['courseid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.courseid.empty');}
	    if(!is_int((int)$this->getParam()['courseid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.courseid.isnotnumber');}
       if(empty($this->getParam()['name'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.name.empty');}
       $name=$this->getParam()['name'];
	   $courseid=$this->getParam()['courseid'];
	   try {
            
         $result=$this->lib->search_course_instances_modules($courseid,$name);
           
       } catch (Exception $ex) {
            $this->getResponse()->danied('badiu.moodle.ws.error.general',$ex);
       }
      return $result;
   }
 
 public function getname() {
	 
       $result=array();
       $cmid=null;
      
       global $DB;
	   if(empty($this->getParam()['cmid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.cmid.empty');}
	    if(!is_int((int)$this->getParam()['cmid'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.cmid.isnotnumber');}
       if(empty($this->getParam()['name'])){ $this->getResponse()->danied('badiu.moodle.ws.error.param.name.empty');}
       $cmid=$this->getParam()['cmid'];
	   try {
            
          $result=$this->lib->get_instance_name($cmid);
           
       } catch (Exception $ex) {
            $this->getResponse()->danied('badiu.moodle.ws.error.general',$ex);
       }
      return $result;
   }
   
 /**
 * Changes the visibility of all modules of a specified type within a given course.
 *
 * This method updates the visibility of all instances of a specific module type within the specified course.
 * It validates the input parameters for 'courseid', 'modulename', and 'visible'. It ensures that the 'courseid'
 * and 'modulename' are valid and exist in the database, and checks that 'visible' is a valid boolean (0 or 1).
 * Upon validation, it retrieves a list of module IDs and changes their visibility using utility methods from the
 * class library. If any validation fails, the method will deny the request and provide an appropriate error message.
 *
 * @return int The count of modules whose visibility was successfully updated, or an error message if an exception occurs.
 *
 * Usage:
 * $result = $thisInstance->changevisibleactivitytype();
 * echo $result . ' modules updated successfully.';
 */    
 public function changevisibleactivitytype() {
	 
		$auth=$this->checkAuth();
		if(!empty($auth)){return $auth;}
       $courseid= $this->getUtildata()->getVaueOfArray($this->getParam(),'courseid');
	   $modulename= $this->getUtildata()->getVaueOfArray($this->getParam(),'modulename');
	   $visible= $this->getUtildata()->getVaueOfArray($this->getParam(),'visible');
	 
		global $DB;
        if (empty($courseid)) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.courseid.undefined');
        }
        if (!is_int((int) $courseid)) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.courseid.isnotnumber');
        }
        if (!$DB->record_exists('course', array('id' => $courseid))) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.courseidnotexist', $courseid . ' not exist in database in the table course');
        }
		
		
		if (empty($modulename)) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.modulename.undefined');
        }
		if (!$DB->record_exists('modules', array('name' => $modulename))) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.modulenotexist', $modulename . ' not exist in database in the table modules');
        }
		
		
		if ($visible==="") {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.visible.undefined');
        }
		
		if ($visible < 0 && $visible > 1) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.visible.notbooleannumber','The param visible should has value 0  or 1');
        }
		
		 $result=0;
		
	   try {
            
          $listcmid=$this->lib->get_course_modules_id($courseid,$modulename);
		  $result=$this->lib->change_visible_course_modules($listcmid,$visible);
           
       } catch (Exception $ex) {
            $this->getResponse()->danied('badiu.moodle.ws.error.general',$ex);
       }
      return $result;
   }
 
 /**
 * Changes the visibility of a specified course module after performing authentication and validation of input parameters.
 *
 * This method first checks for authentication using `checkAuth`. If authentication fails, it returns the error message.
 * It then retrieves and validates the course module ID ('cmid') and the desired visibility state ('visible') from the input parameters.
 * It ensures the 'cmid' exists in the database and checks that 'visible' is a valid boolean value (0 or 1).
 * If all validations pass, it updates the visibility of the course module. If any parameter is invalid or the update fails,
 * it returns an error response. The method handles exceptions and reports them as general errors.
 *
 * @return mixed Returns 1 if the visibility update is successful, 0 if it fails, or an error message if any validation fails.
 *
 * Usage:
 * $result = $thisInstance->changevisibleactivity();
 * if ($result === 1) {
 *     echo 'Visibility updated successfully.';
 * } elseif ($result === 0) {
 *     echo 'Failed to update visibility.';
 * } else {
 *     echo 'Error: ' . $result;
 * }
 */
 public function changevisibleactivity() {
	 
      $auth=$this->checkAuth();
		if(!empty($auth)){return $auth;}
       $cmid= $this->getUtildata()->getVaueOfArray($this->getParam(),'cmid');
	   $visible= $this->getUtildata()->getVaueOfArray($this->getParam(),'visible');
	 
		global $DB;
        if (empty($cmid)) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.cmid.undefined');
        }
        if (!is_int((int) $cmid)) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.cmid.isnotnumber');
        }
		
        if (!$DB->record_exists('course_modules', array('id' => $cmid))) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.cmidnotexist', $cmid . ' not exist in database in the table course_modules');
        }
		
	
		if ($visible==="") {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.visible.undefined');
        }
		
		if ($visible < 0 && $visible > 1) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.visible.notbooleannumber','The param visible should has value 0  or 1');
        }
		
		 $result=0;
		
	   try {
            
          $result=$this->lib->change_visible_course_module($cmid,$visible);
		  if($result){$result=1;}
		  else{$result=0;}
           
       } catch (Exception $ex) {
            $this->getResponse()->danied('badiu.moodle.ws.error.general',$ex);
       }
      return $result;
   }
}
$badiunetws=new local_badiumnet_coursecontent();

?>
