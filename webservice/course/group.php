
<?php

require_once("$CFG->dirroot/group/lib.php");
require_once("$CFG->dirroot/local/badiunet/webservice/course/lib/grouplib.php");
require_once("$CFG->dirroot/local/badiunet/lib/webservicerole.php");
class local_badiunet_group extends local_badiunet_webservicerole  {

    private $lib;
    function __construct() {
        parent::__construct();
        $this->lib=new local_badiunet_lib_group();
    }

    public function create() {

        global $DB;
        $param=$this->getParam();
        if (!isset($param['name'])) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.name.undefined');
        }
        if (empty($param['name'])) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.name.empty');
        }

        if (!isset($param['courseid'])) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.courseid.undefined');
        }
        if (!is_int((int) $param['courseid'])) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.courseid.isnotnumber');
        }
        if (!$DB->record_exists('course', array('id' => $param['courseid']))) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.courseidnotexist', $param['courseid'] . ' not exist in database in the table course');
        }
        if ($DB->record_exists('groups', array('courseid' => $param['courseid'],'idnumber'=>$param['idnumber']))) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.idnumberjustexist', $param['idnumber'] . ' just exist in database in the table groups on course '.$param['courseid']);
        } 
        if ($DB->record_exists('groups', array('courseid' => $param['courseid'],'name'=>$param['name']))) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.namejustexist', $param['name'] . ' just exist in database in the table groups on course '.$this->getParam()['courseid']);
        } 
      
       
        $dto= (object)$param;
        try {
            $result = groups_create_group($dto);
        } catch (Exception $ex) {
            $this->getResponse()->danied('badiu.moodle.ws.error.general', $ex);
        }

        return $result;
    }

    
     public function getlist() {
        $name = null;
        $categoryid = null;
        if (isset($this->getParam()['name'])) {
            $name = $this->getParam()['name'];
        }
        if (isset($this->getParam()['courseid'])) {
            if (!is_int((int) $this->getParam()['courseid'])) {
                $this->getResponse()->danied('badiu.moodle.ws.error.param.courseid.isnotnumber');
            }
            $courseid = $this->getParam()['courseid'];
        }
        $offset = $this->getPaginationOffset();
        $limit = $this->getPaginationLimit();
        $result = array();

        try {

            $list = $this->lib->get_list($courseid, $name, $offset, $limit);

            foreach ($list as $value) {
                array_push($result, array('id' => $value->id, 'name' => $value->name));
            }
        } catch (Exception $ex) {
            $this->getResponse()->danied('badiu.moodle.ws.error.general', $ex);
        }
        return $result;
    }
}
$badiuwsdata = new local_badiunet_group();

?>
