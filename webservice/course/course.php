<?php 
require_once("$CFG->dirroot/local/badiunet/lib/webservicerole.php");
require_once("$CFG->dirroot/local/badiunet/lib/syskey.php");
require_once("$CFG->dirroot/course/externallib.php");
require_once($CFG->dirroot . '/backup/util/includes/backup_includes.php');
require_once("$CFG->dirroot/local/badiunet/webservice/course/lib/courselib.php");
class local_badiunet_webservice_course_course extends local_badiunet_webservicerole {
    private $lib;
 
    function __construct() {
          parent::__construct();
          
          $this->lib = new local_badiunet_lib_course();
    }
   

   public function backup(){
       
        $paramfile=array();
        $courseid= $this->getUtildata()->getVaueOfArray($this->getParam(),'courseid');
        $userinteraction= $this->getUtildata()->getVaueOfArray($this->getParam(),'userinteraction');
        $useridexec=$this->getUtildata()->getVaueOfArray($this->getParam(),'useridexec');
        if(empty($useridexec)){$useridexec=2;}
        global $DB;
        $luser = $DB->get_record('user', array('id'=>$useridexec)); 
        complete_user_login($luser);

        $interationtype=backup::INTERACTIVE_NO;
        if($userinteraction){$interationtype=backup::INTERACTIVE_YES;}
        $bc = new backup_controller(backup::TYPE_1COURSE, $courseid, backup::FORMAT_MOODLE,$interationtype, backup::MODE_SAMESITE, $useridexec);
        $bc->set_status(backup::STATUS_AWAITING);
        $bc->execute_plan();
        $result = $bc->get_results();
        if (isset($result['backup_destination']) && $result['backup_destination']) {
            $file = $result['backup_destination'];
            $context = context_course::instance($courseid);
            $fs = get_file_storage();
            $timestamp = time();

            $filerecord = array(
                'contextid' => $context->id,
                'component' => 'local_badiunet',
                'filearea' => 'backup',
                'itemid' => $timestamp,
                'filepath' => '/',
                'filename' => 'backup_course_'.$courseid,
                'timecreated' => $timestamp,
                'timemodified' => $timestamp
            );
            $storedfile = $fs->create_file_from_storedfile($filerecord, $file);
            $file->delete();

            // Make the link.
            
            $paramfile['contextid']=$storedfile->get_contextid();
            $paramfile['component']=$storedfile->get_component();
            $paramfile['filearea']= $storedfile->get_filearea();
            $paramfile['itemid']=$storedfile->get_itemid();
            $paramfile['filepath']=$storedfile->get_filepath();
            $paramfile['filename']=$storedfile->get_filename();
            
            
        } else {
            $this->setError(array('info'=>'badiu.moodle.net.error.coursebackupfailure','message'=>'Backup of course failure'));
            
        }
        return $paramfile;
       
    }

    public function restore(){
        
        $source= $this->getUtildata()->getVaueOfArray($this->getParam(),'source');
        $target= $this->getUtildata()->getVaueOfArray($this->getParam(),'target');

        $userinteraction= $this->getUtildata()->getVaueOfArray($this->getParam(),'userinteraction');
        $useridexec=$this->getUtildata()->getVaueOfArray($this->getParam(),'useridexec');
        if(empty($useridexec)){$useridexec=2;}
        global $DB,$CFG;
        $luser = $DB->get_record('user', array('id'=>$useridexec)); 
        complete_user_login($luser);

        $interationtype=backup::INTERACTIVE_NO;
        if($userinteraction){$interationtype=backup::INTERACTIVE_YES;}
        
        $storedfile=$this->downloadfile();
      return $storedfile;
        $fcp= $storedfile->get_contenthash();
        $pathdatroot =$CFG->dataroot;
        $f1=substr($fcp,0,2);
        $f2=substr($fcp,2,2);

        $fpath="$pathdatroot/filedir/$f1/$f2/$fcp";
       
        $syskey=new local_badiunet_syskey();
        $timestamp = time();
        $tempfolder=$syskey->make_hash(25);
        $tempfolder=$tempfolder.'.'.$timestamp;
        $targetpath="$pathdatroot/temp/backup/$tempfolder";

        if (!file_exists($targetpath)) {
            mkdir($targetpath, 0777, true);
        }
        return $targetpath;
         $resullt= $storedfile->extract_to_pathname(get_file_packer('application/vnd.moodle.backup'),$targetpath);
        //extract backup
        

        //create course to restore
        $coursename='Badiu 21';//;$this->getUtildata()->getVaueOfArray($this->getParam(),'target.newcourse.name',true);
        $courseshotname="b2122";//$this->getUtildata()->getVaueOfArray($this->getParam(),'target.newcourse.shortname',true);
        $coursecategoryid=5;//$this->getUtildata()->getVaueOfArray($this->getParam(),'target.newcourse.categoryid',true);
       
        $newcourseid = restore_dbops::create_new_course($coursename, $courseshotname, $coursecategoryid);
        return 122; 
        $rc = new restore_controller($targetpath, $newcourseid,$interationtype, backup::MODE_GENERAL, $useridexec, backup::TARGET_NEW_COURSE);

        $rc->execute_precheck();
        $rc->execute_plan();
        $rc->destroy();

        return $newcourseid;
       
    }

    private function downloadfile(){
        $keyauth = $this->getUtildata()->getVaueOfArray($this->getParam(),'source.keys._keyautdbcheckk.name',true);
        $valueauth = $this->getUtildata()->getVaueOfArray($this->getParam(),'source.keys._keyautdbcheckk.value',true);
        $keyauthadtional =$this->getUtildata()->getVaueOfArray($this->getParam(),'source.keys._keyautdbcheckkadtional.name',true); 
        $valueauthadtional = $this->getUtildata()->getVaueOfArray($this->getParam(),'source.keys._keyautdbcheckkadtional.value',true); 
        $keygetfileconfig = $this->getUtildata()->getVaueOfArray($this->getParam(),'source.keys._keyautdbcheckkfconfig.name',true); 
        $fileurl = $this->getUtildata()->getVaueOfArray($this->getParam(),'source.url',true);

        $url="$fileurl/local/badiunet/synchttp/expfile.php?_keyautdbcheckk=$keyauth&_valueautdbcheckv=$valueauth&_keyautdbcheckkadtional=$keyauthadtional&_valueautdbcheckvadtional=$valueauthadtional&_keyautdbcheckkfconfig=$keygetfileconfig";
  return $url;
        //downlaod backup
        $timestamp = time();
        $filerecord = array(
        'contextid' => 1,
        'component' => 'local_badiunet',
        'filearea'  => 'restoure',
        'itemid'    => $timestamp,
        'filepath'  => '/',
        'filename'  => 'course_moodle_2_'.$timestamp,
        'timecreated' => $timestamp,
        'timemodified' => $timestamp
        );
        $fs = get_file_storage();
 
        $storedfile = $fs->create_file_from_url($filerecord,$url, null, true);
        return  $storedfile;
    }
   

    public function create() {

        global $DB;
        if (!isset($this->getParam()['name'])) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.name.undefined');
        }
        if (empty($this->getParam()['name'])) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.name.empty');
        }

        if (!isset($this->getParam()['shortname'])) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.shortname.undefined');
        }
        if (empty($this->getParam()['shortname'])) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.shortname.empty');
        }
        if ($DB->record_exists('course', array('shortname' => $this->getParam()['shortname']))) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.shortnamejustexist', $this->getParam()['shortname'] . ' just exist in database in the table course');
        }

        if (!isset($this->getParam()['categoryid'])) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.categoryid.undefined');
        }
        if (!is_int((int) $this->getParam()['categoryid'])) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.categoryid.isnotnumber');
        }
        if (!$DB->record_exists('course_categories', array('id' => $this->getParam()['categoryid']))) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.categoryidnotexist', $this->getParam()['categoryid'] . ' not exist in database in the table course_categories');
        }



        //check permission review permission
        $this->login();
        $permission = has_capability('moodle/user:create', context_system::instance());
        if (!$permission) {
            $this->getResponse()->danied('badiu.moodle.ws.error.nopermission', 'moodle/user:create');
        }


        $cparam['fullname'] = $this->getParam()['name'];
        $cparam['shortname'] = $this->getParam()['shortname'];
        $cparam['categoryid'] = $this->getParam()['categoryid'];

        $result = null;
        try {
            $result = $this->lib->create($cparam);
        } catch (Exception $ex) {
            $this->getResponse()->danied('badiu.moodle.ws.error.general', $ex);
        }

        return $result;
    }
 public function update() {
        global $DB;
		$id=$this->getUtildata()->getVaueOfArray($this->getParam(),'id');
		$name=$this->getUtildata()->getVaueOfArray($this->getParam(),'name');
		
		if(empty($id)){ $this->getResponse()->danied('badiu.moodle.ws.error.param.id.isrequired');}
		if(!is_int((int)$id)){ $this->getResponse()->danied('badiu.moodle.ws.error.param.id.isnotnumber');}
		
        if(empty($name)){ $this->getResponse()->danied('badiu.moodle.ws.error.param.name.empty');}
        
		$aparam=array();
		$aparam['id']=$id;
		$aparam['fullname']=$name;
		$result=null;
        try {
              $result=$this->lib->update($aparam);
           } catch (Exception $ex) {
                $this->getResponse()->danied('badiu.moodle.ws.error.general',$ex);
           }
        
        return $result;
    }
	
	 public function updateofferstatus() {
        global $DB;
		
		$id = $this->getUtildata()->getVaueOfArray($this->getParam(),'id');
		$visible = $this->getUtildata()->getVaueOfArray($this->getParam(),'visible');
		$startdate = $this->getUtildata()->getVaueOfArray($this->getParam(),'startdate');
		$enddate = $this->getUtildata()->getVaueOfArray($this->getParam(),'enddate');
				
		if(empty($id)){ $this->getResponse()->danied('badiu.moodle.ws.error.param.id.isrequired');}
		if(!is_int((int)$id)){ $this->getResponse()->danied('badiu.moodle.ws.error.param.id.isnotnumber');}
		
		if ($visible !== null && $visible !== '' && !is_int($visible)) { $this->getResponse()->danied('badiu.moodle.ws.error.param.visible.isnotnumber');}
		if ($startdate !== null && $startdate !== '' && !is_int($startdate)) { $this->getResponse()->danied('badiu.moodle.ws.error.param.startdate.isnotnumber');}
		if ($enddate !== null && $enddate !== '' && !is_int($enddate)) { $this->getResponse()->danied('badiu.moodle.ws.error.param.enddate.isnotnumber');}

		
		$aparam=array();
		$aparam['id']=$id;
		$aparam['visible']=$visible;
		$aparam['startdate']=$startdate;
		$aparam['enddate']=$enddate;
		$result=null;
        try {
              $result=$this->lib->update_offer_status($aparam);
           } catch (Exception $ex) {
                $this->getResponse()->danied('badiu.moodle.ws.error.general',$ex);
           }
        
        return $result;
    }
	
	 public function addenrolmethodmanual() {
        global $DB;
		
		$courseid = $this->getUtildata()->getVaueOfArray($this->getParam(),'courseid');
				
		if(empty($courseid)){ $this->getResponse()->danied('badiu.moodle.ws.error.param.courseid.isrequired');}
		if(!is_numeric($courseid)){ $this->getResponse()->danied('badiu.moodle.ws.error.param.courseid.isnotnumber');}
	 
	  if (!$DB->record_exists('course', array('id' => $courseid))) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.courseidnotexist', $courseid . ' not exist in database in the table course');
        }
		
		$aparam=array();
		$aparam['courseid']=$courseid;
		$result=null;
        try {
              $result=$this->lib->add_enrol_method_manual($aparam);
           } catch (Exception $ex) {
                $this->getResponse()->danied('badiu.moodle.ws.error.general',$ex);
           }
        
        return $result;
    }
	public function removeduplicategradeitemtypecourse() {
        global $DB;
		
		$courseid = $this->getUtildata()->getVaueOfArray($this->getParam(),'courseid');
				
		if(empty($courseid)){ $this->getResponse()->danied('badiu.moodle.ws.error.param.courseid.isrequired');}
		if(!is_numeric($courseid)){ $this->getResponse()->danied('badiu.moodle.ws.error.param.courseid.isnotnumber');}
	 
	  if (!$DB->record_exists('course', array('id' => $courseid))) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.courseidnotexist', $courseid . ' not exist in database in the table course');
        }
		
		$aparam=array();
		$aparam['courseid']=$courseid;
		$result=null;
        try {
              $result=$this->lib->remove_duplicate_gradeitemtypecourse($aparam);
           } catch (Exception $ex) {
                $this->getResponse()->danied('badiu.moodle.ws.error.general',$ex);
           }
        
        return $result;
    }
    public function createloop() {
        
        global $DB;
        if (!isset($this->getParam()['name'])) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.name.undefined');
        }
        if (empty($this->getParam()['name'])) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.name.empty');
        }

        if (!isset($this->getParam()['shortname'])) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.shortname.undefined');
        }
        if (empty($this->getParam()['shortname'])) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.shortname.empty');
        }
        if ($DB->record_exists('course', array('shortname' => $this->getParam()['shortname']))) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.shortnamejustexist', $this->getParam()['shortname'] . ' just exist in database in the table course');
        }

        if (!isset($this->getParam()['categoryid'])) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.categoryid.undefined');
        }
        if (!is_int((int) $this->getParam()['categoryid'])) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.categoryid.isnotnumber');
        }
        if (!$DB->record_exists('course_categories', array('id' => $this->getParam()['categoryid']))) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.categoryidnotexist', $this->getParam()['categoryid'] . ' not exist in database in the table course_categories');
        }

        if (!isset($this->getParam()['limit'])) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.limit.undefined');
        }
        if (!is_int((int) $this->getParam()['limit'])) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.limit.isnotnumber');
        }


        //check permission review permission
        $this->login();
        $permission = has_capability('moodle/user:create', context_system::instance());
        if (!$permission) {
            $this->getResponse()->danied('badiu.moodle.ws.error.nopermission', 'moodle/user:create');
        }
          $cont = 0;
          $process = 0;
          $limit = $this->getParam()['limit'];
        for ($index = 0; $index < $limit; $index++) {
              $cont++;
              $cparam=array();
             $cparam['fullname'] = $this->getParam()['name'].' - '.$cont;
             $cparam['shortname'] = $this->getParam()['shortname'].'-'.$cont;
             $cparam['categoryid'] = $this->getParam()['categoryid'];
             
             try {
                   $result = $this->lib->create($cparam);
                    if ($result > 0) {$process++;}
             } catch (Exception $ex) {
                    $this->getResponse()->danied('badiu.moodle.ws.error.general', $ex);
             }
        }
       
        return $process;
    }

    public function getlist() {
        $name = null;
        $categoryid = null;
        if (isset($this->getParam()['name'])) {
            $name = $this->getParam()['name'];
        }
        if (isset($this->getParam()['categoryid'])) {
            if (!is_int((int) $this->getParam()['categoryid'])) {
                $this->getResponse()->danied('badiu.moodle.ws.error.param.categoryid.isnotnumber');
            }
            $categoryid = $this->getParam()['categoryid'];
        }
        $offset = $this->getPaginationOffset();
        $limit = $this->getPaginationLimit();
        $result = array();

        try {

            $list = $this->lib->get_list($name, $categoryid, $offset, $limit);

            foreach ($list as $value) {
                array_push($result, array('id' => $value->id, 'name' => $value->name));
            }
        } catch (Exception $ex) {
            $this->getResponse()->danied('badiu.moodle.ws.error.general', $ex);
        }
        return $result;
    }

    public function getname() {
        $result = array();
        $id = null;

        global $DB;
        if (!isset($this->getParam()['id'])) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.id.undefined');
        }
        if (empty($this->getParam()['id'])) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.id.empty');
        }
        if (!is_int((int) $this->getParam()['id'])) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.id.isnotnumber');
        }
        if (!$DB->record_exists('course', array('id' => $this->getParam()['id']))) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.courseidnotexist', $this->getParam()['id'] . ' not exist in database in the table course');
        }
        $id = $this->getParam()['id'];

        try {

            $name = $this->lib->get_name($id);
        } catch (Exception $ex) {
            $this->getResponse()->danied('badiu.moodle.ws.error.general', $ex);
        }
        return $name;
    }

    public function duplicate() {
        $sourcecourseid = null;
        $name = null;
        $shortname = null;
        $categoryid = null;

        global $DB;
        if (!isset($this->getParam()['sourcecourseid'])) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.sourcecourseid.undefined');
        }
        if (!is_int((int) $this->getParam()['sourcecourseid'])) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.sourcecourseid.isnotnumber');
        }
        if (!$DB->record_exists('course', array('id' => $this->getParam()['sourcecourseid']))) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.courseidnotexist', $this->getParam()['sourcecourseid'] . ' not exist in database in the table course');
        }

        if (!isset($this->getParam()['name'])) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.name.undefined');
        }
        if (empty($this->getParam()['name'])) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.name.empty');
        }

        if (!isset($this->getParam()['shortname'])) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.shortname.undefined');
        }
        if (empty($this->getParam()['shortname'])) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.shortname.empty');
        }
        if ($DB->record_exists('course', array('shortname' => $this->getParam()['shortname']))) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.shortnamejustexist', $this->getParam()['shortname'] . ' just exist in database in the table course');
        }

        if (!isset($this->getParam()['categoryid'])) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.categoryid.undefined');
        }
        if (!is_int((int) $this->getParam()['categoryid'])) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.categoryid.isnotnumber');
        }
        if (!$DB->record_exists('course_categories', array('id' => $this->getParam()['categoryid']))) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.categoryidnotexist', $this->getParam()['categoryid'] . ' not exist in database in the table course_categories');
        }



        //check permission review permission
        $this->login();
        $permission = has_capability('moodle/site:config', context_system::instance());
        if (!$permission) {
            $this->getResponse()->danied('badiu.moodle.ws.error.nopermission', 'moodle/course:create');
        }

        $sourcecourseid = $this->getParam()['sourcecourseid'];
        $name = $this->getParam()['name'];
        $shortname = $this->getParam()['shortname'];
        $categoryid = $this->getParam()['categoryid'];

        $result = null;
        try {
            $cextlib = new core_course_external();
			 $options = array(

                array('name' => 'activities', 'value' => 1),

                array('name' => 'blocks', 'value' => 1),

                array('name' => 'filters', 'value' => 1),

                array('name' => 'users', 'value' => 0),

                array('name' => 'role_assignments', 'value' => 0),

                array('name' => 'comments', 'value' => 0),

                array('name' => 'logs', 'value' => 0),

            );
		$visible=1;
            $result = $cextlib->duplicate_course($sourcecourseid, $name, $shortname, $categoryid,$visible,$options);
        } catch (Exception $ex) {
            $this->getResponse()->danied('badiu.moodle.ws.error.general', $ex);
        }
        $result = $result['id'];
        return $result;
    }

    public function duplicateloop() {
        $sourcecourseid = null;
        $name = null;
        $shortname = null;
        $categoryid = null;
        $limit = null;
        global $DB;
        if (!isset($this->getParam()['sourcecourseid'])) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.sourcecourseid.undefined');
        }
        if (!is_int((int) $this->getParam()['sourcecourseid'])) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.sourcecourseid.isnotnumber');
        }
        if (!$DB->record_exists('course', array('id' => $this->getParam()['sourcecourseid']))) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.courseidnotexist', $this->getParam()['sourcecourseid'] . ' not exist in database in the table course');
        }

        if (!isset($this->getParam()['name'])) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.name.undefined');
        }
        if (empty($this->getParam()['name'])) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.name.empty');
        }

        if (!isset($this->getParam()['shortname'])) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.shortname.undefined');
        }
        if (empty($this->getParam()['shortname'])) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.shortname.empty');
        }
        if ($DB->record_exists('course', array('shortname' => $this->getParam()['shortname']))) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.shortnamejustexist', $this->getParam()['shortname'] . ' just exist in database in the table course');
        }

        if (!isset($this->getParam()['categoryid'])) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.categoryid.undefined');
        }
        if (!is_int((int) $this->getParam()['categoryid'])) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.categoryid.isnotnumber');
        }
        if (!$DB->record_exists('course_categories', array('id' => $this->getParam()['categoryid']))) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.categoryidnotexist', $this->getParam()['categoryid'] . ' not exist in database in the table course_categories');
        }

        if (!isset($this->getParam()['limit'])) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.limit.undefined');
        }
        if (!is_int((int) $this->getParam()['limit'])) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.limit.isnotnumber');
        }


        //check permission review permission
        $this->login();
        $permission = has_capability('moodle/site:config', context_system::instance());
        if (!$permission) {
            $this->getResponse()->danied('badiu.moodle.ws.error.nopermission', 'moodle/course:create');
        }

        $sourcecourseid = $this->getParam()['sourcecourseid'];
        $name = $this->getParam()['name'];
        $shortname = $this->getParam()['shortname'];
        $categoryid = $this->getParam()['categoryid'];
        $limit = $this->getParam()['limit'];
        $result = null;
        $cont = 0;
        try {
            $cextlib = new core_course_external();
            for ($index = 0; $index < $limit; $index++) {
                $cont++;
                $result = $cextlib->duplicate_course($sourcecourseid, $name . ' - ' . $cont, $shortname . '-' . $cont, $categoryid);
            }
        } catch (Exception $ex) {
            $this->getResponse()->danied('badiu.moodle.ws.error.general', $ex);
        }

        return $cont;
    }


   public function import() {
        $sourcecourseid = $this->getUtildata()->getVaueOfArray($this->getParam(),'sourcecourseid');
        $targetcourseid = $this->getUtildata()->getVaueOfArray($this->getParam(),'targetcourseid');
        $deletecontent = $this->getUtildata()->getVaueOfArray($this->getParam(),'deletecontent');
		if(empty($deletecontent)){$deletecontent=0;}
        $targetcoursevisible = $this->getUtildata()->getVaueOfArray($this->getParam(),'targetcoursevisible');
		$targetcoursestartdate = $this->getUtildata()->getVaueOfArray($this->getParam(),'targetcoursestartdate');
		$targetcourseenddate = $this->getUtildata()->getVaueOfArray($this->getParam(),'targetcourseenddate');
		$targetcourseaddmanualenrolmethod = $this->getUtildata()->getVaueOfArray($this->getParam(),'targetcourseaddmanualenrolmethod');
		

        global $DB;
        if (empty($sourcecourseid)) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.sourcecourseid.undefined');
        }
        if (!is_int((int) $sourcecourseid)) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.sourcecourseid.isnotnumber');
        }
        if (!$DB->record_exists('course', array('id' => $sourcecourseid))) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.courseidnotexist', $sourcecourseid . ' not exist in database in the table course');
        }


		if (empty($targetcourseid)) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.targetcourseid.undefined');
        }
        if (!is_int((int) $targetcourseid)) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.targetcourseid.isnotnumber');
        }
        if (!$DB->record_exists('course', array('id' => $targetcourseid))) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.courseidnotexist', $targetcourseid . ' not exist in database in the table course');
        }
		
       

        //check permission review permission
        $this->login();
        $permission = has_capability('moodle/site:config', context_system::instance());
        if (!$permission) {
            $this->getResponse()->danied('badiu.moodle.ws.error.nopermission', 'moodle/course:create');
        }

        

        $result = null;
        try {
            $cextlib = new core_course_external();
			 $options = array(); 
		
			if($deletecontent){
				
				$meparam=array();
				$meparam['courseid']=$targetcourseid;
				$rdigtcresult=$this->lib->remove_duplicate_gradeitemtypecourse($meparam);
			}
            $result = core_course_external::import_course($sourcecourseid, $targetcourseid, $deletecontent, $options);
			$aparam=array();
			$aparam['id']=$targetcourseid;
			$aparam['visible']=$targetcoursevisible;
			$aparam['startdate']=$targetcoursestartdate;
			$aparam['enddate']=$targetcourseenddate;
			
			$resultac=$this->lib->update_offer_status($aparam);
			
			if($targetcourseaddmanualenrolmethod){
				 $meparam=array();
				 $meparam['courseid']=$targetcourseid;
				 $resultme=$this->lib->add_enrol_method_manual($meparam);
			}
        } catch (Exception $ex) {
            $this->getResponse()->danied('badiu.moodle.ws.error.general', $ex);
        }
        if(empty($result)){$result=1;}
        return $result;
    }

/**
 * Deletes a course from Moodle based on the 'courseid' parameter.
 *
 * This method first retrieves the 'courseid' from a parameter array provided by a utility method. It then performs several
 * checks: it ensures the course ID is provided, is a numeric value, and corresponds to an existing course in the database.
 * The user's permissions are checked to ensure they have the capability to delete courses. If all checks pass, the course
 * is deleted using a Moodle core function. If any check fails, an error is reported via a response handling method.
 *
 * @throws Exception If an error occurs during the course deletion process, it catches and reports it.
 * @return mixed The result of the deletion process. Returns 1 if the deletion was attempted but no specific result was returned.
 *
 * Usage:
 * $result = $thisInstance->remove();
 * if ($result == 1) {
 *     echo 'Course deleted successfully.';
 * } else {
 *     echo 'Failed to delete course.';
 * }
 */
 public function remove() {
	    $auth=$this->checkAuth();
		if(!empty($auth)){return $auth;}
		
        $courseid = $this->getUtildata()->getVaueOfArray($this->getParam(),'courseid');
      

        global $DB;
        if (empty($courseid)) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.courseid.undefined');
        }
        if (!is_int((int) $courseid)) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.courseid.isnotnumber');
        }
        if (!$DB->record_exists('course', array('id' => $courseid))) {
            $this->getResponse()->danied('badiu.moodle.ws.error.param.courseidnotexist', $courseid . ' not exist in database in the table course');
        }

       

        //check permission review permission
        $this->login();
        $permission = has_capability('moodle/site:config', context_system::instance());
        if (!$permission) {
            $this->getResponse()->danied('badiu.moodle.ws.error.nopermission', 'moodle/course:delete');
        }

        

        $result = null;
		#session_write_close();
        try {
             $courses = array($courseid);
			$result = core_course_external::delete_courses($courses);
			if(isset($result['warnings']) && sizeof($result['warnings'])==0){$result=1;} 
        } catch (Exception $ex) {
            $this->getResponse()->danied('badiu.moodle.ws.error.general', $ex);
        }
        if(empty($result)){$result=1;}
        return $result;
    }	
}
$badiunetws=new local_badiunet_webservice_course_course();


?>
