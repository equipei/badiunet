<?php 
require_once("$CFG->dirroot/local/badiunet/lib/webservicerole.php");
class local_badiunet_webservice_course_offer extends local_badiunet_webservicerole {
    
    private $reportsend;

    function __construct() {
          parent::__construct();
          
         
    }
   

   public function changetimestart(){
        $courses= $this->getUtildata()->getVaueOfArray($this->getParam(),'courses');
        global $DB;
        $result=0;
        if(!empty($courses) && sizeof($courses)>0){
            foreach ($courses as $row) {
                $id=$this->getUtildata()->getVaueOfArray($row,'id');
                $timestart=$this->getUtildata()->getVaueOfArray($row,'timestart');
                if($id > 0 && $timestart > 0){
                    $dto=new stdClass();
                    $dto->id=$id;
                    $dto->startdate=$timestart;
                    $r=$DB->update_record("course", $dto);
                    if($r){$result++;}
                }
               
            }
        }
       
        return $result;
       
    }

    public function changetimestartandenroltimestart(){
        
        $courses= $this->getUtildata()->getVaueOfArray($this->getParam(),'courses');
        $enablefuturedateenrol= $this->getUtildata()->getVaueOfArray($this->getParam(),'enablefuturedateenrol');
        if(empty($enablefuturedateenrol)){$enablefuturedateenrol=false;}
        
        global $DB;
        $result=0;
        if(!empty($courses) && sizeof($courses)>0){
            foreach ($courses as $row) {
                $id=$this->getUtildata()->getVaueOfArray($row,'id');
                $timestart=$this->getUtildata()->getVaueOfArray($row,'timestart');
                if($id > 0 && $timestart > 0){
                    $updateenrol=false;
                    if($timestart < time()){$updateenrol=true;}
                    if($timestart > time() && $enablefuturedateenrol){$updateenrol=true;}
                    $dto=new stdClass();
                    $dto->id=$id;
                    $dto->startdate=$timestart;
                    $r=$DB->update_record("course", $dto);
                    
                    if($updateenrol){
                        $listenrolid=$this->get_enrolids($id);
                        
                        foreach ($listenrolid as $elrow) {
                            $enrolid=$elrow->id;
                           $this->change_enrol_timestar($enrolid, $timestart);
                        }
                    }
                   
                    if($r){$result++;}
                }
                
            }
        }
       
        return $result;
       
    }

   private function get_enrolids($courseid) {
        global $DB, $CFG;
        $sql = "SELECT id FROM {$CFG->prefix}enrol  WHERE courseid=$courseid AND status=0";
        $rows = $DB->get_records_sql($sql,null,0,500);
        return $rows;
    }  

   private function change_enrol_timestar($enrolid, $timestart) {
        if(empty($enrolid)){return null;}
        if(empty($timestart)){return null;}
        global $DB, $CFG;
        $sql = "UPDATE {$CFG->prefix}user_enrolments SET timestart=$timestart WHERE enrolid=$enrolid ";
        $r = $DB->execute($sql);
        return $r;
    }
}
$badiunetws=new local_badiunet_webservice_course_offer();


?>
