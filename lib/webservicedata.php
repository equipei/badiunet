<?php 
require_once("$CFG->dirroot/local/badiunet/lib/response.php");

class local_badiunet_webservicedata  {
   
    private $param = null; 
    private $folder;
    private $file;
    private $function;
    private $key;
    private $iskeyvalid;
    private $response;
    private $error=null;
   function __construct($param=null) {
       if(!empty($param)){$this->param=$param;}
       $this->folderbase=null;
       $this->folderrelative=null;
       $this->file=null;
       $this->function=null;
       $this->key=null;
       $this->iskeyvalid=false;
      $this->init();
       
   }
   function init() {
      
      if(isset($this->param['_key'])){$this->key=$this->param['_key'];}
      $this->iskeyvalid=$this->isKeyValid($this->key);
      if($this->iskeyvalid){
           $p=explode('.',$this->key);
           $size=sizeof($p);
          
            if(isset($p[0]) && isset($p[1])){
                $this->folderbase="/".$p[0]."/".$p[1]."/webservice";
            }
           $this->folder=$p[0];
          $cont=0;
          $this->folderrelative="";
           foreach($p as $f){
                if($cont >1 && $cont <$size-2){
                    $this->folderrelative.="/$f";
                }
                $cont++;
           }
           $filepos=$size-2;
           if(isset($p[$filepos])){$this->file='/'.$p[$filepos];}
           
           $functionpos=$size-1;
           if(isset($p[$functionpos])){$this->function=$p[$functionpos];}
       }
       
    }
    
   function exec() {
        global $CFG;
        $function=$this->function;
      
        $filepath=$CFG->dirroot.$this->folderbase.$this->folderrelative.$this->file.".php";
       
        if(file_exists($filepath)){
            global $badiunetws;
           
            require_once($filepath);
            $existfunction=method_exists($badiunetws,$function);
          
            if(!$existfunction){
                $info='moodle.local.badiunet.error.functionnotexist';
                $message="function not found";
                $this->response->danied($info,$message);
            }else{
                 $param=$this->param;
                  unset($param['_key']);
                  unset($param['_token']);
                  
                 $badiunetws->setParam($param);
                 $result=$badiunetws->$function();
                 $this->setError($badiunetws->getError());
                 
                 return $result;
                
            }
           
        }else{
           
            $info='moodle.local.badiunet.error.keynotvalid';
            $message="file not found";
            $this->response->danied($info,$message);
        }
        
        
    }
    
    function isKeyValid($key) {
       
        if(empty($key)){return false;}
        $key=trim($key);
        $count=substr_count($key, '.');
        if($count >=3){
              $p=explode('.',$key);
             if(isset($p[0]) && !empty($p[0])
                && isset($p[1]) &&  !empty($p[1])
                && isset($p[2]) &&  !empty($p[2])
                && isset($p[3]) &&  !empty($p[3])){return true;}
        }
        return false;
    }
     public function response($code) {
        $this->code=$code;
        $msg=utf8_encode($this->getMessage($code));
        $response=new local_badiuws_response();
        $response->danied($this->code,$msg);
    }
      
   
    function getParam() {
        return $this->param;
    }

    function setParam($param) {
        $this->param = $param;
    }

    function getResponse() {
        return $this->response;
    }

    function setResponse($response) {
        $this->response= $response;
    }

    public function getError() {
        return $this->error;
    }

    public function setError($error) {
        $this->error = $error;
    }

}
?>
