<?php 
class local_badiunet_theme {
    private $key="_BADIUNET_SYSTEM_MDL_THEME_INFO";
    private $userid;
   
     
    function __construct($userid,$force=false) {
       
        $this->userid=$userid;
        $this->key=$this->key."_".$this->userid;
        $this->init($force);
     }
      
     function get() {
        if(isset($_SESSION[$this->key]) && !empty($_SESSION[$this->key])){
            return $_SESSION[$this->key];
        }
        return null;
     }
     
     function init($force=false) {
        
         if(empty($this->userid)){return null;}
        if($force){
            if(!isset($_SESSION[$this->key])){$_SESSION[$this->key]=null;}
            else{$_SESSION[$this->key]=null;}
        }
        if(isset($_SESSION[$this->key]) && !empty($_SESSION[$this->key])){
            return $_SESSION[$this->key];
        }
       $infotheme=array();
       $sitetheme=$this->getSite();
     
       $isusercustom=$this->isUserCustom();
       if($isusercustom){$sitetheme=$this->getUser();}  
       $version=$this->version($sitetheme);
       $parents=$this->parents($sitetheme);
       $infotheme['name']= $sitetheme;
       $infotheme['version']= $version;
       $infotheme['parents']= $parents;
       $_SESSION[$this->key]=$infotheme;    
       
        return $infotheme;
    }
     
     function getSite() {
        global $DB, $CFG;
        $sql = "SELECT value FROM {$CFG->prefix}config  WHERE name='theme'";
        $row = $DB->get_record_sql($sql);
        $value = null;
        if (!empty($row)) {
            $value = $row->value;
        }
        return $value;
     }

     function isUserCustom() {
        global $DB, $CFG;
        $sql = "SELECT value FROM {$CFG->prefix}config  WHERE name='allowuserthemes'";
        $row = $DB->get_record_sql($sql);
        $value = null;
        if (!empty($row)) {
            $value = $row->value;
        }
        return $value;
     }
     function getUser() {
        global $DB, $CFG;
        $userid=$this->userid;
        if(empty($userid)){return null;}
        $sql = "SELECT theme FROM {$CFG->prefix}user WHERE id=$userid";
        $row = $DB->get_record_sql($sql);
        $value = null;
        if (!empty($row)) {
            $value = $row->theme;
        }
        return $value;
     }
     

     function version($theme) {
         global $CFG;
         $fpath="$CFG->dirroot/theme/$theme/version.php";
        if(!file_exists($fpath)){return null;}
		$plugin=new stdClass();
		$plugin->version="";
        require_once($fpath);
		return $plugin->version;
     }
     function parents($theme) {
        global $CFG;
        $fpath="$CFG->dirroot/theme/$theme/config.php";
       if(!file_exists($fpath)){return null;}
	   $THEME=new stdClass();
	   $THEME->parents="";
       require_once($fpath);
        if(is_array($THEME->parents)){$result=implode(",", $THEME->parents);}
        return $result;

    }
    
     function getUserid() {
         return $this->userid;
     }

     function setUserid($userid) {
         $this->userid = $userid;
     }


}

?>
