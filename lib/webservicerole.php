<?php 
require_once("$CFG->dirroot/local/badiunet/lib/utildata.php");
require_once("$CFG->dirroot/local/badiunet/lib/response.php");
class local_badiunet_webservicerole {
     
    private $param = null;
    private $utildata;
    private $error=null;
	 private $response;
    function __construct() {
        $this->utildata=new local_badiunet_utildata();
		$this->response=new local_badiunet_response();
    }
    
    public function login() {
            global $DB;
           $user = $DB->get_record('user', array('username'=>'admin')); 
           complete_user_login($user);
      }
      
     public function checkAuth() {
		 $auth=$this->getUtildata()->getVaueOfArray($this->getParam(),'_auth');
		 if(empty($auth)){return $this->getResponse()->danied('badiu.moodle.ws.error.param.auth.undefined','Set param _auth for autentication');}
		
		$plugin=new local_badiunet_pluginconfig('local_badiunet');  
        $dconfig=$plugin->getValue('dconfig');
		if(empty($dconfig)){return $this->getResponse()->danied('badiu.moodle.ws.error.config.auth.undefined','Set configuration of param auht in moodle plugin');}
		
		$util=new local_badiunet_util();
		$dconfig=$util->getJson($dconfig);
		
		if(empty($dconfig)){return $this->getResponse()->danied('badiu.moodle.ws.error.config.formatinvalid','Set configuration of param auht in moodle plugin is not valid');}
		
		$timestart=$this->getUtildata()->getVaueOfArray($dconfig,'auth.general.timestart',true);
		$timeend=$this->getUtildata()->getVaueOfArray($dconfig,'auth.general.timeend',true);
		$param=$this->getUtildata()->getVaueOfArray($dconfig,'auth.general.param',true);
		
		if(!empty($timestart) && $timestart > time()){return $this->getResponse()->danied('badiu.moodle.ws.error.config.authtimestartfuture','The configuration of auth timestart is future date');}
		if(!empty($timeend) && $timeend < time()){return $this->getResponse()->danied('badiu.moodle.ws.error.config.authtimeendpast','The configuration of auth timeend is past date');}
		
		if(!is_array($param)){return $this->getResponse()->danied('badiu.moodle.ws.error.config.authparamisnotlist','The configuration of auth param is not a list');}
		 foreach ($param as $k => $v) {
			 $clientvalue=$this->getUtildata()->getVaueOfArray($auth,$k);
			 if($clientvalue!=$v){return $this->getResponse()->danied('badiu.moodle.ws.error.param.authparamnotmatch','The configuration of auth param not match with auth param send');}
		 }
		return null;
		 
	 }
    function getParam() {
        return $this->param;
    }

    function setParam($param) {
        $this->param = $param;
    }

      function getUtildata() {
        return $this->utildata;
    }

    function setUtildata($utildata) {
        $this->utildata = $utildata;
    }

    public function getError() {
        return $this->error;
    }

    public function setError($error) {
        $this->error = $error;
    }
	
	 function getResponse() {
        return $this->response;
    }

    function setResponse($response) {
        $this->response= $response;
    }
	
	   function getPaginationOffset() {
       $offset=0;
       if(isset($this->getParam()['offset']) && (int)$this->getParam()['offset'] >=0 ){
           $offset=(int)$this->getParam()['offset'];
      }
     return $offset;
    }
     function getPaginationLimit() {
       $limit=10;
       if(isset($this->getParam()['limit']) && (int)$this->getParam()['limit'] >=0 ){
           $limit=(int)$this->getParam()['limit'];
      }
     return $limit;
    }
}


?>
