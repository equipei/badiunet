<?php

 class local_badiunet_pluginconfig  {
 	
      /**
     * @var string
     */
    private $name;
 	
    function __construct($name) {
       $this->name=$name;
    }
  
    function getAllAsArray() {
         global $CFG,$DB;
       $sql="SELECT name,value FROM {$CFG->prefix}config_plugins WHERE  plugin ='".$this->getName()."'";
        $rows=$DB->get_records_sql($sql);
        $list=array();
       if(!empty($rows)){
            foreach ($rows as $row) {
                  $list[$row->name]=$row->value;
            }
      }
      return $list;
    }   
	
	 function getValue($key) {
       global $CFG,$DB;
       $sql="SELECT value FROM {$CFG->prefix}config_plugins WHERE  plugin ='".$this->getName()."' AND name= '".$key."'";
       $row=$DB->get_record_sql($sql);
	   
	   if(!empty($row)) return $row->value;
       return null;
    }
	

	/**
 * Retrieves all configuration settings for the current plugin as an associative array.
 *
 * This function fetches all records for the current plugin from the `config_plugins` table
 * and returns them as an associative array. Results can be cached in the user's session
 * for improved performance if `$usersession` is enabled.
 *
 * @param bool $usersession Optional. Determines whether to use session caching. Defaults to `true`.
 *
 * @global object $CFG Global configuration object, used to access the database prefix.
 * @global object $DB Global database object, used to execute SQL queries.
 * 
 * @return array An associative array of configuration settings for the plugin.
 *               Keys are the configuration names, and values are their corresponding values.
 */
function getAll($usersession = true) {
    $key = '_local_badiunet_plugin_session_' . $this->getName();
    if ($usersession) {
        // Check if the data is already cached in the user's session.
        if (isset($_SESSION[$key])) {
            $list = $_SESSION[$key];
            if (is_array($list) && sizeof($list) > 0) {
                return $list;
            }
        }
    }

    global $CFG, $DB;
    // Prepare the SQL query to fetch all plugin configurations.
    $sql = "SELECT * FROM {$CFG->prefix}config_plugins WHERE plugin = :plugin";
    $fparam = array('plugin' => $this->getName());
    $rows = $DB->get_records_sql($sql, $fparam);

    $result = array();
    if (is_array($rows)) {
        foreach ($rows as $row) {
            $result[$row->name] = $row->value;
        }
    }

    // Cache the results in the user's session if session caching is enabled.
    $_SESSION[$key] = $result;

    return $result;
}

    function save($key,$value) {
          global $DB;
        $dto=new stdClass;
        $dto->plugin=$this->getName();
        $dto->name=$key;
        $dto->value=$value;
        return $DB->insert_record('config_plugins', $dto);
    } 
     function edit($key,$value) {
         
          global $DB;
        $dto=new stdClass;
        $dto->id=$this->get_id($key);
        $dto->plugin=$this->getName();
        $dto->name=$key;
        $dto->value=$value;
        return $DB->update_record('config_plugins', $dto);
    } 
    
    function add($key,$value) {
			if($value==NULL){$value= " ";}
           if($this->exist($key)){       
                 $this->edit($key,$value) ;   
           }else{
                $this->save($key,$value);
           }
      }
     function exist($key) {
       global $CFG,$DB;
       $sql="SELECT COUNT(id) AS countrecord FROM {$CFG->prefix}config_plugins WHERE  plugin='".$this->getName()."' AND name='".$key."'";
       $r=$DB->get_record_sql($sql);
        return $r->countrecord;
    }
    function existkeyvalue($key,$value) {
       global $CFG,$DB;
       $sql="SELECT COUNT(id) AS countrecord FROM {$CFG->prefix}config_plugins WHERE  plugin='".$this->getName()."' AND name='".$key."' AND value='".$value."'";
       $r=$DB->get_record_sql($sql);
        return $r->countrecord;
    }
    
     function get_id($key) {
          global $CFG,$DB;
       $sql="SELECT id FROM {$CFG->prefix}config_plugins WHERE  plugin='".$this->getName()."' AND name='".$key."'";
       $r=$DB->get_record_sql($sql);
        return $r->id;
    } 
    
    function getKey($value) {
          global $CFG,$DB;
       $sql="SELECT MAX(name) AS name FROM {$CFG->prefix}config_plugins WHERE  plugin='".$this->getName()."' AND value='".$value."'";
       $r=$DB->get_record_sql($sql);
        return $r->name;
    } 
    
    public function getName() {
        return $this->name;
    }

    public function setName($name) {
        $this->name = $name;
    }


 }