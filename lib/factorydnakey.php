<?php
require_once("$CFG->dirroot/local/badiunet/lib/syskey.php");
class local_badiunet_factorydnakey {

    public static $KEY_BADIUNET_REGISTER = 'bnetcoreservicekeyregistertdnasystem';

    //
    function __construct() {
        
    }

    function set() {
        if ($this->exist()) {
            return null;
        }
        global $DB;
        $data = new stdClass();
        $data->name = self::$KEY_BADIUNET_REGISTER;
        $data->value = $this->makeKey();
        $result = $DB->insert_record('config', $data);
        if ($result) {
            return $data->value;
        }
        return null;
    }

    function get() {
        global $CFG, $DB;
        $key = self::$KEY_BADIUNET_REGISTER;
        $sql = "SELECT value FROM {$CFG->prefix}config WHERE  name= '" . $key . "'";
        $row = $DB->get_record_sql($sql);

        if (!empty($row)) {
            return $row->value;
        }
        return null;
    }

    public function exist() {
        global $DB, $CFG;
        $key = self::$KEY_BADIUNET_REGISTER;
        $sql = "SELECT COUNT(id) AS countrecord FROM {$CFG->prefix}config WHERE name='" . $key . "'";
        $r = $DB->get_record_sql($sql);
        if (!empty($r)) {
            return $r->countrecord;
        }
        return false;
    }

    public function makeKey() {
        $mkey = new local_badiunet_syskey();
        $now = time();
        $basekey = "|$now";
        $lenth = strlen($basekey);
        $lenth = 150 - $lenth;
        $thash = $mkey->make_hash($lenth);
        $token = $thash . $basekey;
        return $token;
    }

}
