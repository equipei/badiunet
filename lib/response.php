<?php
require_once("$CFG->dirroot/local/badiunet/lib/util.php");
require_once("$CFG->dirroot/local/badiunet/lib/templetecript.php");
class local_badiunet_response  { 
    
        public static  $REQUEST_ACCEPT   ="accept";
        public static  $REQUEST_DENIED   ="danied";
	private $status;
	private $info;
	private $message;
       private $criptyidentify;
       
    function __construct() {
                $this->status='';
		$this->info='';
		$this->message='';
                $this->criptyidentify =0;
  }
       
      
      public function get() {
			$response=array();
			$response['status']=$this->status;
			$response['info']=$this->info;
			$response['message']=$this->message;
                       return $response;
      }
      public function accept($message,$info='',$senddata=true) {
            
			$this->setStatus(self::$REQUEST_ACCEPT);
			$this->setInfo($info);
                        $this->setMessage($message);
                        if($senddata){
                             $json = json_encode($this->get());
                            $json=$this->encriptMessage($json);
                            header("Content-Type: application/json;charset=utf-8");
                            echo $json; 
                            exit;
                        }
      }
       public function danied($info,$message='',$senddata=true) {
			$this->setStatus(self::$REQUEST_DENIED);
			$this->setInfo($info);
                        $this->setMessage($message);
                        if($senddata){
                             $json = json_encode($this->get());
                            header("Content-Type: application/json;charset=utf-8");
                            $json=$this->encriptMessage($json);
                            echo $json; 
                            exit;
                        }
      }
     
     public function encriptMessage($message) {
              $tcript=new local_badiunet_templatecript();
              $criptytype=$tcript->getCriptyType();
              if(empty( $criptytype)){return $message;}
              if($criptytype=='s1' || $criptytype=='k1' || $criptytype=='k2' || $criptytype=='ks'){
                  $message=$tcript->encode($criptytype,$message,$this->criptyidentify);
              }
              return $message;
     }
              
    public function getStatus() {
          return $this->status;
      }


      public function setStatus($status) {
          $this->status = $status;
      }
	  
	   public function getInfo() {
          return $this->info;
      }


      public function setInfo($info) {
          $this->info = $info;
      }
	   public function getMessage() {
          return $this->message;
      }


      public function setMessage($message) {
          $this->message = $message;
      }

      function getCriptyidentify() {
          return $this->criptyidentify;
      }

      function setCriptyidentify($criptyidentify) {
          $this->criptyidentify = $criptyidentify;
      }


}
