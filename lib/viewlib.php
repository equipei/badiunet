<?php

/**
 * Viewlib from module Badiu.Net Client
 *
 * @package    local_badiunet
 * @copyright  Badiu 2023
 */
require_once("$CFG->dirroot/local/badiunet/lib/utildata.php");
class local_badiunet_viewlib {
	 private $utildata;
    function __construct() {
        $this->utildata=new local_badiunet_utildata();
	
    }
	
	function makeCard($list) {
      if(!is_array($list)){return null;}
	
	 $html="";
	 $html.="<div class=\"row badiu-row\">";

    foreach ($list as $row) {
		$cardhead=$this->getUtildata()->getVaueOfArray($row,'cardhead');
		$cardlink=$this->getUtildata()->getVaueOfArray($row,'cardlink');
		$cardlinktarget=$this->getUtildata()->getVaueOfArray($row,'cardlinktarget');
		$carddescription=$this->getUtildata()->getVaueOfArray($row,'carddescription');
	
		$item="
		<div class=\"col-sm-6 col-md-4 mb-2 badiu-col\">
        <div class=\"card badiu-card  h-100 d-flex flex-column\">
            <div class=\"card-header\">
                <a href=\"$cardlink\" $cardlinktarget>$cardhead</a>
             </div>
            <div class=\"card-body flex-grow-1\">
               $carddescription
            </div>
        </div>
		</div>
		
		
		";
		$html.=$item;
	
      }
	 
	 $html.="</div>";
	 
	return 	$html; 
    }

	
	function getUtildata() {
        return $this->utildata;
    }

    function setUtildata($utildata) {
        $this->utildata = $utildata;
    }
}
