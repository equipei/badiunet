<?php
class local_badiunet_formutil  {

     
	function __construct(){
		
	}
	
			
	public function option_yes_not(){
 		$op=array();
                $op[0]=get_string('no','local_badiunet');
                $op[1]=get_string('yes','local_badiunet');
                return $op;
 	}
       
	  public function label_yes_not($value){
 		  if($value==1){return get_string('yes','local_badiunet');}
		  else if($value==0){return get_string('no','local_badiunet');}
          else return get_string('no','local_badiunet');
 	}
	
/**
 * Returns an array with the available environment levels.
 *
 * @return array An associative array where the key is the level identifier and the value is the corresponding translated string.
 */
public function option_enviromentlevel() {
    $op = array();
    $op['level1'] = get_string('enviromentlevel1', 'local_badiunet');
    $op['level2'] = get_string('enviromentlevel2', 'local_badiunet');
    $op['level3'] = get_string('enviromentlevel3', 'local_badiunet');
    return $op;
}


/**
 * Returns the translated label for a specific environment level value.
 *
 * @param string $value The environment level identifier (e.g., 'level1', 'level2', 'level3').
 * @return string|null The translated string corresponding to the environment level or null if the value does not match any level.
 */
public function label_enviromentlevel($value) {
    if ($value == 'level1') {
        return get_string('enviromentlevel1', 'local_badiunet');
    } else if ($value == 'level2') {
        return get_string('enviromentlevel2', 'local_badiunet');
    } else if ($value == 'level3') {
        return get_string('enviromentlevel3', 'local_badiunet');
    }
    return null;
}

/**
 * Generates an array of options for chat agent levels.
 *
 * This function retrieves strings defined in the localization files to create
 * an associative array of agent level options. These options can be used in
 * forms or dropdown menus.
 *
 * @return array An associative array where keys are option values and values are localized strings.
 */
public function option_levelchatagent() {
    $op = array();
    $op[200] = get_string('levelsharechatagent', 'local_badiunet');
    $op[10] = get_string('levelcustomchatagent', 'local_badiunet');
   
    return $op;
}

/**
 * Returns the label corresponding to a specific chat agent level value.
 *
 * This function maps a given agent level value to its corresponding localized string.
 * If the value does not match any predefined level, it returns null.
 *
 * @param int $value The agent level value (e.g., 200 for standard, 10 for custom).
 * @return string|null The localized label for the agent level or null if the value is not valid.
 */
public function label_levelchatagent($value) {
    if ($value == 200) {
        return get_string('evelsharechatagent', 'local_badiunet');
    } else if ($value == 10) {
        return get_string('levelcustomchatagent', 'local_badiunet');
    } 
    return null;
}
        public function option_keysync_user(){
				$op=array();
				$op['']='';
                $op['username']=get_string('username','local_badiunet');
				$op['idnumber']=get_string('idnumber','local_badiunet');
				$op['email']=get_string('email','local_badiunet');
                
                return $op;
 	}

    public function option_systemdtype(){
				$op=array();
				$op['']='';
                $op['badiugc2']=get_string('badiugc2','local_badiunet');
			   return $op;
 	}	
	
	public function label_systemdtype($value){
 		  if($value=='badiugc2'){return get_string('badiugc2','local_badiunet');}
		  return null;
 	}
	
	public function token_options(){
				global $CFG;
				require_once("$CFG->dirroot/local/badiunet/app/sserver/dblib.php");
				
				$dblib = new local_badiunet_app_sserver_dblib();
				$listss=$dblib->get_name_servicekeyinstance();
				
				$op=array();
				$op['']='';
				$op['defaultoken']=get_string('defaultoken','local_badiunet');
                if(is_array($listss)){
				  foreach ($listss as $row){
					  $op[$row->servicekeyinstance]=$row->name;
				  } 
			    }
				
                return $op;
 	}	
}