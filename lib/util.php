<?php
require_once("$CFG->dirroot/local/badiunet/lib/httpquerystring.php");
class local_badiunet_util {
    private $lastipconnection;
    function __construct() {
        $this->lastipconnection=null;
    }

    public function getJson($input, $outArray=true) {
        $result = null;
        $max_int_length = strlen((string) PHP_INT_MAX) - 1;
        $json_without_bigints = preg_replace('/:\s*(-?\d{' . $max_int_length . ',})/', ': "$1"', $input);
        $result = json_decode($json_without_bigints, $outArray);
        return $result;
    }
/*
 * $out 0 - ojbect | 1- array | 2 - original text
 */
    public function request($url,$pdata=null,$out = 1) {
       $data = $this->requestService($url,$pdata);
      // print_r($data);exit;    
       if($this->isResponseError($data)){return $data;}
       if($out==2){return $data;}
       $outarray=false;
       if($out){$outarray=true;}
        $result = $this->getJson($data, $outarray);
        return $result;
    }

    public function requestService($url,$pdata=null) {
        $host=null;
        $param=null;
          
        if(empty($pdata)){
            $h=$this->splitHostParam($url);
            $host=$h->host;
            $param=$paramjson = json_encode($h->param);
        }else if(is_array($pdata)){
             $host=$url;
             $param=json_encode($pdata);
        }else{
           
           $param= $pdata;
        }
   
        $ch = curl_init();
       
       // echo $host;
        //print_r($param);
        curl_setopt($ch, CURLOPT_URL, $host);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $param);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        $this->lastipconnection=curl_getinfo($ch,CURLINFO_PRIMARY_IP);
        curl_close($ch);
        return $result;
       
    }

    function splitHostParam($url) {
        $address=new stdClass(); 
        $address->host="";
        $address->param="";
         $pos=strpos($url,'?');
         if ($pos === false) {
             $address->host=$url;
             return $address;
         }
      
        $p = explode("?", $url);
        if(isset($p[0])){$address->host=$p[0];}
        if(isset($p[1])){$address->param=$p[1];}
        if(!empty($address->param)){
            $querystr=new local_badiunet_httpquerystring($address->param);
            $address->param=$querystr->getParam();
        }
         
        return $address;
    }

    function getVlueOfArray($array, $key){
        $value=null;
        if(!is_array($array)){return null;}
         if (array_key_exists($key,$array)){
            $value= $array[$key];
         }
        return $value;
    }
    
    function isResponseError($response) {
        if(!is_array($response)){
             $response=$this->getJson($response, true);
        }
         if(is_array($response)){
               if ( array_key_exists('status',$response) 
                       && array_key_exists('info',$response) 
                       && array_key_exists('message',$response) ){return false;}
                  
           }
         return true;
       } 
       
    function plugininfo() {
        global $CFG;
        $fpath="$CFG->dirroot/local/badiunet/version.php";
       if(!file_exists($fpath)){return null;}
	   $plugin=new stdClass();
		$plugin->version="";
		$plugin->release="";
       require_once($fpath);
       if(isset($plugin)){
            $info=array('name'=>'badiunet','version'=>$plugin->version,'release'=>$plugin->release);
            return $info;
       }
       
        return null;

    }
	
	function getDefaultLang() {
        global $CFG;
       $defaultlang=$CFG->lang;
	   if(isset($_SESSION['SESSION']->lang) && !empty($_SESSION['SESSION']->lang)){$defaultlang=$_SESSION['SESSION']->lang;}
	   else if(isset($_SESSION['USER']->lang) && !empty($_SESSION['USER']->lang)){$defaultlang=$_SESSION['USER']->lang;}
        return $defaultlang;
    }
       function getLastipconnection() {
           return $this->lastipconnection;
       }

       function setLastipconnection($lastipconnection) {
           $this->lastipconnection = $lastipconnection;
       }


 function castStringToArray($text, $separator=',') {
		if($text==""){return null;}
		if(empty($separator)){return null;}
		$result=array();
		 $pos=stripos($text, $separator);
         if($pos=== false){
            $result=array($text);
        }else{
             $result= explode($separator, $text);
        }
		
		return $result;
	}

/**
 * Retrieves the original client's IP address considering possible proxy usage.
 *
 * This function checks a list of common HTTP headers used to pass the client's IP address through proxies.
 * It filters out private and reserved IP addresses to ensure that only a public IP address is returned.
 *
 * The function iterates through several server variables to find the client's IP address,
 * validating each one to ensure it is a public IP.
 * If no valid IP is found in the headers, it defaults to using the REMOTE_ADDR server variable.
 *
 * @return string The IP address of the original client, or the server's REMOTE_ADDR if no valid client IP is found.
 */
function getClientIp() {
    $ip_keys = array(
        'HTTP_CLIENT_IP', 
        'HTTP_X_FORWARDED_FOR', 
        'HTTP_X_FORWARDED', 
        'HTTP_X_CLUSTER_CLIENT_IP', 
        'HTTP_FORWARDED_FOR', 
        'HTTP_FORWARDED', 
        'REMOTE_ADDR'
    );

    foreach ($ip_keys as $key) {
        if (array_key_exists($key, $_SERVER) === true) {
            foreach (explode(',', $_SERVER[$key]) as $ip) {
                $ip = trim($ip);
                // Validate the IP and ensure it's not a private or reserved range
                if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) !== false) {
                    return $ip;
                }
            }
        }
    }
    // Default to REMOTE_ADDR if no valid IP is found
	if(isset($_SERVER['REMOTE_ADDR'])){ return $_SERVER['REMOTE_ADDR'];}
	return 'ipserverromentenotfound';
   
}

/**
 * Gets the context dynamically based on the current page and global variables.
 *
 * This function determines the appropriate context (system, course, or activity)
 * depending on the page where the user is currently navigating.
 *
 * @global stdClass $COURSE The global course object for the current course.
 * @global moodle_page $PAGE The global page object for the current page.
 * @return context The determined context (system, course, or activity).
 */
function get_dynamic_context() {
    global $COURSE, $PAGE;
	
	$uri= $this->clean_uri();
  
    // Check if it's the system context
    if (empty($COURSE->id) || $COURSE->id == SITEID) {
		return context_system::instance();
    }

    // Check if it's the course context
    if (isset($COURSE->id) && $COURSE->id != SITEID && strpos($PAGE->url->out(), '/course/view.php') !== false) {
		return context_course::instance($COURSE->id);
    }

    // Check if it's the activity context
    if (isset($PAGE->cm->id)) {
		 return context_module::instance($PAGE->cm->id);
    }
    // Returns the system context as default if unable to determine
    return context_system::instance();
}	

/**
 * Cleans the current URI by removing the host portion and redundant slashes.
 * 
 * This function processes the `REQUEST_URI` server variable, compares it with
 * the global `wwwroot` setting from the `$CFG` configuration, and removes the 
 * host portion and unnecessary segments if they match the base path of the host.
 * 
 * @global object $CFG Global configuration object containing the `wwwroot` setting.
 * 
 * @return string The cleaned URI, with the host and redundant segments removed if 
 *                applicable, or the original URI if no changes are needed.
 */
    public function clean_uri(){
         $uri=$_SERVER['REQUEST_URI'];
         global $CFG;
         $host=$CFG->wwwroot;
         $p=explode("//",$host);
         if(!isset($p[1])){ return $uri;}
         $shost=explode("/",$p[1]); 
         
         $suri=explode("/",$uri);
         
         $cont=0;
         $newuri="";
         foreach ($suri as $key => $value) {
            
                $phost=null;
                if(isset($shost[$key])){$phost=$shost[$key];}
                if($key > 0 ){
                    if(empty($value)){$newuri.='/';$cont++;}
                    else if($phost!=$value){
                        if($newuri!='/'){$newuri.='/'.$value;$cont++;}
                    }
                    
                    
                }
                    
                
            
           
         }
         if($cont==0){ return $uri;}
		
        return $newuri;
     }
}
