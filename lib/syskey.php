<?php
class local_badiunet_syskey  {
    
      
      public function make_hash($numberchar){ 
                $listchar='ABCDEFGHIJKLMOPQRSTUVXWYZ0123456789abcdefghijklmntuvxyz'; 
		$coutnchar = strlen($listchar); 
		$coutnchar--; 
		$thash=NULL; 
		for($x=1;$x<=$numberchar;$x++){ 
			$pos = rand(0,$coutnchar); 
			$thash .= substr($listchar,$pos,1); 
		} 
		return $thash; 
	} 
     
      
     public function verifying_digit_sum_template1($txt){
                $caractere= str_split($txt);
                $peso=3;
		$soma=0;
		$w=0;
		$z=0;
		for ($y=0;$y<sizeof($caractere);$y++)
			{	
				$w=ord($caractere[$y]);
				$z=$w*$peso;
				$soma+=$z;
				$peso++;
				if($peso>8){$peso=3;}
			}
		return $soma;
      }
      
        public function verifying_digit_sum_template2($txt){
           $caractere= str_split($txt);
           $y=  sizeof($caractere);
	   $peso=9;
           $soma=0;
	    $w=0;
	    $z=0;
	   while($y>0)
			{	
				$w=intval($caractere[$y-1]);
				$z=$w*$peso;
				$soma+=$z;
				$y--;
				$peso--;
				if($peso<4){$peso=9;}
			}
		return $soma;
       }
       
       //verifying digit
       public function verifying_digit_template11($numbersum){
           	$dv=0;
		$resto=$numbersum%11;
		$dv=11-$resto;
		if($dv==0 || $dv==10 || $dv==11){$dv=1;}
	
		return $dv;
          
       }
       
        public function  make_verifying_digit($txt,$limitdigit=30){
           $sum1=$this->verifying_digit_sum_template1($txt);
          $dv1=$this->verifying_digit_template11($sum1);
          $ntxt=$txt."".$dv1;
          $value="";
          for($i=0;$i<=$limitdigit-1;$i++){
              $sumd=$this->verifying_digit_sum_template2($ntxt);
              $dvd=$this->verifying_digit_template11($sumd);
              $ntxt=$ntxt."".$dvd;
              $value=$value."".$dvd;
          }
          return $value;
            
       }   

  
    
}
