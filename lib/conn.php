<?php
require_once("$CFG->dirroot/local/badiunet/lib/netlib.php");
class local_badiunet_conn {

   public $remoteip;
    function __construct() {
        $this->remoteip=null;
    }

function request($host,$param,$outarray=false) {
       
        $paramjson = json_encode($param);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $host);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $paramjson);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($ch);
        $this->remoteip = curl_getinfo($ch,CURLINFO_PRIMARY_IP);
        curl_close($ch);
       // return  $result;
        $obj=$this->convertData($result,$outarray);
       if($hotip){ $obj->ip= $ip;}
       // print_r($obj);exit;
        return $obj;
    }
    function convertData($data,$outarray=false) {
        $max_int_length = strlen((string) PHP_INT_MAX) - 1;
        $json_without_bigints = preg_replace('/:\s*(-?\d{' . $max_int_length . ',})/', ': "$1"', $data);
        $oresult = json_decode($json_without_bigints, $outarray);
        return $oresult;
    }
    
 
      function existToken($token) {
         $exist=false;
         $netlib=new local_badiunet_netlib();
         $defaulttoken=$netlib->getToken();
         if($token==$defaulttoken){$exist=true;}
        return $exist;
     }
     
     function getRemoteip() {
         return $this->remoteip;
     }

     function setRemoteip($remoteip) {
         $this->remoteip = $remoteip;
     }


}

?>
