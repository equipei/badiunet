<?php 
class local_badiunet_role {
    private $key="_BADIUNET_SYSTEM_MDL_USER_ROLES_SHORTNAME";
    private $userid;
   
     
    function __construct($userid,$force=false) {
        $this->userid=$userid;
        $this->key=$this->key."_".$this->userid;
        $this->init($force);
     }
      
     function get() {
        if(isset($_SESSION[$this->key]) && !empty($_SESSION[$this->key])){
            return $_SESSION[$this->key];
        }
        return null;
     }
     
     function init($force=false) {
        
         if(empty($this->userid)){return null;}
        if($force){
            if(!isset($_SESSION[$this->key])){$_SESSION[$this->key]=null;}
            else{$_SESSION[$this->key]=null;}
        }
        if(isset($_SESSION[$this->key]) && !empty($_SESSION[$this->key])){
            return $_SESSION[$this->key];
        }
        
         $roles="";
        $isadmin = $this->isAdmin();
        
        if($isadmin){$roles="admin";}
        else{
            $roles=$this->getListDb();
        }
        $_SESSION[$this->key]=$roles;
       
        return $roles;
    }
     
     function isAdmin() {
        $userid=$this->userid;
        global $DB, $CFG;
        $sql = "SELECT value FROM {$CFG->prefix}config  WHERE name='siteadmins'";
        $row = $DB->get_record_sql($sql);
        $value = null;
        $result = false;
        if (!empty($row)) {
            $value = $row->value;
        }

        if (empty($value)) {
            return $result;
        }

        $pos = stripos($value, ",");
        if ($pos === false) {
            if ($userid == $value) {
                $result = true;
            }
        } else {
            $luser = explode(",", $value);
            foreach ($luser as $uid) {
                if ($uid == $userid) {
                    $result = true;
                    break;
                }
            }
        }
        return $result;
     }

     function getListDb() {
        global $DB, $CFG;
        $userid=$this->userid;
        $sql = "SELECT DISTINCT r.shortname FROM {$CFG->prefix}role r INNER JOIN {$CFG->prefix}role_assignments rs ON r.id=rs.roleid WHERE rs.userid=$userid";
        $rows = $DB->get_records_sql($sql);
        $value = "";
      
        if (!empty($rows)) {
            $cont=0;
            foreach ($rows as $row){
               
                $shortname=$row->shortname; 
                if($cont==0){ $value=$shortname;}
                else { $value.=",".$shortname;}
                $cont++;
            }
           
        }
         return $value;
     }
     
     function getUserid() {
         return $this->userid;
     }

     function setUserid($userid) {
         $this->userid = $userid;
     }


}

?>
