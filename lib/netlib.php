<?php
require_once("$CFG->dirroot/local/badiunet/lib/pluginconfig.php"); 
require_once("$CFG->dirroot/local/badiunet/app/sserver/dblib.php");
require_once("$CFG->dirroot/local/badiunet/lib/utildata.php");
class local_badiunet_netlib  {

     /**
     * @var string
     */
    private $appkeyinstance;
      /**
     * @var string
     */
    private $token;
	
	  /**
     * @var string
     */
    private $url;
    
	  /**
     * @var string
     */
    private $defaultmodulekey;
    
    	  /**
     * @var string
     */
    private $defaultmoduleurlparam;
	
  /**
     * @var string
     */
    private $autosyncuser;
    
    /**
     * @var string
     */
    private $syncpermission;
    
    /**
     * @var string
     */
    private $listipallowed;
	
	 /**
     * @var string
     */
    private $enviroment;
	 /**
     * @var integer
     */
    private $serviceenable;
	
	function __construct($appkeyinstance=null){
        $this->setAppkeyinstance($appkeyinstance);
		$this->initConfig($appkeyinstance);
	}
	public function initConfig($appkeyinstance=null) {
         if(empty($appkeyinstance)){
            $plugin=new local_badiunet_pluginconfig('local_badiunet');  
            $this->token=$plugin->getValue('servicetoken');
            $this->token=$this->clean($this->token);
			$this->serviceenable=$plugin->getValue('serviceenable');
			$this->enviroment=$plugin->getValue('enviroment');
			
            $this->url=$plugin->getValue('serviceurl');
            if(empty($this->url)){$this->url='https://mreport1.badiu.com.br';}
            $this->defaultmodulekey=$plugin->getValue('defaultmodulekey');
            $this->defaultmoduleurlparam=$plugin->getValue('defaultmoduleurlparam');
            $this->autosyncuser=$plugin->getValue('autosyncuser');
            $this->syncpermission=$plugin->getValue('syncpermission');
            $this->listipallowed=$plugin->getValue('serverrmoteipallowed');
            $this->addModuleIntanceOnUrlParam();
            $this->castIpallowedToArray();
         }else{
            $dblib = new local_badiunet_app_sserver_dblib();
            $utildata=new local_badiunet_utildata();
            $rowintace=$dblib->get_by_servicekeyinstance($appkeyinstance);
			$rowintace=(array)$rowintace;
			$this->token=$utildata->getVaueOfArray($rowintace,'servicetoken');
            $this->token=$this->clean($this->token);
			$this->serviceenable=$utildata->getVaueOfArray($rowintace,'sstatus');
			$this->enviroment=$utildata->getVaueOfArray($rowintace,'enviroment');
            $this->url=$utildata->getVaueOfArray($rowintace,'serviceurl');
            $this->defaultmodulekey=$utildata->getVaueOfArray($rowintace,'defaultmodulekey');
            $this->defaultmoduleurlparam=$utildata->getVaueOfArray($rowintace,'defaultmoduleurlparam'); //review this field not exist
            $this->syncpermission=$utildata->getVaueOfArray($rowintace,'syncpermission');//review this field not exist
            $this->listipallowed=$utildata->getVaueOfArray($rowintace,'serverrmoteipallowed');
            $this->addModuleIntanceOnUrlParam();
            $this->castIpallowedToArray();
         }
		
	}
	 public function initConfigKey($key,$appkeyinstance=null) {
		 $value=null;
		 if(empty($appkeyinstance)){
            $plugin=new local_badiunet_pluginconfig('local_badiunet');  
            $value=$plugin->getValue($key);
		 }else{
            $dblib = new local_badiunet_app_sserver_dblib();
            $value=$dblib->get_comun_by_servicekeyinstance($appkeyinstance,$key);
		 }
		 return $value; 
	 }
        
	 public function getModuleInstance() {
			$token=$this->token;
			$p=explode("|",$token);
			
			$instance=0;
			if(isset($p[4])){$instance=$p[4];}
          return $instance;
      }
      public function getEntityInstance() {
			$token=$this->token;
			$p=explode("|",$token);
			
			$instance=0;
			if(isset($p[0])){$instance=$p[0];}
          return $instance;
      }  
       public function addModuleIntanceOnUrlParam() {
           if(!empty( $this->defaultmoduleurlparam)){
                $serviceid=$this->getModuleInstance();
                $this->defaultmoduleurlparam=str_replace('__token_serviceid',$serviceid,$this->defaultmoduleurlparam);   
          }
            
       }
         public function getUrlService() {
          return $this->url."/system/service/process";;
      }
       public function clean($str) {
            if(!empty($str)){
                $str=trim($str);
                $str= str_replace("\\t",'',$str);
                $str= str_replace(" ",'',$str);
            }
           return $str; 
        }
       public function castIpallowedToArray() {
           if(empty($this->listipallowed)){return array();}
           $list=array();
            $pos=stripos($this->listipallowed, ",");
              if($pos=== false){
                  $list=array($this->listipallowed);
              }else{
                  $list= explode(",", $this->listipallowed);
              }
           $this->listipallowed=$list;
       }
       public function isIpAllowed($ip) {
           if (in_array($ip, $this->listipallowed)){return true;}
           return false;    
        }
    /*
	Convert param separate by commar or by breakline to array
	*/
	public function castParamListToArray($param) {
           if(empty($param)){return array();}
           $list=array();
            $pos=stripos($param, ",");
            if($pos!== false){
                $list= explode(",", $param);
				return $list;
			}
			$list= preg_split('/\r\n|[\r\n]/', $param);
			if(is_array($list) && sizeof($list) > 0){return $list;}
			
			if(!empty($param)){ $list=array($param);}
          return $list;
       }
       public function getToken() {
          return $this->token;
      }

      public function setToken($token) {
          $this->token = $token;
      }
	  
	  public function getUrl() {
          return $this->url;
      }

      public function setUrl($url) {
          $this->url = $url;
      }

	   public function getDefaultmodulekey() {
          return $this->defaultmodulekey;
      }

      public function setDefaultmodulekey($defaultmodulekey) {
          $this->defaultmodulekey = $defaultmodulekey;
      }
	  
	   public function getAutosyncuser() {
          return $this->autosyncuser;
      }

      public function setAutosyncuser($autosyncuser) {
          $this->autosyncuser = $autosyncuser;
      }
      
      function getDefaultmoduleurlparam() {
          return $this->defaultmoduleurlparam;
      }

      function setDefaultmoduleurlparam($defaultmoduleurlparam) {
          $this->defaultmoduleurlparam = $defaultmoduleurlparam;
      }

      function getSyncpermission() {
          return $this->syncpermission;
      }

      function setSyncpermission($syncpermission) {
          $this->syncpermission = $syncpermission;
      }

      function getListipallowed() {
          return $this->listipallowed;
      }

      function setListipallowed($listipallowed) {
          $this->listipallowed = $listipallowed;
      }

      function getAppkeyinstance() {
        return $this->appkeyinstance;
    }

    function setAppkeyinstance($appkeyinstance) {
		$this->appkeyinstance = $appkeyinstance;
    }

/**
 * Get the value of enviroment
 *
 * @return string
 */
public function getEnviroment()
{
    return $this->enviroment;
}

/**
 * Set the value of enviroment
 *
 * @param string $enviroment
 * @return self
 */
public function setEnviroment($enviroment)
{
    $this->enviroment = $enviroment;
    return $this;
}

/**
 * Get the value of serviceenable
 *
 * @return integer
 */
public function getServiceenable()
{
    return $this->serviceenable;
}

/**
 * Set the value of serviceenable
 *
 * @param integer $serviceenable
 * @return self
 */
public function setServiceenable($serviceenable)
{
    $this->serviceenable = $serviceenable;
    return $this;
}
}
