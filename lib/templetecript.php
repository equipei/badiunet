<?php
require_once("$CFG->dirroot/local/badiunet/lib/cript.php");
require_once("$CFG->dirroot/local/badiunet/lib/pluginconfig.php");
require_once("$CFG->dirroot/local/badiunet/lib/util.php");
require_once("$CFG->dirroot/local/badiunet/fcservice/maccess.php");
require_once("$CFG->dirroot/local/badiunet/app/sserver/dblib.php");
class local_badiunet_templatecript {
    
    private $type=null;
    private $identify=0;
    private $cript = null;
    private $criptk1 = null;
    private $criptk2 = null;
    private $criptks = null;
	private $appkeyinstance;
	private $iscripted;
    function __construct($appkeyinstance=null) {
		$this->setAppkeyinstance($appkeyinstance);
         $this->cript = new local_badiunet_cript();
		 $this->iscripted=false;
     }
    
    function decode($content) {
		if(!is_string($content)){return $content;} 
        $util = new local_badiunet_util();
      $p= explode("|",$content);
      $type=$util->getVlueOfArray($p, 0);
     
      $identify=$util->getVlueOfArray($p, 1);
      $this->identify=$identify;
      if(!empty($type)){
		   $allowedValues = array('s1', 'k1', 'k2', 'ks');
		   if(!in_array($type, $allowedValues)) { $this->iscripted=false;return $content;}
          $lenghscriptconf=strlen("$type|$identify|");
          $lenghscontent=strlen($content);
          $content=substr($content,$lenghscriptconf, $lenghscontent);  
          $ckey=$this->getCriptKey($type,$identify);
          $this->cript->setKey($ckey);
         
          $content=$this->cript->decode($content);
		  $this->iscripted=true;
      }
      return $content;
    }
    function encode($type,$content,$identify=0) {
        if(empty($identify)){$identify=$this->identify;}
        $ckey=$this->getCriptKey($type,$identify);
        $this->cript->setKey($ckey);
        $content=$this->cript->encode($content);
        $content="$type|$identify|$content";
        return $content;
    }
    
    function getCriptKey($type,$identify=0) {
        $ckey=null;
        if($type=='s1'){
            $ckey= $this->cript->getKey();
        }else if($type=='k1'){
            if(!empty($this->criptk1)){$ckey=$this->criptk1;}
            else{ 
				if(!empty($this->appkeyinstance)){
					$dblib = new local_badiunet_app_sserver_dblib();
					$ckey=$dblib->get_comun_by_servicekeyinstance($this->appkeyinstance,'criptk1');
				}else{
					$plugin=new local_badiunet_pluginconfig('local_badiunet');  
					$ckey= $plugin->getValue('criptk1');
				}
				
                
                $ckey=base64_decode($ckey);
                $this->criptk1=$ckey;
            }
        }else if($type=='k2'){
            if(!empty($this->criptk2)){$ckey=$this->criptk2;}
            else{
				if(!empty($this->appkeyinstance)){
					$dblib = new local_badiunet_app_sserver_dblib();
					$ckey=$dblib->get_comun_by_servicekeyinstance($this->appkeyinstance,'criptk2');
				}else{
					$plugin=new local_badiunet_pluginconfig('local_badiunet');  
					$ckey= $plugin->getValue('criptk2');
				}
                $ckey=base64_decode($ckey);
                 $this->criptk2=$ckey;
            }
           
        }else if($type=='ks'){
             if(!empty($this->criptks)){$ckey=$this->criptks;}
             else{
                 $maccess=new local_badiunet_maccess();
                 //$ckey=$maccess->getByKey('criptykey');
                 $ckey=$maccess->getCriptykey($identify);
                 $ckey=base64_decode($ckey);
                 $this->criptks=$ckey;
                
             }
           
        }
        
        return $ckey;
    }
    function getCriptyType($content=null) {
        if(empty($content)){$content=file_get_contents('php://input');}
        if(empty($content)){return null;}
        $util = new local_badiunet_util();
        $p= explode("|",$content);
        $type=$util->getVlueOfArray($p, 0);
        $identify=$util->getVlueOfArray($p, 1);
        $this->identify=$identify;
        return $type;
    }
    function getType() {
        return $this->type;
    }


    function getCript() {
        return $this->cript;
    }

    function setType($type) {
        $this->type = $type;
    }

    

    function setCript($cript) {
        $this->cript = $cript;
    }
  function getIdentify() {
        return $this->identify;
    }

    function setIdentify($identify) {
        $this->identify = $identify;
    }

    function getCriptk1() {
        return $this->criptk1;
    }

    function getCriptk2() {
        return $this->criptk2;
    }

    function getCriptks() {
        return $this->criptks;
    }

    function setCriptk1($criptk1) {
        $this->criptk1 = $criptk1;
    }

    function setCriptk2($criptk2) {
        $this->criptk2 = $criptk2;
    }

    function setCriptks($criptks) {
        $this->criptks = $criptks;
    }

 function getAppkeyinstance() {
        return $this->appkeyinstance;
    }

    function setAppkeyinstance($appkeyinstance) {
		$this->appkeyinstance = $appkeyinstance;
    }
	
	
	 // Getter for iscripted
    public function getIscripted() {
        return $this->iscripted;
    }

    // Setter for iscripted
    public function setIscripted($value) {
        $this->iscripted = $value;
    }
}

?>
