<?php 
require_once("$CFG->dirroot/local/badiunet/lib/utildata.php");
class local_badiunet_dblocallib {
    private $tableallowed=array('local_badiunet_session','local_badiunet_data','local_badiunet_course','local_badiunet_categories','local_badiunet_user','local_badiunet_user_enrol','local_badiunet_log','local_badiunet_log_sumarize');
    private $table;
    private $keys;
    private $error=null;
     
    function __construct($table,$keys=array()) {
        $this->table=$table;
        $this->keys=$keys;
    } 
      /**
       * This function check if table plugin table. 
       * Table of plugins starts with {prefix}local_badiunet_
       * If return false operaction is aborted
       * @return boolean
       */
     function isTableValid() {
        global $CFG;
        $result=false;
        $tbl=$CFG->prefix.$this->table;
        $tblpattern=$CFG->prefix."local_badiunet_";
       
        $pos=strpos($tbl,$tblpattern);
        if ($pos === false) {$result=false;}
        else if ($pos === 0) {$result=true;}
        if(!$result){$this->error=array('info'=>'badiu.moodle.net.error.tablenotallowed','message'=>'Table not start with local_badiunet');}
       
        if ($result  && !in_array($this->table, $this->tableallowed)){
                $this->error=array('info'=>'badiu.moodle.net.error.tablenotexist','message'=>'Table not exist');
            }
        
        return $result;
     }
 
     function add($row) {
        if(!empty($this->error)){return null;}
         $result=null;
        $exist=$this->exist($row);
        if($exist){$result=$this->editrow($row);}
        else {$result=$this->saverow($row);}
        return $result;
     }
     function saverow($row) {
        if(!empty($this->error)){return null;}
        global $DB;
       return $DB->insert_record($this->table, $row);
    }
    function saverows($rows) {
        if(!empty($this->error)){return null;}
       global $DB;
       $result=  $DB->insert_records($this->table, $rows);
       return $result;
    }
    function editrow($row) {
        if(!empty($this->error)){return null;}
        global $DB;
        $param=$this->addDataToKeys($row);
        if(!empty($this->error)){return null;}
        $row['id']=$DB->get_field($this->table,'id',$param);
        return $DB->update_record($this->table, $row);
    }
    function deleterows($filter) {
        if(empty($filter) || sizeof($filter)==0){$this->error=array('info'=>'badiu.moodle.net.error.delterowsfilterparamrequired','message'=>'Filter param is required to delete rows');}
        if(!empty($this->error)){return null;}
       global $DB;
       $result=  $DB->delete_records($this->table, $filter);
       return $result;
    }
    function deleteall() {
       if(!empty($this->error)){return null;}
       global $DB;
       $result=  $DB->delete_records($this->table);
       return $result;
    }
    function addDataToKeys($row) {
        
       
        $list=array();
        $utildata=new local_badiunet_utildata();
        $contkey=0;
        $contvalue=0;
        if(is_array($this->keys)){
            foreach ($this->keys as $k) {
                $key=$utildata->getVaueOfArray($k,'key');
                $value=$utildata->getVaueOfArray($row,$key);
                if(!array_key_exists($key,$row)){$this->error=array('info'=>'badiu.moodle.net.error.keysnotexistinrow','message'=>'Keys of not exist in row');break;}
               
                $list[$key]=$value;
                $contkey++;
                if($value !=null || $value!=""){$contvalue++;}
            }
         }
        
        if($contkey ==0){$this->error=array('info'=>'badiu.moodle.net.error.keystableisrequired','message'=>'Keys of tables is required');}
        if($contvalue < $contkey){$this->error=array('info'=>'badiu.moodle.net.error.missingvalueforkey','message'=>'missing value for key');}

        return $list;
    }
    function exist($row) {
        if(!empty($this->error)){return null;}
        global $DB;
        $param=$this->addDataToKeys($row);
        if(!empty($this->error)){return null;}
        $result= $DB->record_exists($this->table, $param);
        return $result;
    }

     function getTable() {
         return $this->table;
     }

     function setTable($table) {
         $this->table = $table;
     }

     function getKeys() {
        return $this->keys;
    }

    function setKeys($keys) {
        $this->keys = $keys;
    }


    public function getError() {
        return $this->error;
    }

    public function setError($error) {
        $this->error = $error;
    }
}

?>
