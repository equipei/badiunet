<?php
/**
 * Manage from module TCC UnaSUS MBP.
 *
 * @package    local_badiunet
 * @copyright  UnaSUS 2020
 */

require_once("../../config.php");
require_once("$CFG->dirroot/local/badiunet/lib/viewlib.php");
require_once("$CFG->dirroot/local/badiunet/lib/netlib.php");
require_login(true);

$context = context_system::instance();
require_capability('local/badiunet:viewsystemreport', $context);


$PAGE->set_context($context);
$PAGE->set_heading(get_string('panelmaneger', 'local_badiunet'));
$PAGE->set_title(get_string('panelmaneger', 'local_badiunet'));
$PAGE->set_url("{$CFG->wwwroot}/local/badiunet/manage.php");
$PAGE->navbar->add(get_string('pluginname','local_badiunet'), new moodle_url("{$CFG->wwwroot}/local/badiunet/manage.php"));
$PAGE->navbar->add(get_string('panelmaneger','local_badiunet'));

echo $OUTPUT->header();
echo $OUTPUT->heading(get_string('panelmaneger', 'local_badiunet'));
$netlib = new local_badiunet_netlib();
$parentid=$netlib->getModuleInstance();
// Make cards to show in manage file.
$cards = array(
    array (
        'cardhead'        => get_string('cardaccessmreportlocaltitle', "local_badiunet"),
        'cardlink'        => "{$CFG->wwwroot}/local/badiunet/fcservice/index.php",
        'carddescription' => get_string('cardaccessmreportlocaldescription', "local_badiunet"),
    ),
    array (
        'cardhead'        => get_string('cardaccessmreportremotetitle', "local_badiunet"),
        'cardlink'        => "{$CFG->wwwroot}/local/badiunet/fcservice/reqservice.php?_key=badiu.moodle.mreport.sitedata.frontpage&parentid=$parentid&_serviceid=$parentid&_datasource=servicesql",
		'cardlinktarget'  =>'target="_blank"',
        'carddescription' => get_string('cardaccessmreportremotedescription', "local_badiunet"),
    ),
    array (
        'cardhead'        => get_string('cardaccesschata2ititle', "local_badiunet"),
        'cardlink'        => "{$CFG->wwwroot}/local/badiunet/chat/index.php",
		'cardlinktarget'  =>'target="_blank"',
        'carddescription' => get_string('cardaccesschata2idescription', "local_badiunet"),
    )
   
);

$viewlib=new local_badiunet_viewlib();
echo $viewlib->makeCard($cards);
echo $OUTPUT->footer();
