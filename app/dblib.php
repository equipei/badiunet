<?php
require_once("$CFG->dirroot/local/badiunet/lib/httputil.php");
require_once("$CFG->dirroot/local/badiunet/lib/httpquerystring.php");
require_once("$CFG->dirroot/local/badiunet/lib/util.php");
class local_badiunet_app_dblib {
    private $table;
    function __construct($table= null) {
        if(!empty($table)){$this->table=$table;}
    }

    function save($dto) {
        global $DB;
        $dto->timecreated = time();
        $dto->isrealese=1;
        return $DB->insert_record($this->getTable(), $dto);
    }

    function edit($dto) {
        global $DB;
        $dto->timemodified = time();
        return $DB->update_record($this->getTable(), $dto);
    }

    function add($dto) {

        $this->add_default_value($dto);
		 $dto = $this->exec_before($dto);
        if (!empty($dto->id)) {
            $this->edit($dto);
         
        } else {
             $dto->id = $this->save($dto);
        }
		 $dto = $this->exec_after($dto);
       return $dto;
    }
   function get_by_id($id) {
        global $DB, $CFG;
		$tbl=$this->getTable();
        $sql = "SELECT * FROM {$CFG->prefix}$tbl  WHERE id = $id";
        $result = $DB->get_record_sql($sql);
        return $result;
    }

	function delete_by_id($id) {
        global $DB;
		 $result = $DB->delete_records($this->getTable(),array('id'=>$id));
        return $result;
    }
    function paging($countrows,$param){
        global $OUTPUT,$CFG;
        $httpquerystring=new local_badiunet_httpquerystring();
        $httpquerystring->setParam((array)$param);
        $httputil=new local_badiunet_httputil();
        $httpquerystring->makeParam();
        $httpquerystring->remove('page');
        $httpquerystring->makeQuery();
        $newquery=$httpquerystring->getQuery();
        $url=$httputil->get_current_url();

        echo  $OUTPUT->paging_bar($countrows, $param->page,$param->perpage, "$url?$newquery");
    }
    function exist($dto) {

        return false;
    }
    function add_default_value($dto) {

        return $dto;
    }
	 function exec_before($dto)
    {

        return $dto;
    }
    function exec_after($dto)
    {

        return $dto;
    }

    function search_param($param) {

        return null;
    }
    function search_count($param) {

        return null;
    }

    function search($param) {

        return null;
    }
    function view($dto, $param) {

        return null;
    }
    function getTable() {
        return $this->table;
    }

    function setTable($table) {
        $this->table = $table;
    }

 public function getUtil(){
	 if(!isset($this->util) || $this->util==null){$this->util=new local_badiunet_util();}
	   return $this->util;
   } 

}

?>
