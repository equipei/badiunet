<?php
require_once("$CFG->dirroot/local/badiunet/app/dblib.php");
require_once("$CFG->dirroot/local/badiunet/app/sserver/util.php");
require_once("$CFG->dirroot/local/badiunet/lib/formutil.php"); 
class local_badiunet_app_sserver_dblib extends local_badiunet_app_dblib {
    
    function __construct($table="local_badiunet_sserver") {
        parent::__construct($table);
    }

    function add_default_value($dto) {
        if(empty($dto->id)){$dto->sstatus='notenableb';}
		$dto=$this->manage_default_proxy_value($dto) ;
        return $dto;
    }
    function search_param($param) {
        $wsql = "";
        if (!empty($param->name)) {
            $name  = strtolower($param->name);
            $wsql .= " AND LOWER(s.name) LIKE '%'".$name."'%'";
        }
        return  $wsql;
    }

	function exec_after($dto)
    {
		if($dto->dtype=='badiugc2' && $dto->defaultsystem==1){
			global $CFG,$DB;
			$sql="UPDATE {$CFG->prefix}local_badiunet_sserver SET defaultsystem=:defaultsystem WHERE dtype=:dtype AND id != :id";
			$fparam=array('defaultsystem'=>0,'dtype'=>'badiugc2','id'=>$dto->id);
			$r=$DB->execute($sql,$fparam);
		}
        return $dto;
    }
    function manage_default_proxy_value($dto) {
		 if(empty($dto->id)){return $dto;}
		 if(!$dto->proxyenable){return $dto;}
		 
		 if(!empty($dto->proxyrole)){return $dto;} 
		 
		 $proxyrole='
		 [
{"statuslogin": "loggedin","roleshortname": "student","urisources": ["/","/my/","/my","/my/courses.php","/?redirect=0"],"urltarget": "BADIU_GC2_URL/tms/my/student/fview/default/index","operation": "redirect","sysroleshortname": "MOODLE_SYSTEM_MY_ROLES_SHORTNAME","authmethod": "badiuauth"},
{"statuslogin": "loggedin","roleshortname": "editingteacher","urisource": "/?redirect=0","urltarget": "BADIU_GC2_URL/tms/my/student/fview/default/index","operation": "redirect","sysroleshortname": "MOODLE_SYSTEM_MY_ROLES_SHORTNAME"},
{"statuslogin": "loggedin","roleshortname": "admin","urisource": "/?redirect=0","urltarget": "BADIU_GC2_URL/tms/my/student/fview/default/index","operation": "redirect","sysroleshortname": "MOODLE_SYSTEM_MY_ROLES_SHORTNAME"},
{"statuslogin": "loggedoff","urisource": "/","urltarget": "BADIU_GC2_LOGIN_URL","operation": "redirecttologin"},
{"statuslogin": "loggedoff","urisource": "/login/index.php","urltarget": "BADIU_GC2_LOGIN_URL","operation": "redirecttologin"}
]';
		 $dto->proxyrole=$proxyrole;
		 return $dto; 
	}
	function count_default_proxy_dtype($param) {
	
		 global $DB, $CFG;
		 $defaultsystem=$this->getUtil()->getVlueOfArray($param, 'defaultsystem');
		 $dtype=$this->getUtil()->getVlueOfArray($param, 'dtype');
		 $proxyenable=$this->getUtil()->getVlueOfArray($param, 'proxyenable');
		 if(empty($defaultsystem)){return null;}
		 if(empty($dtype)){return null;}
		 if($proxyenable===null || $proxyenable===""){return null;}
		 $fparam=array('defaultsystem'=>$defaultsystem,'dtype'=>$dtype,'proxyenable'=>$proxyenable);
		
		$sql = "SELECT COUNT(id) AS countrecord FROM {$CFG->prefix}local_badiunet_sserver  WHERE defaultsystem=:defaultsystem AND dtype=:dtype AND proxyenable=:proxyenable";
		$result = $DB->get_record_sql($sql,$fparam);
		
		if(!empty($result)){return $result->countrecord;}
		
        return $result;
	}
	 function get_default_proxy_dtype($param) {
		
		 global $DB, $CFG;
		 $defaultsystem=$this->getUtil()->getVlueOfArray($param, 'defaultsystem');
		 $dtype=$this->getUtil()->getVlueOfArray($param, 'dtype');
		 $proxyenable=$this->getUtil()->getVlueOfArray($param, 'proxyenable');
		 if(empty($defaultsystem)){return null;}
		 if(empty($dtype)){return null;}
		 if($proxyenable===null || $proxyenable===""){return null;}
		  $fparam=array('defaultsystem'=>$defaultsystem,'dtype'=>$dtype,'proxyenable'=>$proxyenable);
		$sql = "SELECT id,serviceurl,proxyrole FROM {$CFG->prefix}local_badiunet_sserver  WHERE defaultsystem=:defaultsystem AND dtype=:dtype AND proxyenable=:proxyenable";
	
        $result = $DB->get_record_sql($sql,$fparam);
		
        return $result;
	}
    function get_by_servicekeyinstance($servicekeyinstance) {
        global $DB, $CFG;
        if(empty($servicekeyinstance)){return null;}
        else {$servicekeyinstance="'".$servicekeyinstance."'";}
        $sql = "SELECT id,name,sstatus,enviroment,serviceurl,servicetoken,defaultmodulekey,criptk1,criptk2,serverrmoteipallowed,servicekeyinstance,anonymousaccesskeys,dconfig,description,timecreated,timemodified FROM {$CFG->prefix}local_badiunet_sserver  WHERE servicekeyinstance = $servicekeyinstance";
        $result = $DB->get_record_sql($sql);
        return $result;
    }
	function get_id_by_servicekeyinstance($servicekeyinstance) {
        global $DB, $CFG;
		if(empty($servicekeyinstance)){return null;}
        $sql = "SELECT id FROM {$CFG->prefix}local_badiunet_sserver  WHERE servicekeyinstance = :servicekeyinstance";
		$pfilter=array('servicekeyinstance'=>$servicekeyinstance);
        $result = $DB->get_record_sql($sql,$pfilter);
		if(!empty($result)){return $result->id;}
        return $result;
    }
	
	function get_name_servicekeyinstance() {
        global $DB, $CFG;
		$sql = "SELECT id,name,servicekeyinstance FROM {$CFG->prefix}local_badiunet_sserver  WHERE id > 0";
		$result = $DB->get_records_sql($sql);
		return $result;
    }
	function get_comun_by_servicekeyinstance($servicekeyinstance,$column) {
        global $DB, $CFG;
        if(empty($servicekeyinstance)){return null;}
		
		
		if(empty($column)){return null;}
        
        $sql = "SELECT $column FROM {$CFG->prefix}local_badiunet_sserver  WHERE servicekeyinstance = :servicekeyinstance ";
		$pfilter=array('servicekeyinstance'=>$servicekeyinstance);
        $result = $DB->get_record_sql($sql,$pfilter);
		if(!empty($result)){return $result->$column;}
        return $result;
    }
   
    function search_count($param) {
        global $DB, $CFG;
        $wsql   = $this->search_param($param);
        $sql    = "SELECT COUNT(s.id) as countrecord  FROM {$CFG->prefix}local_badiunet_sserver s WHERE s.id > 0  $wsql ";
        $result = $DB->get_record_sql($sql);
        if (!empty($result)) {
            $result = $result->countrecord;
        }
        return $result;
    }

    
    function search($param) {
        global $DB, $CFG;
        $wsql=$this->search_param($param);
        $sql ="SELECT s.id,s.name,s.sstatus,s.serviceurl,s.servicekeyinstance,s.dtype,s.defaultsystem FROM {$CFG->prefix}local_badiunet_sserver s  WHERE s.id > 0  $wsql ORDER BY s.name ";
        $result= $DB->get_records_sql($sql,null,$param->page*$param->perpage, $param->perpage);
        return $result;
    }

    function view($dto, $param) {
		
		 if(isset($param->opkey) && $param->opkey=='viewrowdetails' ){return null;}
        global $CFG;
        $util=new local_badiunet_app_sserver_util();
		$futil= new local_badiunet_formutil();
        $table        = new html_table();
        $table->head  = [get_string('id', 'local_badiunet'), get_string('sserver', 'local_badiunet'), get_string('serviceurl', 'local_badiunet'), get_string('servicekeyinstanceshort', 'local_badiunet'), get_string('systemdtype', 'local_badiunet'), get_string('defaultsystem', 'local_badiunet'), get_string('servicestatus', 'local_badiunet'),''];
        $table->align = ['left', 'left', 'left', 'left', 'left', 'left', 'left'];
        $table->class = 'generaltable table';
        $table->data  = [];

        // Make table rows.
        foreach ($dto->rows as $row) {
            $status=$util->get_status_label($row->servicekeyinstance, $row->sstatus);
            $link=$util->managelink($row);  
			$dtype=$futil->label_systemdtype($row->dtype);
			$defaultsystem=$futil->label_yes_not($row->defaultsystem);
            $table->data[] = array($row->id, $row->name, $row->serviceurl, $row->servicekeyinstance,$dtype,$defaultsystem,$status,$link);
        }
	   echo "<a href=\"edit.php\">" . get_string('addnew', 'local_badiunet') . "</a><br /> ";
        echo get_string('countrecord', 'local_badiunet') . $dto->countrecord;
        $this->paging($dto->countrecord, $param);
        echo html_writer::table($table);
        $this->paging($dto->countrecord, $param);
    }

	function viewdetail($dto, $param) {
		if(!isset($param->opkey)){return null;}
		if(isset($param->opkey) && $param->opkey!='viewrowdetails' ){return null;}
		$data=$this->get_by_id($param->id);
		$outhtml="";
		$futil= new local_badiunet_formutil();
		ob_start();
		include_once("viewhtml.php");
		 $outhtml = ob_get_contents();
		ob_end_clean();
		return $outhtml;
		
	}
}
