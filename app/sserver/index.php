<?php
require_once("../../../../config.php");
require_once("$CFG->dirroot/local/badiunet/app/sserver/dblib.php");
//require_once("$CFG->dirroot/local/badiunet/app/sserver/form_search.php");
require_login();
if (!has_capability('moodle/site:config', get_context_instance(CONTEXT_SYSTEM))) {
    require_capability('local/badiunet:config', get_context_instance(CONTEXT_SYSTEM), NULL, false);
}

$param               = new stdClass;
$param->page         = optional_param('page', 0,PARAM_INT);
$param->perpage      = optional_param('perpage', 10,PARAM_INT);
$param->name         = optional_param('name', null,PARAM_TEXT);
$param->id        = optional_param('id', 0, PARAM_INT);
$param->opkey        = optional_param('_opkey', NULL, PARAM_TEXT);

$PAGE->set_context(context_system::instance());
$PAGE->set_heading(get_string('sserver', 'local_badiunet'));
$PAGE->set_title(get_string('sserver', 'local_badiunet'));
$PAGE->set_url('/local/badiunet/index.php');

$PAGE->navbar->add(get_string('pluginname','local_badiunet'));
$PAGE->navbar->add(get_string('sserver','local_badiunet'),new moodle_url("$CFG->wwwroot/local/badiunet/app/sserver/index.php"));
$PAGE->navbar->add(get_string('managesserver','local_badiunet'));

echo $OUTPUT->header();
echo $OUTPUT->heading(get_string('sserver', 'local_badiunet'));

$dto   = new stdClass;
$dblib = new local_badiunet_app_sserver_dblib();
//$form  = new local_badiunet_app_sserver_form_search();

$dto->countrecord = $dblib->search_count($param);
$dto->rows        = $dblib->search($param);

//$currenttab = 'resource';
//print_tabs($tabs, $currenttab, $inactive, $activated);

//$form->display();
echo $dblib->view($dto, $param);
echo $dblib->viewdetail($dto, $param);
echo $OUTPUT->footer();

