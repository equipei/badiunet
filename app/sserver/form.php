<?php

require_once $CFG->libdir . '/formslib.php';
require_once("$CFG->dirroot/local/badiunet/lib/formutil.php"); 
class local_badiunet_app_sserver_form extends moodleform {

	/**
	 * Form definition.
	 *
	 * @throws coding_exception
	 * @throws dml_exception
	 */
	function definition() {
		global $dto;
		$util= new local_badiunet_formutil();
		$yesnotoptions=$util->option_yes_not();
		$optionsystemdtype=$util->option_systemdtype();
		$mform        = &$this->_form;
		$renderer     = &$mform->defaultRenderer();
		
		$mform->addElement('header', 'general', get_string('general', 'local_badiunet') , '');

		if (isset($dto->id) && $dto->id != null && $dto->id > 0) {
			$mform->addElement('hidden', 'id', $dto->id);
			$mform->setType('id', PARAM_INT);

		}

		$mform->addElement('text', 'name', get_string('sservername', 'local_badiunet'),'size="70"');
		$mform->setType('name', PARAM_TEXT);
		$mform->addRule('name', get_string('requiredfield', 'local_badiunet') , 'required', null, 'cliente');
		$mform->setDefault('name', $dto->name);

		$mform->addElement('text', 'serviceurl', get_string('serviceurl', 'local_badiunet'),'size="70"');
		$mform->setType('serviceurl', PARAM_TEXT);
		$mform->addRule('serviceurl', get_string('requiredfield', 'local_badiunet') , 'required', null, 'cliente');
		$mform->setDefault('serviceurl', $dto->serviceurl);
		
		$mform->addElement('text', 'servicekeyinstance', get_string('servicekeyinstance', 'local_badiunet'),'size="70"');
		$mform->setType('servicekeyinstance', PARAM_TEXT);
		$mform->addRule('servicekeyinstance', get_string('requiredfield', 'local_badiunet') , 'required', null, 'cliente');
		$mform->setDefault('servicekeyinstance', $dto->servicekeyinstance);
		
		$mform->addElement('select', 'dtype', get_string('systemdtype','local_badiunet'), $optionsystemdtype);
        $mform->setType('dtype', PARAM_TEXT);
        $mform->setDefault('dtype', $dto->dtype);
		
		$mform->addElement('select', 'defaultsystem', get_string('defaultsystem','local_badiunet'), $yesnotoptions);
        $mform->setType('defaultsystem', PARAM_INT);
        $mform->setDefault('defaultsystem', $dto->defaultsystem);
		
		$mform->addElement('header', 'security', get_string('security', 'local_badiunet') , '');

		$mform->addElement('text', 'servicetoken', get_string('servicetoken', 'local_badiunet'),'size="70"');
		$mform->setType('servicetoken', PARAM_TEXT);
		$mform->setDefault('servicetoken', $dto->servicetoken);

		$mform->addElement('textarea', 'criptk1', get_string('criptk1', 'local_badiunet'),'wrap="virtual" rows="5" cols="70"');
		$mform->setType('criptk1', PARAM_TEXT);
		$mform->setDefault('criptk1', $dto->criptk1);
		
		$mform->addElement('textarea', 'criptk2', get_string('criptk2', 'local_badiunet'),'wrap="virtual" rows="5" cols="70"');
		$mform->setType('criptk2', PARAM_TEXT);
		$mform->setDefault('criptk2', $dto->criptk2);

		$mform->addElement('text', 'serverrmoteipallowed', get_string('serverrmoteipallowed', 'local_badiunet'),'size="70"');
		$mform->setType('serverrmoteipallowed', PARAM_TEXT);
		$mform->setDefault('serverrmoteipallowed', $dto->serverrmoteipallowed);

		
		
		$mform->addElement('header', 'proxy', get_string('proxy', 'local_badiunet') , '');
		
		$mform->addElement('select', 'proxyenable', get_string('proxyenable','local_badiunet'), $yesnotoptions);
        $mform->setType('proxyenable', PARAM_INT);
        $mform->setDefault('proxyenable', $dto->proxyenable);
		
		$mform->addElement('textarea', 'proxyrole', get_string('proxyrole', 'local_badiunet'),'wrap="virtual" rows="7" cols="70"');
		$mform->setType('proxyrole', PARAM_TEXT);
		$mform->setDefault('proxyrole', $dto->proxyrole);
		
		/*$mform->addElement('header', 'remoteaccess', get_string('remoteaccess', 'local_badiunet') , '');
		
		$mform->addElement('select', 'enableaccessremote', get_string('enableaccessremote','local_badiunet'), $yesnotoptions);
        $mform->setType('enableaccessremote', PARAM_INT);
        $mform->setDefault('enableaccessremote', $dto->enableaccessremote);
		
		$mform->addElement('select', 'enableaccessremotecsddata', get_string('enableaccessremotecsddata','local_badiunet'), $yesnotoptions);
        $mform->setType('enableaccessremotecsddata', PARAM_INT);
        $mform->setDefault('enableaccessremotecsddata', $dto->enableaccessremotecsddata);
		
		$mform->addElement('select', 'enableremoteauth', get_string('enableremoteauth','local_badiunet'), $yesnotoptions);
        $mform->setType('enableremoteauth', PARAM_INT);
        $mform->setDefault('enableremoteauth', $dto->enableremoteauth);
		
		$mform->addElement('select', 'enableremotecoursebackup', get_string('enableremotecoursebackup','local_badiunet'), $yesnotoptions);
        $mform->setType('enableremotecoursebackup', PARAM_INT);
        $mform->setDefault('enableremotecoursebackup', $dto->enableremotecoursebackup);
		
		$mform->addElement('select', 'enableremotecourserestore', get_string('enableremotecourserestore','local_badiunet'), $yesnotoptions);
        $mform->setType('enableremotecourserestore', PARAM_INT);
        $mform->setDefault('enableremotecourserestore', $dto->enableremotecourserestore);
		
		$mform->addElement('select', 'enableremotewebservice', get_string('enableremotewebservice','local_badiunet'), $yesnotoptions);
        $mform->setType('enableremotewebservice', PARAM_INT);
        $mform->setDefault('enableremotewebservice', $dto->enableremotewebservice);
		
		$mform->addElement('select', 'enableremoteconsolidate', get_string('enableremoteconsolidate','local_badiunet'), $yesnotoptions);
        $mform->setType('enableremoteconsolidate', PARAM_INT);
        $mform->setDefault('enableremoteconsolidate', $dto->enableremoteconsolidate);
 
		$mform->addElement('header', 'syncuser', get_string('syncuser', 'local_badiunet') , '');
		
		$mform->addElement('select', 'autosyncuser', get_string('autosyncuser','local_badiunet'), $yesnotoptions);
        $mform->setType('autosyncuser', PARAM_INT);
        $mform->setDefault('autosyncuser', $dto->autosyncuser);
		
		$keysyncuseroptions = $util->option_keysync_user();
		$mform->addElement('select', 'keysyncuser', get_string('keysyncuser','local_badiunet'), $keysyncuseroptions);
        $mform->setType('keysyncuser', PARAM_TEXT);
        $mform->setDefault('keysyncuser', $dto->keysyncuser);
 
 		$mform->addElement('header', 'otherconfig', get_string('otherconfig', 'local_badiunet') , '');
		
		$mform->addElement('textarea', 'description', get_string('description','local_badiunet'), 'wrap="virtual" rows="7" cols="70"');
		$mform->setType('description', PARAM_TEXT);
		$mform->setDefault('description', $dto->description);
		
		
		$mform->addElement('textarea', 'anonymousaccesskeys', get_string('anonymousaccesskeys', 'local_badiunet'),'wrap="virtual" rows="7" cols="70"');
		$mform->setType('anonymousaccesskeys', PARAM_TEXT);
		$mform->setDefault('anonymousaccesskeys', $dto->anonymousaccesskeys);
		
		$mform->addElement('textarea', 'dconfig', get_string('dconfig', 'local_badiunet'),'wrap="virtual" rows="7" cols="70"');
		$mform->setType('dconfig', PARAM_TEXT);
		$mform->setDefault('dconfig', $dto->dconfig);
		*/
		$this->add_action_buttons(true, get_string('save', 'local_badiunet'));
        $this->set_data($currententry);
	}

	/**
	 * Form validation.
	 *
	 * @param array $data
	 * @param array $files
	 * @return array
	 */
	function validation($data, $files) {
		global $role;
		$errors = parent::validation($data, $files);
		$url=$data['serviceurl'];
		if (!empty($url) &&!filter_var($url, FILTER_VALIDATE_URL)) {
			$errors['serviceurl'] = get_string('serviceurlnotvalid', 'local_badiunet');
		} 
		return $errors;
	}
}
