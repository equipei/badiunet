<div id="accordion">

  <div class="card">
    <div class="card-header">
      <a class="card-link" data-toggle="collapse" href="#general">
        <?php  echo  get_string('general', 'local_badiunet');/* echo $data->name;*/?>
      </a>
    </div>
    <div id="general" class="collapse show" data-parent="#accordion">
      <div class="card-body">
       <table class="table">
	   <tr>
		 <td><?php  echo get_string('sservername', 'local_badiunet');?></td>
		 <td> <?php  echo $data->name;?></td>
	   </tr>
	   
	    <tr>
		 <td><?php  echo get_string('serviceurl', 'local_badiunet');?></td>
		 <td> <?php  echo $data->serviceurl;?></td>
	   </tr>
	   
	    <tr>
		 <td><?php  echo get_string('servicekeyinstance', 'local_badiunet');?></td>
		 <td> <?php  echo $data->servicekeyinstance;?></td>
	   </tr>
	   
	   <tr>
		 <td><?php  echo get_string('systemdtype', 'local_badiunet');?></td>
		 <td> <?php echo $futil->label_systemdtype($data->dtype);?></td>
	   </tr>
	   <tr>
		 <td><?php  echo get_string('defaultsystem', 'local_badiunet');?></td>
		 <td> <?php  echo $futil->label_yes_not($data->defaultsystem);?></td>
	   </tr>
	   </table>
      </div>
    </div>
  </div>

  <div class="card">
    <div class="card-header">
      <a class="collapsed card-link" data-toggle="collapse" href="#security">
       <?php  echo  get_string('security', 'local_badiunet');?>
      </a>
    </div>
    <div id="security" class="collapse" data-parent="#accordion">
      <div class="card-body">
        <table class="table">
	   <tr>
		 <td><?php  echo get_string('servicetoken', 'local_badiunet');?></td>
		 <td> <?php  echo $data->servicetoken;?></td>
	   </tr>
	   
	    <tr>
		 <td><?php  echo get_string('criptk1', 'local_badiunet');?></td>
		 <td> <?php  echo $data->criptk1;?></td>
	   </tr>
	   
	    <tr>
		 <td><?php  echo get_string('criptk2', 'local_badiunet');?></td>
		 <td> <?php  echo $data->criptk2;?></td>
	   </tr>
	   
	   <tr>
		 <td><?php  echo get_string('serverrmoteipallowed', 'local_badiunet');?></td>
		 <td> <?php  echo $data->serverrmoteipallowed;?></td>
	   </tr>
	   </table>
      </div>
    </div>
  </div>


 <div class="card">
    <div class="card-header">
      <a class="collapsed card-link" data-toggle="collapse" href="#proxy">
       <?php  echo  get_string('proxy', 'local_badiunet');?>
      </a>
    </div>
    <div id="proxy" class="collapse" data-parent="#accordion">
      <div class="card-body">
        <table class="table">
	   <tr>
		 <td><?php  echo get_string('proxyenable', 'local_badiunet');?></td>
		 <td> <?php  echo $futil->label_yes_not($data->proxyenable);?></td>
	   </tr>
	   
	    <tr>
		 <td><?php  echo get_string('proxyrole', 'local_badiunet');?></td>
		 <td> <?php  echo $data->proxyrole;?></td>
	   </tr>
	   
	   </table>
      </div>
    </div>
  </div>
<!--
  <div class="card">
    <div class="card-header">
      <a class="collapsed card-link" data-toggle="collapse" href="#remoteaccess">
       <?php  echo get_string('remoteaccess', 'local_badiunet');?>
      </a>
    </div>
    <div id="remoteaccess" class="collapse" data-parent="#accordion">
      <div class="card-body">
       
      </div>
    </div>
  </div>

    <div class="card">
    <div class="card-header">
      <a class="collapsed card-link" data-toggle="collapse" href="#otherconfig">
       <?php  echo  get_string('otherconfig', 'local_badiunet');?>
      </a>
    </div>
    <div id="otherconfig" class="collapse" data-parent="#accordion">
      <div class="card-body">
        
      </div>
    </div>
  </div>

  
</div>
-->