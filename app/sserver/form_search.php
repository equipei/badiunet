<?php
require_once("$CFG->libdir/formslib.php");

class local_badiunet_app_sserver_form_search extends moodleform {
    /**
     * Definition of form.
     *
     * @throws coding_exception
     * @throws dml_exception
     */
    function definition() {
        global $param;
        
        $mform    = & $this->_form;
        $renderer = & $mform->defaultRenderer();

        $mform->addElement('header', '', get_string('general', 'mod_cmsunasus') , '');

        $mform->addElement('text', 'name', get_string('title', 'mod_cmsunasus'));
        $mform->setType('name', PARAM_TEXT);

        

        $this->add_action_buttons(true, get_string('search', 'mod_cmsunasus'));
    }

    /**
     * Validation of form.
     *
     * @param array $data
     * @param array $files
     * @return array
     */
    function validation($data, $files) {
        $errors = parent::validation($data, $files);
        return $errors;
    }
}
