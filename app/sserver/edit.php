<?php
require_once("../../../../config.php");
require_once("$CFG->dirroot/local/badiunet/app/sserver/dblib.php");
require_once("$CFG->dirroot/local/badiunet/app/sserver/form.php");

require_login();
if (!has_capability('moodle/site:config', get_context_instance(CONTEXT_SYSTEM))) {
    require_capability('local/badiunet:config', get_context_instance(CONTEXT_SYSTEM), NULL, false);
}
$param     = new stdClass;
$param->id        = optional_param('id', 0, PARAM_INT);
$param->opkey        = optional_param('_opkey', NULL, PARAM_TEXT);
$param->tokenexec        = optional_param('_tokenexec', NULL, PARAM_TEXT);


$dto     = new stdClass;
$dblib   = new local_badiunet_app_sserver_dblib();

$msgaddsuccess = get_string('addsuccess', 'local_badiunet');

$context = context_system::instance();
$PAGE->set_context($context);
$PAGE->set_title(get_string('sserveredit', 'local_badiunet'));
$PAGE->set_url('/local/badiunet/app/sserver/edit.php');
$PAGE->navbar->add(get_string('pluginname','local_badiunet'));
$PAGE->navbar->add(get_string('sserver','local_badiunet'),new moodle_url("$CFG->wwwroot/local/badiunet/app/sserver/index.php"));
$PAGE->navbar->add(get_string('sserveredit','local_badiunet'));
if(!empty($param->id) && is_int($param->id)){
    $dto = $dblib->get_by_id($param->id); 
} 

$form     = new local_badiunet_app_sserver_form();
deleterow($param,$dto,$dblib);
proccesform($dblib,$form);
pageview($form,$param);


 function proccesform($dblib,$form){
     global $CFG;
    $urlindex = "$CFG->wwwroot/local/badiunet/app/sserver/index.php";
    if ($form->is_cancelled()) {
        redirect($urlindex);
    } else if ($formdata = $form->get_data()) {
        $fresult= $dblib->add($formdata);
        if(!empty($formdata->id)){$msgaddsuccess = get_string('editsuccess', 'local_badiunet');}
        redirect($urlindex, $msgaddsuccess,2);
    }
 }

 function deleterow($param,$dto,$dblib){
    global $CFG;
    global $OUTPUT;
    $urlindex = "$CFG->httpswwwroot/local/badiunet/app/sserver/index.php";
    $tokenexec=md5($dto->timecreated);
    if($param->opkey=='removerowbyidconfirm'){
        $id=$param->id;
       
        $urlremoveexec = "$CFG->httpswwwroot/local/badiunet/app/sserver/edit.php?id=$id&_opkey=removerowbyidexec&_tokenexec=$tokenexec";
       /* $fileurl        = new moodle_url($urledit);
        $continueurl    = new moodle_url($urledit, ['id' => $dto->id, 'delete' => md5($dto->timemodified)]);
        $continuebutton = new single_button($continueurl, get_string('delete'), 'post');
*/
        //echo $OUTPUT->confirm(get_string('deleteconfirm', 'theme_baseunasus', $dto->name), $continuebutton, $fileurl);
        echo $OUTPUT->header();
        echo $OUTPUT->heading(get_string('managesserver', 'local_badiunet'));
        echo $OUTPUT->confirm(get_string('deleteconfirm', 'local_badiunet', $dto->name), $urlremoveexec, $urlindex);
        echo $OUTPUT->footer();
    }else  if($param->opkey=='removerowbyidexec' && $param->tokenexec==$tokenexec ){
        if(!empty($dto->id)){
            $dblib->delete_by_id($dto->id);
            redirect($urlindex,'registro excluido com sucesso',2);
        }
        redirect($urlindex);
    }

}

 function pageview($form,$param){
    if($param->opkey=='removerowbyidconfirm'){return null;}
     global $OUTPUT;
    echo $OUTPUT->header();
    echo $OUTPUT->heading(get_string('managesserver', 'local_badiunet'));
    $form->display();
    echo $OUTPUT->footer();
}