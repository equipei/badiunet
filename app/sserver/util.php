<?php

class local_badiunet_app_sserver_util {
    
    
    function get_status_label($servicekeyinstance,$status) {
		 global $CFG;
        $label="";
            if($status=='notenableb'){
				$label=get_string('servicestatusnotenabled', 'local_badiunet');
				$labelanbleservice=get_string('servicestatussetenable', 'local_badiunet');
				$linkenable="<a href=\"$CFG->wwwroot/local/badiunet/fcservice/status.php?_enableremoteservice=1&_appservicekeyinstance=$servicekeyinstance&_urltarget=appsserver\">$labelanbleservice</a>";
				$label.=" | $linkenable";
			}
            else if($status=='enable'){$label=get_string('servicestatusenable', 'local_badiunet');}
         
        return  $label;
    }
    function  managelink($row){
		$outhtml="";
		if(empty($row)){return $outhtml;}
		if (!is_object($row)) {return $outhtml;}
		$id=$row->id;
		$labelview=get_string('view', 'local_badiunet');
		$labeledit=get_string('edit', 'local_badiunet');
		$labelremove=get_string('remove', 'local_badiunet');
		$urlview="index.php?id=$id&_opkey=viewrowdetails";
		$urledit="edit.php?id=$id";
		$urlremove="edit.php?id=$id&_opkey=removerowbyidconfirm";
		$outhtml.="<a href=\"$urlview\"> $labelview | </a>";
		$outhtml.="<a href=\"$urledit\"> $labeledit | </a>";
		$outhtml.="<a href=\"$urlremove\"> $labelremove  </a>";
        return $outhtml;
    }
}
