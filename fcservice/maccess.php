<?php

require_once("$CFG->dirroot/local/badiunet/lib/util.php");
require_once("$CFG->dirroot/local/badiunet/lib/netlib.php");
require_once("$CFG->dirroot/local/badiunet/lib/response.php");
require_once("$CFG->dirroot/local/badiunet/lib/factorydnakey.php");
require_once("$CFG->dirroot/local/badiunet/lib/templetecript.php");
require_once("$CFG->dirroot/local/badiunet/lib/sessionlib.php");
require_once("$CFG->dirroot/local/badiunet/lib/role.php");
require_once("$CFG->dirroot/local/badiunet/lib/theme.php");
class local_badiunet_maccess {
     private $messageeror='';
      private $util = null;
       private $tcript = null;
     function __construct() {
         $this->util=new local_badiunet_util();
         $this->tcript = new local_badiunet_templatecript();
     }
    
     function start($data) {
        
         $_SESSION['_local_badiunet_maccess_session']=$data;
         global $USER;
         $userid=0;
         if(!empty($USER->id)){$userid=$USER->id;}
         $sessionlib=new local_badiunet_sessionlib();
         $dto=new stdClass;
         $dto->name=$this->util->getVlueOfArray($data, 'tkey');
         $criptykey=$this->util->getVlueOfArray($data, 'criptykey');
		 $rkeys=$this->util->getVlueOfArray($data, 'rkeys');
		 $value=array();
		 $value['criptykey']=$criptykey;
		 $value['rkeys']=$rkeys;
		 $dto->value=json_encode($value);
         
         $dto->userid=$userid;
         $dto->dtype='moodle';
         $dto->timeexpire=0;
         $dto->serverip=$this->util->getLastipconnection();
         $sessionlib->add($dto);
        
         $routefrontpage=$this->util->getVlueOfArray($data, 'routefrontpage');
         if(!empty($routefrontpage)){
            $_SESSION['_local_badiunet_maccess_session_routefrontpage']=$routefrontpage;
         }
         
     }
     
     function end() {
         $_SESSION['_local_badiunet_maccess_session']=null;
     }
     function tryrelogin($action=0) {
         if($action==0){
             if(isset($_SESSION['_local_badiunet_maccess_session_tryrelogin'])){
                $result=$_SESSION['_local_badiunet_maccess_session_tryrelogin'];
                return $result;
             }
          return 0;   
         }else  if($action==1){
              if(isset($_SESSION['_local_badiunet_maccess_session_tryrelogin'])){
                  $result=$_SESSION['_local_badiunet_maccess_session_tryrelogin'];
                  $result++;
              }else{
                  $_SESSION['_local_badiunet_maccess_session_tryrelogin']=1;
              }
         }else  if($action==2){
             $_SESSION['_local_badiunet_maccess_session_tryrelogin']=0;
         }
      
     }
     function get() {
         $result=null;
         if(isset($_SESSION['_local_badiunet_maccess_session'])){
             $result=$_SESSION['_local_badiunet_maccess_session'];
         }
        
         return $result;
     }
     function getByKey($key) {
         $result=$this->get();
         $value=$this->util->getVlueOfArray($result, $key);
         return $value;
     }
     
     function getCriptykey($sessionid) {
         $result=$this->get();
         $value=$this->util->getVlueOfArray($result,'criptykey');
         if(empty($value)){
              $sessionlib=new local_badiunet_sessionlib();
              $value=$sessionlib->getValue($sessionid);
			  $value=$this->util->getJson($value, true);
			  $value=$this->util->getVlueOfArray($value,'criptykey');
         }
         return $value;
     }
     function getRkeys($sessionid) {
              $sessionlib=new local_badiunet_sessionlib();
              $value=$sessionlib->getValue($sessionid);
			  $value=$this->util->getJson($value, true);
			  $value=$this->util->getVlueOfArray($value,'rkeys');
        
         return $value;
     }
     function exist() {
         $result=$this->get();
         if($result) {return true;}
         return false;
     }
     function isLogged() {
         $islogued=$this->exist();
         if(!$islogued){$islogued=$this->login();}
         return $islogued;
     }
     function login() {
         global $USER;
         global $CFG;
         $userfullname=$USER->firstname;
         if(!empty($USER->lastname)){$userfullname." ".$USER->lastname;}
		 $appkeyinstance = optional_param('_appservicekeyinstance',NULL,PARAM_TEXT);
		 $this->tcript->setAppkeyinstance($appkeyinstance);
		 $netlib=new local_badiunet_netlib($appkeyinstance);
         $rolelib=new local_badiunet_role($USER->id);
         $themelib=new local_badiunet_theme($USER->id);
         $useroles=$rolelib->get();
         $themeinfo=$themelib->get();
         $plugininfo= $this->util->plugininfo();
		 $dlang=$this->util->getDefaultLang();
         $url=$netlib->getUrlService();
		 
         $token=$netlib->getToken(); 
         $sservicid=$netlib->getModuleInstance();
         
         $data=array();
         //$data['_token']=$cript->encode($token);
         $data['_service']='badiu.auth.core.webservice.navegation';
         $moodleinfo=$this->getMoodleName();
         $moodlename=$moodleinfo->fullname;
         $moodleshortname=$moodleinfo->shortname;
         
		 $anonymousaccess=1;
		 if(isloggedin() && !isguestuser($USER)){$anonymousaccess=0;}
		 
		
		
		 $autosyncuser= $netlib->initConfigKey('autosyncuser',$appkeyinstance); 
		 $keysyncuser= $netlib->initConfigKey('keysyncuser',$appkeyinstance);
		
         $reqparam=array();
         $reqparam['client']=array('browser'=>$_SERVER['HTTP_USER_AGENT'],'ip'=>$_SERVER["REMOTE_ADDR"],'moodleurl'=>$CFG->wwwroot,'moodlename'=>$moodlename,'moodleshortname'=>$moodleshortname,'moodleversiontxt'=>$CFG->release,'moodleversionnumber'=>$CFG->version,'moodledbtype'=>$CFG->dbtype,'theme'=>$themeinfo,'plugin'=>$plugininfo,'lang'=>$dlang);
         $reqparam['user']=array('username'=>$USER->username,'idnumber'=>$USER->idnumber,'firstname'=>$USER->firstname,'lastname'=>$USER->lastname,'id'=>$USER->id,'email'=>$USER->email,'roles'=>$useroles,'auth'=>$USER->auth,'anonymousaccess'=>$anonymousaccess,'autosync'=>$autosyncuser,'keysync'=>$keysyncuser);
         $reqparam['_token']=$token; 
		
         $reqparam=json_encode($reqparam);
		 
         $reqparam=$this->tcript->encode('k1',$reqparam,$sservicid);
         $data['_param']=$reqparam;
      
         $response=$this->util->request($url,$data);
       
         if($this->util->isResponseError($response)){$this->messageeror=$response;return false;}
         $status= $this->util->getVlueOfArray($response, 'status');
         $info= $this->util->getVlueOfArray($response, 'info');
         $message=$this->util->getVlueOfArray($response, 'message');
        
        $message=$this->tcript->decode($message);
        
        $message=$this->util->getJson($message, true);
        $sessiontimeduration=$this->util->getVlueOfArray($message, 'sessiontimeduration'); 
		if(!empty($sessiontimeduration)){
			$timeexpire=($sessiontimeduration*60)+time();
			$message['timeexpire']=$timeexpire;
		}
         if($status=='accept'){
             $this->start($message);
             $this->messageeror='';
             return true;
            
         }  
         else{
             $this->messageeror='Falha no login';
             //echo $this->messageeror;exit;
            return false; 
         }    
         
     }
     function getKeyToCriptFromTokenDna() {
         $key=null;
         $fkey=new local_badiunet_factorydnakey();
         $tokendna=$fkey->get();
         if(!empty($tokendna)){
            $key=substr($tokendna,25,50);
         }
         return $key;
     }
     
     public function getMoodleName() {
         global $CFG,$DB;
        $sql="SELECT fullname,shortname FROM {$CFG->prefix}course WHERE  id=1 AND format='site' ";
        $row=$DB->get_record_sql($sql);
        if(!empty($row)){return $row;}
        return null;
    }
    
    
     function getMessageeror() {
         return $this->messageeror;
     }

     function setMessageeror($messageeror) {
         $this->messageeror = $messageeror;
     }


}

?>
