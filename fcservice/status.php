<?php

require("../../../config.php");
require_once("$CFG->dirroot/local/badiunet/fcservice/register.php");

require_login();
if (!has_capability('moodle/site:config', get_context_instance(CONTEXT_SYSTEM))) {
    require_capability('local/badiunet:config', get_context_instance(CONTEXT_SYSTEM), NULL, false);
}  
$register=new local_badiunet_register();

$context = context_system::instance();

$PAGE->set_context($context);
$PAGE->set_url('/local/badiunet/fcservice/index.php');
$PAGE->set_pagelayout('standard');
$PAGE->set_title(get_string('pluginname', 'local_badiunet'));
$PAGE->set_heading(get_string('pluginname', 'local_badiunet'));

$register->enableservice();
$register->serviceagree();

?>
