<?php
require_once("../../../config.php");
require_once("$CFG->dirroot/local/badiunet/lib/util.php");
require_once("$CFG->dirroot/local/badiunet/lib/netlib.php");
require_once("$CFG->dirroot/local/badiunet/lib/httpquerystring.php");
require_once("$CFG->dirroot/local/badiunet/fcservice/maccess.php");
require_once("$CFG->dirroot/local/badiunet/fcservice/factorycontent.php");
require_once("$CFG->dirroot/local/badiunet/lib/templetecript.php");

new local_badiunet_request_service();

 
class local_badiunet_request_service {
    private $paramquery=null;
    private $username=null;
	private $userid=null;
    private $urltarget=null;
	private $netlib = null;
	private $maccess = null;
    function __construct() {
		 $appkeyinstance = optional_param('_appservicekeyinstance',NULL,PARAM_TEXT);
		 $this->netlib=new local_badiunet_netlib($appkeyinstance);
		 $this->maccess=new local_badiunet_maccess();
        $this->exec();
    }
    
	
    function exec() {
		require_login();
		
        //check param
       $this->validatePararm();
	   
	   $this->checkRegister();
	   $this->checkLogin();
		
		//register remote
		$this->remoteRegister();
       
	   //redirect
        $this->execRedirect();
    }
    
    function printContent($content) {
        global $PAGE,$OUTPUT;
         $context = context_system::instance();
        $PAGE->set_context($context);
        $PAGE->set_url('/local/badiunet/fcservice/index.php');
        $PAGE->set_pagelayout('standard');
        $PAGE->set_title('Badiu.Net');
        $PAGE->set_heading('Badiu.Net');
        
        echo $OUTPUT->header();
        echo $content;
        echo $OUTPUT->footer();
        exit;
    }
        
    function validatePararm(){
        $msg=null;
		$query=null;
		if(isset($_SERVER["QUERY_STRING"])){$query = $_SERVER["QUERY_STRING"];}
		$httpqs=new local_badiunet_httpquerystring($query);
		if(!$httpqs->existKey('_key')){$this->printContent("Param _key is required");}
        $this->setParamquery($httpqs->getParam());
       
    }
    
    function remoteRegister(){
		$appkeyinstance = optional_param('_appservicekeyinstance',NULL,PARAM_TEXT);
        $util = new local_badiunet_util();
        $netlib = new local_badiunet_netlib($appkeyinstance);
        $tcript = new local_badiunet_templatecript();
		
		$sservicid=$netlib->getModuleInstance();
		
		$tkey=$this->maccess->getByKey('tkey');
		$tkey=$tcript->encode('s1',$tkey);
		
		$reqperam=$this->makeParamToRemoteRequest();
        $httpqs=new local_badiunet_httpquerystring();
        $httpqs->add('_service','badiu.moodle.core.lib.remoteaccess');
        $httpqs->add('_function','remoteAccessRequest'); 
        $httpqs->add('_tokensession',$tkey);
		
		$httpqs->add('_param',$reqperam);
        $data=$httpqs->getParam();
		$url = $netlib->getUrlService();
		
        $response= $util->request($url, $data);
	 
        if(!$util->isResponseError($response)){
            $message=$util->getVlueOfArray($response, 'message');
            $status= $util->getVlueOfArray($response, 'status');
            $message=$tcript->decode($message); 
			if($status=='accept' && !empty($message)){ 
               $message=$util->getJson($message);
			   $resadd=$this->addSession($message);
			   if($resadd){
				    $tokenauth=$util->getVlueOfArray($message, '_tokenauth');
					$this->urltarget= $util->getVlueOfArray($message,'badiuneturluth');
					$this->urltarget.="?_tokenauth=$tokenauth";
			   }else {
				    $this->printContent('Insert data to session database in moodle failed');    
			   }
			  
           }
          else if($status=='danied' ){ 
				$info=$util->getVlueOfArray($response, 'info');    
				$message=$util->getVlueOfArray($response, 'message');   
				$msg="Error code: $info <br /> Message: $message";
				$this->printContent($msg); 
		  } 
        }else{
            $this->printContent('Remote access failed. '.$response);    
        }
       
	   
    } 
	function addSession($message) {
		 $util = new local_badiunet_util();
		 $sessionlib=new local_badiunet_sessionlib();
         $dto=new stdClass;
         $dto->name=$util->getVlueOfArray($message, '_tokenauth');
         $rkeys=$util->getVlueOfArray($message, 'rkeys');
		 $value=array();
		 $value['rkeys']=$rkeys;
		 $dto->value=json_encode($value);
		 global $USER;
         $dto->userid=$USER->id;
         $dto->dtype='badiu.net';
         $dto->timeexpire=time()+300;
         $rid=$sessionlib->add($dto);
		 return $rid;
		 
	}
	 function makeParamToRemoteRequest() {
			 global $CFG;
			 $tcript = new local_badiunet_templatecript();
			  $util = new local_badiunet_util();
			$reqparam=array();
			$options=array();
        
			$tkey=$this->maccess->getByKey('tkey');
			$rkeys=$this->maccess->getRkeys($tkey);
			$choosekey = rand(0, 19);
			$rkey= $util->getVlueOfArray($rkeys, $choosekey);
			
			$reqparam['moodleurl'] = $CFG->wwwroot;
			$reqparam['moodleversiontxt'] = $CFG->release;
			$reqparam['moodleversionnumber'] = $CFG->version;
			$reqparam['moodledbtype'] = $CFG->dbtype;
			$reqparam['client']=array('browser'=>$_SERVER['HTTP_USER_AGENT'],'ip'=>$_SERVER["REMOTE_ADDR"],'moodleurl'=>$CFG->wwwroot);
			$reqparam['requestparam']=$this->getParamquery();
			$reqparam['rkey']=$rkey;
			$reqparam=json_encode($reqparam);
			$reqparam=$tcript->encode('s1',$reqparam);
			
			return $reqparam;
		 }
	
     function execRedirect(){
		 redirect($this->urltarget);
         exit;
     }
        
    function checkRegister(){
       
        global $CFG;
        $token=$this->netlib->getToken();
         if(empty($token)){
            $url=$CFG->wwwroot.'/local/badiunet/fcservice/status.php';
            redirect($url);
        }
      }

 function checkLogin() {
	   
       $maccess=new local_badiunet_maccess();
       $islogged=$maccess->isLogged();
	   if(!$islogged){printContent('You are not logged in MReport system');}
	   
   }
   
   function getParamquery() {
        return $this->paramquery;
    }
	
	function setParamquery($paramquery) {
        $this->paramquery = $paramquery;
    }
}

?>
