<?php

require_once("../../../config.php");
require_once("$CFG->dirroot/local/badiunet/lib/util.php");
require_once("$CFG->dirroot/local/badiunet/lib/netlib.php");
require_once("$CFG->dirroot/local/badiunet/fcservice/maccess.php");
require_once("$CFG->dirroot/local/badiunet/fcservice/factorycontent.php");



$factorycontent=new local_badiunet_factorycontent('formated');
if(!$factorycontent->browseAsGuest()){require_login();}

$factorycontent->init();

$context = context_system::instance();


$PAGE->set_context($context);
$PAGE->set_url('/local/badiunet/fcservice/index.php');
$PAGE->set_pagelayout('standard');
$PAGE->set_title($factorycontent->getPagetitle());
$PAGE->set_heading($factorycontent->getPagetitle());

$factorycontent->makeNavbar();

echo $OUTPUT->header();

echo $factorycontent->getContent();

echo $OUTPUT->footer();
echo $factorycontent->makeMenu();

?>
