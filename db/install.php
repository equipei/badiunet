<?php

defined('MOODLE_INTERNAL') || die();

function xmldb_local_badiunet_install() {
    global $DB;

	$pconfg = new stdClass();
    $pconfg->plugin = 'local_badiunet';
    $pconfg->name = 'serviceenable';
    $pconfg->value = '1';
    $id = $DB->insert_record('config_plugins', $pconfg);
	
	$pconfg = new stdClass();
    $pconfg->plugin = 'local_badiunet';
    $pconfg->name = 'enablechat';
    $pconfg->value = '1';
    $id = $DB->insert_record('config_plugins', $pconfg);
	
	$pconfg = new stdClass();
    $pconfg->plugin = 'local_badiunet';
    $pconfg->name = 'levelchatagent';
    $pconfg->value = '200';
    $id = $DB->insert_record('config_plugins', $pconfg);

	$pconfg = new stdClass();
    $pconfg->plugin = 'local_badiunet';
    $pconfg->name = 'showchatinallpages';
    $pconfg->value = '0';
    $id = $DB->insert_record('config_plugins', $pconfg);
	
	$pconfg = new stdClass();
    $pconfg->plugin = 'local_badiunet';
    $pconfg->name = 'showchatoutsidecourseforroles';
    $pconfg->value = 'student,editingteacher,manager';
    $id = $DB->insert_record('config_plugins', $pconfg);
	
	
    $pconfg = new stdClass();
    $pconfg->plugin = 'local_badiunet';
    $pconfg->name = 'enviroment';
    $pconfg->value = 'level1';
    $id = $DB->insert_record('config_plugins', $pconfg);
	
	
    $pconfg = new stdClass();
    $pconfg->plugin = 'local_badiunet';
    $pconfg->name = 'defaultmodulekey';
    $pconfg->value = '';
    $id = $DB->insert_record('config_plugins', $pconfg);
    
    $pconfg = new stdClass();
    $pconfg->plugin = 'local_badiunet';
    $pconfg->name = 'defaultmoduleurlparam';
    $pconfg->value = '';
    $id = $DB->insert_record('config_plugins', $pconfg);
    
   $pconfg = new stdClass();
    $pconfg->plugin = 'local_badiunet';
    $pconfg->name = 'keysyncuser';
    $pconfg->value = 'username';
    $id = $DB->insert_record('config_plugins', $pconfg);
    
    $pconfg = new stdClass();
    $pconfg->plugin = 'local_badiunet';
    $pconfg->name = 'servicetoken';
    $pconfg->value = '';
    $id = $DB->insert_record('config_plugins', $pconfg);
    
    $pconfg = new stdClass();
    $pconfg->plugin = 'local_badiunet';
    $pconfg->name = 'serviceurl';
    $pconfg->value = 'https://mreport1.badiu.com.br';
    $id = $DB->insert_record('config_plugins', $pconfg);
    
  
    $pconfg = new stdClass();
    $pconfg->plugin = 'local_badiunet';
    $pconfg->name = 'criptk1';
    $pconfg->value = '';
    $id = $DB->insert_record('config_plugins', $pconfg);
    
    $pconfg = new stdClass();
    $pconfg->plugin = 'local_badiunet';
    $pconfg->name = 'criptk2';
    $pconfg->value = '';
    $id = $DB->insert_record('config_plugins', $pconfg);
    
    $pconfg = new stdClass();
    $pconfg->plugin = 'local_badiunet';
    $pconfg->name = 'serverrmoteipallowed';
    $pconfg->value = '';
    $id = $DB->insert_record('config_plugins', $pconfg);
	
	   
	$pconfg = new stdClass();
    $pconfg->plugin = 'local_badiunet';
    $pconfg->name = 'enableremoteauth';
    $pconfg->value = '0';
    $id = $DB->insert_record('config_plugins', $pconfg);
		
	$pconfg = new stdClass();
    $pconfg->plugin = 'local_badiunet';
    $pconfg->name = 'autosyncuser';
    $pconfg->value = '0';
    $id = $DB->insert_record('config_plugins', $pconfg);
	
	
    $pconfg = new stdClass();
    $pconfg->plugin = 'local_badiunet';
    $pconfg->name = 'dconfig';
    $pconfg->value = '';
    $id = $DB->insert_record('config_plugins', $pconfg);
   
}