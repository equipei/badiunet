<?php
defined('MOODLE_INTERNAL') || die();

function  xmldb_local_badiunet_upgrade($oldversion) {
      global $CFG, $THEME, $DB;
      $dbman = $DB->get_manager();
      
       if ($oldversion < 2022060400) {
        require_once ("upgradetotoversion1.2.php");
            $uptov12=new local_badiunet_upgrade_to_version12();
      
        upgrade_plugin_savepoint(true, 2022060400, 'local', 'badiunet');
      
       } 
	   if ($oldversion < 2025010100) {
        require_once ("upgradetotoversion1.4.php");
		 $uptov12=new local_badiunet_upgrade_to_version14();
      
        upgrade_plugin_savepoint(true, 2025010100, 'local', 'badiunet');
      
       } 
        return true;
}

?>