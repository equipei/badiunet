<?php
defined('MOODLE_INTERNAL') || die();

class local_badiunet_upgrade_to_version12 {

    function __construct() {
        $this->exec();
    }
    function  exec() {
        $this->create_table_sserver();
        $this->create_table_module();
        $this->create_table_data(); 
        $this->create_table_course();
        $this->create_table_categories();
        $this->create_table_user();
        $this->create_table_enrol();
        $this->create_table_log();
        $this->create_table_log_sumarize();
        $this->create_table_mod_gisumarize();
		$this->create_table_general_sumarize();
		$this->update_setting();
    }

	function  update_setting() {
		$this->update_settingdb('enableaccessremotecsddata','1');
		$this->update_settingdb('enableremoteauth','0');
		$this->update_settingdb('enableremotecoursebackup','0');
		$this->update_settingdb('enableremotecourserestore','0');
		$this->update_settingdb('enableremotewebservice','0');
		$this->update_settingdb('enableremoteconsolidate','1');
		$this->update_settingdb('autosyncuser','0');
		$this->update_settingdb('keysyncuser','');
		
	}
	function  update_settingdb($key,$value) {
		global $DB;
		if(!empty($key)){
			$exist=$DB->record_exists('config_plugins', array('plugin'=>'local_badiunet','name'=>$key));
			if(!$exist){
				$pconfg = new stdClass();
				$pconfg->plugin = 'local_badiunet';
				$pconfg->name = $key;
				$pconfg->value = $value;
				$id = $DB->insert_record('config_plugins', $pconfg);
			}
		}
		

	}
    function  create_table_sserver() {
        global $DB;
        $dbman = $DB->get_manager();     
        $table = new xmldb_table('local_badiunet_sserver');
        if (!$dbman->table_exists($table)) {
            //id
            $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
            $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
            $dbman->create_table($table);
        }

        //name
        $field = new xmldb_field('name', XMLDB_TYPE_CHAR, '100', null, XMLDB_NOTNULL,null, null, 'id');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        //sstatus
        $field = new xmldb_field('sstatus', XMLDB_TYPE_CHAR, '50', null, XMLDB_NOTNULL,null, null, 'name');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
		
		   //enviroment
          $field = new xmldb_field('enviroment', XMLDB_TYPE_CHAR, '50', null, XMLDB_NOTNULL,null, null, 'sstatus');
          if (!$dbman->field_exists($table, $field)) {
              $dbman->add_field($table, $field);
          }
         //serviceurl
         $field = new xmldb_field('serviceurl', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL,null, null, 'enviroment');
         if (!$dbman->field_exists($table, $field)) {
             $dbman->add_field($table, $field);
         }

          //servicetoken
          $field = new xmldb_field('servicetoken', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL,null, null, 'serviceurl');
          if (!$dbman->field_exists($table, $field)) {
              $dbman->add_field($table, $field);
          }

        //defaultmodulekey
        $field = new xmldb_field('defaultmodulekey', XMLDB_TYPE_TEXT, null, null, null, null, 'servicetoken');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        
        //criptk1
        $field = new xmldb_field('criptk1', XMLDB_TYPE_TEXT, null, null, null, null, 'defaultmodulekey');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        //criptk2
        $field = new xmldb_field('criptk2', XMLDB_TYPE_TEXT, null, null, null, null, 'criptk1');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        //serverrmoteipallowed
        $field = new xmldb_field('serverrmoteipallowed', XMLDB_TYPE_CHAR, '255', null, null,null, null, 'criptk2');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        //servicekeyinstance
        $field = new xmldb_field('servicekeyinstance', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL,null, null, 'serverrmoteipallowed');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

		
		//enableremoteauth
        $field = new xmldb_field('enableremoteauth',XMLDB_TYPE_INTEGER, '2',  null, null, null, '0', 'serverrmoteipallowed');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
		
		
		//autosyncuser
        $field = new xmldb_field('autosyncuser',XMLDB_TYPE_INTEGER, '2',  null, null, null, '0', 'enableremoteauth');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
		
		//keysyncuser
        $field = new xmldb_field('keysyncuser', XMLDB_TYPE_CHAR, '50', null, XMLDB_NOTNULL,null, null, 'autosyncuser');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
		
			 
		 //dtype
        $field = new xmldb_field('dtype', XMLDB_TYPE_CHAR, '50', null, XMLDB_NOTNULL,null, null, 'keysyncuser');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
		
		//defaultsystem
        $field = new xmldb_field('defaultsystem',XMLDB_TYPE_INTEGER, '2',  null, null, null, '0', 'dtype');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
		
		//proxyenable
        $field = new xmldb_field('proxyenable',XMLDB_TYPE_INTEGER, '2',  null, null, null, '0', 'defaultsystem');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
		
		 //proxyrole
         $field = new xmldb_field('proxyrole', XMLDB_TYPE_TEXT, null, null, null, null, 'proxyenable');
         if (!$dbman->field_exists($table, $field)) {
             $dbman->add_field($table, $field);
         }
		 
         //dconfig
         $field = new xmldb_field('dconfig', XMLDB_TYPE_TEXT, null, null, null, null, 'anonymousaccesskeys');
         if (!$dbman->field_exists($table, $field)) {
             $dbman->add_field($table, $field);
         }

          //description
          $field = new xmldb_field('description', XMLDB_TYPE_TEXT, null, null, null, null, 'dconfig');
          if (!$dbman->field_exists($table, $field)) {
              $dbman->add_field($table, $field);
          }
        //timecreated
        $field = new xmldb_field('timecreated',XMLDB_TYPE_INTEGER, '10',  null, null, null, '0', 'description');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        //timemodified
        $field = new xmldb_field('timemodified', XMLDB_TYPE_INTEGER, '10',  null, null, null, '0', 'timecreated');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

       //add index  
         $index = new xmldb_index('name');
         $index->set_attributes(XMLDB_INDEX_UNIQUE, array('name'));
        if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}

         //add index  
         $index = new xmldb_index('servicekeyinstance');
         $index->set_attributes(XMLDB_INDEX_UNIQUE, array('servicekeyinstance'));
        if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}

        $index = new xmldb_index('sstatus');
        $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('sstatus'));
         if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}

        $index = new xmldb_index('timecreated');
        $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('timecreated'));
         if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}

        $index = new xmldb_index('timemodified');
         $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('timemodified'));
        if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}
    }
    function  create_table_module() {
        global $DB;
        $dbman = $DB->get_manager();     
        $table = new xmldb_table('local_badiunet_module');
        if (!$dbman->table_exists($table)) {
            //id
            $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
            $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
            $dbman->create_table($table);
        }

        //name
        $field = new xmldb_field('modulekey', XMLDB_TYPE_CHAR, '100', null, XMLDB_NOTNULL,null, null, 'id');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

     

          //sserverid
          $field = new xmldb_field('sserverid', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL,null, null, 'modulekey');
          if (!$dbman->field_exists($table, $field)) {
              $dbman->add_field($table, $field);
          }

      
         //dconfig
         $field = new xmldb_field('dconfig', XMLDB_TYPE_TEXT, null, null, null, null, 'sserverid');
         if (!$dbman->field_exists($table, $field)) {
             $dbman->add_field($table, $field);
         }

          //description
          $field = new xmldb_field('description', XMLDB_TYPE_TEXT, null, null, null, null, 'dconfig');
          if (!$dbman->field_exists($table, $field)) {
              $dbman->add_field($table, $field);
          }
        //timecreated
        $field = new xmldb_field('timecreated',XMLDB_TYPE_INTEGER, '10',  null, null, null, '0', 'description');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        //timemodified
        $field = new xmldb_field('timemodified', XMLDB_TYPE_INTEGER, '10',  null, null, null, '0', 'timecreated');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

       //add index  
         $index = new xmldb_index('modulekey');
         $index->set_attributes(XMLDB_INDEX_UNIQUE, array('modulekey'));
        if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}

        $index = new xmldb_index('sserverid');
        $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('sserverid'));
         if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}

        $index = new xmldb_index('timecreated');
        $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('timecreated'));
         if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}

        $index = new xmldb_index('timemodified');
         $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('timemodified'));
        if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}
    }
    function  create_table_data() {
        global $DB;
        $dbman = $DB->get_manager();     
        $table = new xmldb_table('local_badiunet_data');
        if (!$dbman->table_exists($table)) {
            //id
            $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
            $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
            $dbman->create_table($table);
        }

        //name
        $field = new xmldb_field('name', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL,null, null, 'id');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        //value
        $field = new xmldb_field('value', XMLDB_TYPE_TEXT, null, null, null, null, 'name');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        
        //timecreated
        $field = new xmldb_field('timecreated',XMLDB_TYPE_INTEGER, '10',  null, null, null, '0', 'value');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        $field = new xmldb_field('timemodified', XMLDB_TYPE_INTEGER, '10',  null, null, null, '0', 'timecreated');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

       //add index  
         $index = new xmldb_index('name');
         $index->set_attributes(XMLDB_INDEX_UNIQUE, array('name'));
        if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}
    }

    function  create_table_course() {
        global $DB;
        $dbman = $DB->get_manager();     
        $table = new xmldb_table('local_badiunet_course');
        if (!$dbman->table_exists($table)) {
            //id
            $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
            $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
            $dbman->create_table($table);
        }

          //courseid
          $field = new xmldb_field('courseid',XMLDB_TYPE_INTEGER, '10',  null, XMLDB_NOTNULL, null, '0', 'id');
          if (!$dbman->field_exists($table, $field)) {
              $dbman->add_field($table, $field);
          }
        //name
        $field = new xmldb_field('name', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL,null, null, 'courseid');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        //value
        $field = new xmldb_field('value', XMLDB_TYPE_TEXT, null, null, null, null, 'name');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('valueint',XMLDB_TYPE_INTEGER, '10',  null, null, null, null, 'value');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('valuenumber',XMLDB_TYPE_NUMBER, '10,5',  null, null, null, null, 'valueint');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        //timecreated
        $field = new xmldb_field('timecreated',XMLDB_TYPE_INTEGER, '10',  null, null, null, '0', 'valuenumber');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        $field = new xmldb_field('timemodified', XMLDB_TYPE_INTEGER, '10',  null, null, null, '0', 'timecreated');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

          //add index  
         $index= new xmldb_index('courseid_name');
         $index->set_attributes(XMLDB_INDEX_UNIQUE, array('name','courseid'));
        if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}

        $index = new xmldb_index('courseid');
        $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('courseid'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}

       $index = new xmldb_index('name');
        $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('name'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}

       $index = new xmldb_index('valueint');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('valueint'));
      if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}

      $index = new xmldb_index('valuenumber');
      $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('valuenumber'));
     if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}

        $index = new xmldb_index('timecreated');
         $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('timecreated'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}

       $index = new xmldb_index('timemodified');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('timemodified'));
      if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}
    }


    function  create_table_categories() {
        global $DB;
        $dbman = $DB->get_manager();     
        $table = new xmldb_table('local_badiunet_categories');
        if (!$dbman->table_exists($table)) {
            //id
            $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
            $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
            $dbman->create_table($table);
        }

          $field = new xmldb_field('categoryid',XMLDB_TYPE_INTEGER, '10',  null, XMLDB_NOTNULL, null, '0', 'id');
          if (!$dbman->field_exists($table, $field)) {
              $dbman->add_field($table, $field);
          }
        //name
        $field = new xmldb_field('name', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL,null, null, 'categoryid');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        //value
        $field = new xmldb_field('value', XMLDB_TYPE_TEXT, null, null, null, null, 'name');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('valueint',XMLDB_TYPE_INTEGER, '10',  null, null, null, null, 'value');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('valuenumber',XMLDB_TYPE_NUMBER, '10,5',  null, null, null, null, 'valueint');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        //timecreated
        $field = new xmldb_field('timecreated',XMLDB_TYPE_INTEGER, '10',  null, null, null, '0', 'valuenumber');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        $field = new xmldb_field('timemodified', XMLDB_TYPE_INTEGER, '10',  null, null, null, '0', 'timecreated');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

          //add index  
         $index= new xmldb_index('categoryid_name');
         $index->set_attributes(XMLDB_INDEX_UNIQUE, array('name','categoryid'));
        if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}

        $index = new xmldb_index('categoryid');
        $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('categoryid'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}

       $index = new xmldb_index('name');
        $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('name'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}

       $index = new xmldb_index('valueint');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('valueint'));
      if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}

      $index = new xmldb_index('valuenumber');
      $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('valuenumber'));
     if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}

        $index = new xmldb_index('timecreated');
         $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('timecreated'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}

       $index = new xmldb_index('timemodified');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('timemodified'));
      if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}
    }

    function  create_table_user() {
        global $DB;
        $dbman = $DB->get_manager();     
        $table = new xmldb_table('local_badiunet_user');
        if (!$dbman->table_exists($table)) {
            //id
            $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
            $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
            $dbman->create_table($table);
        }

		 $field = new xmldb_field('userid',XMLDB_TYPE_INTEGER, '10',  null, XMLDB_NOTNULL, null, '0', 'id');
          if (!$dbman->field_exists($table, $field)) {
              $dbman->add_field($table, $field);
          }
        //name
        $field = new xmldb_field('name', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL,null, null, 'userid');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        //value
        $field = new xmldb_field('value', XMLDB_TYPE_TEXT, null, null, null, null, 'name');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('valueint',XMLDB_TYPE_INTEGER, '10',  null, null, null, null, 'value');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('valuenumber',XMLDB_TYPE_NUMBER, '10,5',  null, null, null, null, 'valueint');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        //timecreated
        $field = new xmldb_field('timecreated',XMLDB_TYPE_INTEGER, '10',  null, null, null, '0', 'valuenumber');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        $field = new xmldb_field('timemodified', XMLDB_TYPE_INTEGER, '10',  null, null, null, '0', 'timecreated');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

          //add index  
		 $index= new xmldb_index('userid_name');
         $index->set_attributes(XMLDB_INDEX_UNIQUE, array('name','userid'));
        if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}

        $index = new xmldb_index('userid');
        $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('userid'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}

       $index = new xmldb_index('name');
        $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('name'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}

       $index = new xmldb_index('valueint');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('valueint'));
      if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}

      $index = new xmldb_index('valuenumber');
      $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('valuenumber'));
     if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}

        $index = new xmldb_index('timecreated');
         $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('timecreated'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}

       $index = new xmldb_index('timemodified');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('timemodified'));
      if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}
    }

    
    function  create_table_enrol() {
        global $DB;
        $dbman = $DB->get_manager();     
        $table = new xmldb_table('local_badiunet_user_enrol');
        if (!$dbman->table_exists($table)) {
            //id
            $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
            $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
            $dbman->create_table($table);
        }


          $field = new xmldb_field('userid',XMLDB_TYPE_INTEGER, '10',  null, XMLDB_NOTNULL, null, '0', 'id');
          if (!$dbman->field_exists($table, $field)) {
              $dbman->add_field($table, $field);
          }
        
          $field = new xmldb_field('courseid',XMLDB_TYPE_INTEGER, '10',  null, XMLDB_NOTNULL, null, '0', 'userid');
          if (!$dbman->field_exists($table, $field)) {
              $dbman->add_field($table, $field);
          }

        $field = new xmldb_field('name', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL,null, null, 'courseid');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        //value
        $field = new xmldb_field('value', XMLDB_TYPE_TEXT, null, null, null, null, 'name');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('valueint',XMLDB_TYPE_INTEGER, '10',  null, null, null, null, 'value');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('valuenumber',XMLDB_TYPE_NUMBER, '10,5',  null, null, null, null, 'valueint');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        //timecreated
        $field = new xmldb_field('timecreated',XMLDB_TYPE_INTEGER, '10',  null, null, null, '0', 'valuenumber');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        $field = new xmldb_field('timemodified', XMLDB_TYPE_INTEGER, '10',  null, null, null, '0', 'timecreated');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

          //add index  
         $index= new xmldb_index('userid_courseid_name');
         $index->set_attributes(XMLDB_INDEX_UNIQUE, array('userid','courseid','name'));
        if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}

        $index = new xmldb_index('userid');
        $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('userid'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}

       $index = new xmldb_index('courseid');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('courseid'));
      if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}

       $index = new xmldb_index('name');
        $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('name'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}

       $index = new xmldb_index('valueint');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('valueint'));
      if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}

      $index = new xmldb_index('valuenumber');
      $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('valuenumber'));
     if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}

        $index = new xmldb_index('timecreated');
         $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('timecreated'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}

       $index = new xmldb_index('timemodified');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('timemodified'));
      if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}
    }

    function  create_table_log() {
        global $DB;
        $dbman = $DB->get_manager();     
        $table = new xmldb_table('local_badiunet_log');
        if (!$dbman->table_exists($table)) {
            //id
            $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
            $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
            $dbman->create_table($table);
        }


          $field = new xmldb_field('instanceid',XMLDB_TYPE_INTEGER, '10',  null, XMLDB_NOTNULL, null, '0', 'id');
          if (!$dbman->field_exists($table, $field)) {
              $dbman->add_field($table, $field);
          }
        
          $field = new xmldb_field('instancetype', XMLDB_TYPE_CHAR, '50', null, XMLDB_NOTNULL,null, null, 'instanceid');
          if (!$dbman->field_exists($table, $field)) {
              $dbman->add_field($table, $field);
          }

        $field = new xmldb_field('name', XMLDB_TYPE_CHAR, '200', null, XMLDB_NOTNULL,null, null, 'instancetype');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        //value
        $field = new xmldb_field('value', XMLDB_TYPE_TEXT, null, null, null, null, 'name');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('valueint',XMLDB_TYPE_INTEGER, '10',  null, null, null, null, 'value');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        $field = new xmldb_field('valuenumber',XMLDB_TYPE_NUMBER, '10,5',  null, null, null, null, 'valueint');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        //timecreated
        $field = new xmldb_field('timecreated',XMLDB_TYPE_INTEGER, '10',  null, null, null, '0', 'valuenumber');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        $field = new xmldb_field('timemodified', XMLDB_TYPE_INTEGER, '10',  null, null, null, '0', 'timecreated');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

          //add index  
          $index= new xmldb_index('instancetype_instancetype_name');
          $index->set_attributes(XMLDB_INDEX_UNIQUE, array('instanceid','instancetype','name'));
         if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}
 
         $index = new xmldb_index('instanceid');
         $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('instanceid'));
        if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}
 
        $index = new xmldb_index('instancetype');
        $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('instancetype'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}

       $index = new xmldb_index('name');
        $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('name'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}

       $index = new xmldb_index('valueint');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('valueint'));
      if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}

      $index = new xmldb_index('valuenumber');
      $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('valuenumber'));
     if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}

        $index = new xmldb_index('timecreated');
         $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('timecreated'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}

       $index = new xmldb_index('timemodified');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('timemodified'));
      if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}
    }


    function  create_table_log_sumarize() {
        global $DB;
        $dbman = $DB->get_manager();     
        $table = new xmldb_table('local_badiunet_log_sumarize');
        if (!$dbman->table_exists($table)) {
            //id
            $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
            $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
            $dbman->create_table($table);
        }

        $field = new xmldb_field('name', XMLDB_TYPE_CHAR, '255', null, XMLDB_NOTNULL,null, null, 'id');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

          $field = new xmldb_field('logid',XMLDB_TYPE_INTEGER, '10',  null, XMLDB_NOTNULL, null, '0', 'name');
          if (!$dbman->field_exists($table, $field)) {
              $dbman->add_field($table, $field);
          }
        
          $field = new xmldb_field('logtimecreated',XMLDB_TYPE_INTEGER, '10',  null, XMLDB_NOTNULL, null, '0', 'logid');
          if (!$dbman->field_exists($table, $field)) {
              $dbman->add_field($table, $field);
          }
          $field = new xmldb_field('userid',XMLDB_TYPE_INTEGER, '10',  null, null, null, null, 'logtimecreated');
          if (!$dbman->field_exists($table, $field)) {
              $dbman->add_field($table, $field);
          }
          $field = new xmldb_field('courseid',XMLDB_TYPE_INTEGER, '10',  null, null, null, null, 'userid');
          if (!$dbman->field_exists($table, $field)) {
              $dbman->add_field($table, $field);
          }
          $field = new xmldb_field('cmid',XMLDB_TYPE_INTEGER, '10',  null, null, null, null, 'courseid');
          if (!$dbman->field_exists($table, $field)) {
              $dbman->add_field($table, $field);
          }
          $field = new xmldb_field('module', XMLDB_TYPE_CHAR, '255', null, null,null, null, 'cmid');
          if (!$dbman->field_exists($table, $field)) {
              $dbman->add_field($table, $field);
          }
          $field = new xmldb_field('action', XMLDB_TYPE_CHAR, '255', null, null,null, null, 'module');
          if (!$dbman->field_exists($table, $field)) {
              $dbman->add_field($table, $field);
          }
          $field = new xmldb_field('actiontype', XMLDB_TYPE_CHAR, '255', null, null,null, null, 'action');
          if (!$dbman->field_exists($table, $field)) {
              $dbman->add_field($table, $field);
          }

  
          $field = new xmldb_field('year',XMLDB_TYPE_INTEGER, '10',  null, null, null, null, 'actiontype');
          if (!$dbman->field_exists($table, $field)) {
              $dbman->add_field($table, $field);
          }
          $field = new xmldb_field('month',XMLDB_TYPE_INTEGER, '10',  null, null, null, null, 'year');
          if (!$dbman->field_exists($table, $field)) {
              $dbman->add_field($table, $field);
          }
          $field = new xmldb_field('day',XMLDB_TYPE_INTEGER, '10',  null, null, null, null, 'month');
          if (!$dbman->field_exists($table, $field)) {
              $dbman->add_field($table, $field);
          }
          $field = new xmldb_field('week',XMLDB_TYPE_INTEGER, '10',  null, null, null, null, 'day');
          if (!$dbman->field_exists($table, $field)) {
              $dbman->add_field($table, $field);
          }
          $field = new xmldb_field('hour',XMLDB_TYPE_INTEGER, '10',  null, null, null, null, 'week');
          if (!$dbman->field_exists($table, $field)) {
              $dbman->add_field($table, $field);
          }
          $field = new xmldb_field('minute',XMLDB_TYPE_INTEGER, '10',  null, null, null, null, 'hour');
          if (!$dbman->field_exists($table, $field)) {
              $dbman->add_field($table, $field);
          }
          $field = new xmldb_field('duration',XMLDB_TYPE_INTEGER, '10',  null, null, null, null, 'minute');
          if (!$dbman->field_exists($table, $field)) {
              $dbman->add_field($table, $field);
          }

          
          $field = new xmldb_field('timecreated',XMLDB_TYPE_INTEGER, '10',  null, null, null, '0', 'duration');
          if (!$dbman->field_exists($table, $field)) {
              $dbman->add_field($table, $field);
          }
          $field = new xmldb_field('timemodified', XMLDB_TYPE_INTEGER, '10',  null, null, null, '0', 'timecreated');
          if (!$dbman->field_exists($table, $field)) {
              $dbman->add_field($table, $field);
          }
  
       

          //add index  
         $index= new xmldb_index('logid_name');
         $index->set_attributes(XMLDB_INDEX_UNIQUE, array('logid','name'));
        if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}

        $index = new xmldb_index('logid');
        $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('logid'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}

       $index = new xmldb_index('logtimecreated');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('logtimecreated'));
      if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}

       $index = new xmldb_index('name');
        $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('name'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}

       $index = new xmldb_index('userid');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('userid'));
      if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}

      $index = new xmldb_index('courseid');
      $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('courseid'));
     if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}

       $index = new xmldb_index('module');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('module'));
      if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}
      
      $index = new xmldb_index('action');
      $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('action'));
     if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}

     $index = new xmldb_index('actiontype');
      $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('actiontype'));
     if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}

     $index = new xmldb_index('year');
      $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('year'));
     if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}

     $index = new xmldb_index('month');
      $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('month'));
     if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}

     $index = new xmldb_index('day');
      $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('day'));
     if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}

     $index = new xmldb_index('week');
      $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('week'));
     if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}

     $index = new xmldb_index('minute');
      $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('minute'));
     if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}

     $index = new xmldb_index('duration');
      $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('duration'));
     if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}
     
        $index = new xmldb_index('timecreated');
         $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('timecreated'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}

       $index = new xmldb_index('timemodified');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('timemodified'));
      if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}
    }
	
	
	
    function  create_table_mod_gisumarize() {
        global $DB;
        $dbman = $DB->get_manager();     
        $table = new xmldb_table('local_badiunet_mod_isumarize');
        if (!$dbman->table_exists($table)) {
            //id
            $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
            $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
            $dbman->create_table($table);
        }

        //moodle
        $field = new xmldb_field('moodleid',XMLDB_TYPE_INTEGER, '10',  null, null, null, '0', 'id');
          if (!$dbman->field_exists($table, $field)) {
              $dbman->add_field($table, $field);
          }
       
        $field = new xmldb_field('moodlename', XMLDB_TYPE_CHAR, '255', null, null,null, null, 'moodleid');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

		
        $field = new xmldb_field('moodleurl', XMLDB_TYPE_CHAR, '255', null, null,null, null, 'moodlename');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

     //user
          $field = new xmldb_field('userid',XMLDB_TYPE_INTEGER, '10',  null, XMLDB_NOTNULL, null, '0', 'moodleurl');
          if (!$dbman->field_exists($table, $field)) {
              $dbman->add_field($table, $field);
          }
       
        $field = new xmldb_field('userfirstname', XMLDB_TYPE_CHAR, '255', null, null,null, null, 'userid');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

		
        $field = new xmldb_field('userlastname', XMLDB_TYPE_CHAR, '255', null, null,null, null, 'userfirstname');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
		
		$field = new xmldb_field('useremail', XMLDB_TYPE_CHAR, '255', null, null,null, null, 'userlastname');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
		
		$field = new xmldb_field('useridnumber', XMLDB_TYPE_CHAR, '255', null, null,null, null, 'useremail');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
		
		
        $field = new xmldb_field('userinfo', XMLDB_TYPE_TEXT, null, null, null, null, 'useridnumber');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
            
        //course
        $field = new xmldb_field('courseid',XMLDB_TYPE_INTEGER, '10',  null, null, null, null, 'useridnumber');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
     
      $field = new xmldb_field('coursefullname', XMLDB_TYPE_CHAR, '255', null, null,null, null, 'courseid');
      if (!$dbman->field_exists($table, $field)) {
          $dbman->add_field($table, $field);
      }

      $field = new xmldb_field('courseshortname', XMLDB_TYPE_CHAR, '255', null, null,null, null, 'coursefullname');
      if (!$dbman->field_exists($table, $field)) {
          $dbman->add_field($table, $field);
      }
      
      $field = new xmldb_field('courseidnumber', XMLDB_TYPE_CHAR, '255', null, null,null, null, 'courseshortname');
      if (!$dbman->field_exists($table, $field)) {
          $dbman->add_field($table, $field);
      }

      $field = new xmldb_field('courseinfo', XMLDB_TYPE_TEXT, null, null, null, null, 'courseidnumber');
      if (!$dbman->field_exists($table, $field)) {
          $dbman->add_field($table, $field);
      }

      //course category
      $field = new xmldb_field('coursecatid',XMLDB_TYPE_INTEGER, '10',  null, null, null, null, 'courseinfo');
      if (!$dbman->field_exists($table, $field)) {
          $dbman->add_field($table, $field);
      }
   
    $field = new xmldb_field('coursecatname', XMLDB_TYPE_CHAR, '255', null, null,null, null, 'coursecatid');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
    }

    $field = new xmldb_field('coursecatidnumber', XMLDB_TYPE_CHAR, '255', null, null,null, null, 'coursecatname');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
    }
    
    $field = new xmldb_field('coursecatparent', XMLDB_TYPE_CHAR, '255', null, null,null, null, 'coursecatidnumber');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
    }

    $field = new xmldb_field('coursecatinfo', XMLDB_TYPE_TEXT, null, null, null, null, 'coursecatparent');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
    }
     
    //group
    $field = new xmldb_field('groupid',XMLDB_TYPE_INTEGER, '10',  null, null, null, null, 'coursecatinfo');
      if (!$dbman->field_exists($table, $field)) {
          $dbman->add_field($table, $field);
      }
   
    $field = new xmldb_field('groupname', XMLDB_TYPE_CHAR, '255', null, null,null, null, 'groupid');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
    }

    $field = new xmldb_field('groupidnumber', XMLDB_TYPE_CHAR, '255', null, null,null, null, 'groupname');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
    }
    
   
    $field = new xmldb_field('groupinfo', XMLDB_TYPE_TEXT, null, null, null, null, 'groupidnumber');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
    }
        
    //activity
    $field = new xmldb_field('activityid',XMLDB_TYPE_INTEGER, '10',  null, XMLDB_NOTNULL, null, '0', 'groupinfo');
      if (!$dbman->field_exists($table, $field)) {
          $dbman->add_field($table, $field);
      }
   
      $field = new xmldb_field('activitymodule', XMLDB_TYPE_CHAR, '255', null, null,null, null, 'activityid');
      if (!$dbman->field_exists($table, $field)) {
          $dbman->add_field($table, $field);
      }

      
    $field = new xmldb_field('activityname', XMLDB_TYPE_CHAR, '255', null, null,null, null, 'activitymodule');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
    }

    $field = new xmldb_field('activityidnumber', XMLDB_TYPE_CHAR, '255', null, null,null, null, 'activityname');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
    }
    
  $field = new xmldb_field('activitytimestart',XMLDB_TYPE_INTEGER, '10',  null, null, null, null, 'activityidnumber');
      if (!$dbman->field_exists($table, $field)) {
          $dbman->add_field($table, $field);
      }
   
      $field = new xmldb_field('activitytimeend',XMLDB_TYPE_INTEGER, '10',  null, null, null, null, 'activitytimestart');
      if (!$dbman->field_exists($table, $field)) {
          $dbman->add_field($table, $field);
      }

    $field = new xmldb_field('activityinfo', XMLDB_TYPE_TEXT, null, null, null, null, 'activitytimeend');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
    }

    //response 
    $field = new xmldb_field('responseid',XMLDB_TYPE_INTEGER, '10',  null, XMLDB_NOTNULL, null, '0', 'activityinfo');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
    }

    $field = new xmldb_field('responsetime',XMLDB_TYPE_INTEGER, '10',  null, null, null, null, 'responseid');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
    }

    $field = new xmldb_field('responsecontent', XMLDB_TYPE_TEXT, null, null, null, null, 'responsetime');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
    }

    $field = new xmldb_field('responsegrade',XMLDB_TYPE_NUMBER, '10,5',  null, null, null, null, 'responsecontent');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
    }

     $field = new xmldb_field('responseteachercomment', XMLDB_TYPE_TEXT, null, null, null, null, 'responsegrade');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
    }

    //customchar
    $field = new xmldb_field('customchar1', XMLDB_TYPE_CHAR, '255', null, null,null, null, 'responseteachercomment');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
    }
    $field = new xmldb_field('customchar2', XMLDB_TYPE_CHAR, '255', null, null,null, null, 'customchar1');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
    }
    $field = new xmldb_field('customchar3', XMLDB_TYPE_CHAR, '255', null, null,null, null, 'customchar2');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
    }
    $field = new xmldb_field('customchar4', XMLDB_TYPE_CHAR, '255', null, null,null, null, 'customchar3');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
    }$field = new xmldb_field('customchar5', XMLDB_TYPE_CHAR, '255', null, null,null, null, 'customchar4');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
    }

    //customtext
    $field = new xmldb_field('customtext1', XMLDB_TYPE_TEXT, null, null, null, null, 'customchar5');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
    }
    $field = new xmldb_field('customtext2', XMLDB_TYPE_TEXT, null, null, null, null, 'customtext1');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
    }
    $field = new xmldb_field('customtext3', XMLDB_TYPE_TEXT, null, null, null, null, 'customtext2');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
    }
    $field = new xmldb_field('customtext4', XMLDB_TYPE_TEXT, null, null, null, null, 'customtext3');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
    }
    $field = new xmldb_field('customtext5', XMLDB_TYPE_TEXT, null, null, null, null, 'customtext4');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
    }

    //customint
    $field = new xmldb_field('customint1',XMLDB_TYPE_INTEGER, '10',  null, null, null, null, 'customtext5');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
    }

    $field = new xmldb_field('customint2',XMLDB_TYPE_INTEGER, '10',  null, null, null, null, 'customint1');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
    }
    $field = new xmldb_field('customint3',XMLDB_TYPE_INTEGER, '10',  null, null, null, null, 'customint2');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
    }
    $field = new xmldb_field('customint4',XMLDB_TYPE_INTEGER, '10',  null, null, null, null, 'customint3');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
    }

    $field = new xmldb_field('customint5',XMLDB_TYPE_INTEGER, '10',  null, null, null, null, 'customint4');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
    }

    //customintnumber
    $field = new xmldb_field('customintnumber1',XMLDB_TYPE_NUMBER, '10,5',  null, null, null, null, 'customint5');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
    }

    $field = new xmldb_field('customintnumber2',XMLDB_TYPE_NUMBER, '10,5',  null, null, null, null, 'customintnumber1');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
    }

    $field = new xmldb_field('customintnumber3',XMLDB_TYPE_NUMBER, '10,5',  null, null, null, null, 'customintnumber2');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
    }
    $field = new xmldb_field('customintnumber4',XMLDB_TYPE_NUMBER, '10,5',  null, null, null, null, 'customintnumber3');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
    }

    $field = new xmldb_field('customintnumber5',XMLDB_TYPE_NUMBER, '10,5',  null, null, null, null, 'customintnumber4');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
    }
    

       //timecreated
       $field = new xmldb_field('timecreated',XMLDB_TYPE_INTEGER, '10',  null,XMLDB_NOTNULL, null, '0', 'customintnumber5');
       if (!$dbman->field_exists($table, $field)) {
           $dbman->add_field($table, $field);
       }

       $field = new xmldb_field('timemodified', XMLDB_TYPE_INTEGER, '10',  null, null, null, '0', 'timecreated');
       if (!$dbman->field_exists($table, $field)) {
           $dbman->add_field($table, $field);
       }

    //add index  
        //moodle
        $index = new xmldb_index('moodleid');
        $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('moodleid'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}

        $index = new xmldb_index('moodlename');
        $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('moodlename'));
        if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}

        $index = new xmldb_index('moodleurl');
        $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('moodleurl'));
        if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}

    //user
         $index = new xmldb_index('userid');
        $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('userid'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}

        $index = new xmldb_index('userfirstname');
        $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('userfirstname'));
        if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}

        $index = new xmldb_index('userlastname');
        $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('userlastname'));
        if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}

       $index = new xmldb_index('useremail');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('useremail'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}

       $index = new xmldb_index('useridnumber');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('useridnumber'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}

       //course
       $index = new xmldb_index('courseid');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('courseid'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}

       $index = new xmldb_index('coursefullname');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('coursefullname'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}

       $index = new xmldb_index('courseshortname');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('courseshortname'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}


       $index = new xmldb_index('courseidnumber');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('courseidnumber'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}

       
       //coursecat
       $index = new xmldb_index('coursecatid');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('coursecatid'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}

       $index = new xmldb_index('coursecatname');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('coursecatname'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}

       $index = new xmldb_index('coursecatidnumber');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('coursecatidnumber'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}

   //coursecat
       $index = new xmldb_index('coursecatid');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('coursecatid'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}

       $index = new xmldb_index('coursecatname');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('coursecatname'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}

       $index = new xmldb_index('coursecatidnumber');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('coursecatidnumber'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}

          //group
          $index = new xmldb_index('groupid');
          $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('groupid'));
          if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}
   
          $index = new xmldb_index('groupname');
          $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('groupname'));
          if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}
   
          $index = new xmldb_index('groupidnumber');
          $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('groupidnumber'));
          if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}
   
   //activity
       $index = new xmldb_index('activityid');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('activityid'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}

       $index = new xmldb_index('activityname');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('activityname'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}

       $index = new xmldb_index('activityidnumber');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('activityidnumber'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}   

       $index = new xmldb_index('activityidnumber');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('activityidnumber'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);} 

       $index = new xmldb_index('activitymodule');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('activitymodule'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);} 

       $index = new xmldb_index('activitytimestart');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('activitytimestart'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);} 

       $index = new xmldb_index('activitytimeend');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('activitytimeend'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);} 
     
//response
       $index = new xmldb_index('responseid');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('responseid'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);} 

       $index = new xmldb_index('responsetime');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('responsetime'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);} 

       $index = new xmldb_index('responsegrade');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('responsegrade'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);} 

       //customchar
       $index = new xmldb_index('customchar1');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('customchar1'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);} 

       $index = new xmldb_index('customchar2');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('customchar2'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);} 

              //customint
       $index = new xmldb_index('customint1');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('customint1'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);} 

       $index = new xmldb_index('customint2');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('customint2'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);} 
     
       //customintnumber
       $index = new xmldb_index('customintnumber1');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('customintnumber1'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);} 

       $index = new xmldb_index('customintnumber2');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('customintnumber2'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}        
    }
	
	 function  create_table_general_sumarize() {
        global $DB;
        $dbman = $DB->get_manager();     
        $table = new xmldb_table('local_badiunet_gnl_sumarize');
        if (!$dbman->table_exists($table)) {
            //id
            $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
            $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
            $dbman->create_table($table);
        }

        //moodle
        $field = new xmldb_field('moodleid',XMLDB_TYPE_INTEGER, '10',  null, null, null, '0', 'id');
          if (!$dbman->field_exists($table, $field)) {
              $dbman->add_field($table, $field);
          }
       
        $field = new xmldb_field('moodlename', XMLDB_TYPE_CHAR, '255', null, null,null, null, 'moodleid');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

		
        $field = new xmldb_field('moodleurl', XMLDB_TYPE_CHAR, '255', null, null,null, null, 'moodlename');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

		//name
        $field = new xmldb_field('name', XMLDB_TYPE_CHAR, '100', null, XMLDB_NOTNULL,null, null, 'moodleurl');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
		
		
        //value
        $field = new xmldb_field('value', XMLDB_TYPE_TEXT, null, null, null, null, 'name');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
		
    //customchar
    $field = new xmldb_field('customchar1', XMLDB_TYPE_CHAR, '255', null, null,null, null, 'value');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
    }
    $field = new xmldb_field('customchar2', XMLDB_TYPE_CHAR, '255', null, null,null, null, 'customchar1');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
    }
    $field = new xmldb_field('customchar3', XMLDB_TYPE_CHAR, '255', null, null,null, null, 'customchar2');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
    }
    $field = new xmldb_field('customchar4', XMLDB_TYPE_CHAR, '255', null, null,null, null, 'customchar3');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
    }$field = new xmldb_field('customchar5', XMLDB_TYPE_CHAR, '255', null, null,null, null, 'customchar4');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
    }

    //customtext
    $field = new xmldb_field('customtext1', XMLDB_TYPE_TEXT, null, null, null, null, 'customchar5');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
    }
    $field = new xmldb_field('customtext2', XMLDB_TYPE_TEXT, null, null, null, null, 'customtext1');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
    }
    $field = new xmldb_field('customtext3', XMLDB_TYPE_TEXT, null, null, null, null, 'customtext2');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
    }
    $field = new xmldb_field('customtext4', XMLDB_TYPE_TEXT, null, null, null, null, 'customtext3');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
    }
    $field = new xmldb_field('customtext5', XMLDB_TYPE_TEXT, null, null, null, null, 'customtext4');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
    }

    //customint
    $field = new xmldb_field('customint1',XMLDB_TYPE_INTEGER, '10',  null, null, null, null, 'customtext5');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
    }

    $field = new xmldb_field('customint2',XMLDB_TYPE_INTEGER, '10',  null, null, null, null, 'customint1');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
    }
    $field = new xmldb_field('customint3',XMLDB_TYPE_INTEGER, '10',  null, null, null, null, 'customint2');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
    }
    $field = new xmldb_field('customint4',XMLDB_TYPE_INTEGER, '10',  null, null, null, null, 'customint3');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
    }

    $field = new xmldb_field('customint5',XMLDB_TYPE_INTEGER, '10',  null, null, null, null, 'customint4');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
    }

    //customintnumber
    $field = new xmldb_field('customintnumber1',XMLDB_TYPE_NUMBER, '10,5',  null, null, null, null, 'customint5');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
    }

    $field = new xmldb_field('customintnumber2',XMLDB_TYPE_NUMBER, '10,5',  null, null, null, null, 'customintnumber1');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
    }

    $field = new xmldb_field('customintnumber3',XMLDB_TYPE_NUMBER, '10,5',  null, null, null, null, 'customintnumber2');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
    }
    $field = new xmldb_field('customintnumber4',XMLDB_TYPE_NUMBER, '10,5',  null, null, null, null, 'customintnumber3');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
    }

    $field = new xmldb_field('customintnumber5',XMLDB_TYPE_NUMBER, '10,5',  null, null, null, null, 'customintnumber4');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
    }
    

       //timecreated
       $field = new xmldb_field('timecreated',XMLDB_TYPE_INTEGER, '10',  null,XMLDB_NOTNULL, null, '0', 'customintnumber5');
       if (!$dbman->field_exists($table, $field)) {
           $dbman->add_field($table, $field);
       }

       $field = new xmldb_field('timemodified', XMLDB_TYPE_INTEGER, '10',  null, null, null, '0', 'timecreated');
       if (!$dbman->field_exists($table, $field)) {
           $dbman->add_field($table, $field);
       }

    //add index  
        //moodle
        $index = new xmldb_index('moodleid');
        $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('moodleid'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}

        $index = new xmldb_index('moodlename');
        $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('moodlename'));
        if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}

        $index = new xmldb_index('moodleurl');
        $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('moodleurl'));
        if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}

   	
         $index = new xmldb_index('name');
          $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('name'));
        if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}

       //customchar
       $index = new xmldb_index('customchar1');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('customchar1'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);} 

       $index = new xmldb_index('customchar2');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('customchar2'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);} 
	   
	    $index = new xmldb_index('customchar3');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('customchar3'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);} 
	   
	    $index = new xmldb_index('customchar4');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('customchar4'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);} 
	   
	    $index = new xmldb_index('customchar5');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('customchar5'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);} 

              //customint
       $index = new xmldb_index('customint1');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('customint1'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);} 

       $index = new xmldb_index('customint2');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('customint2'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);} 
     
       $index = new xmldb_index('customint3');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('customint3'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);} 
	   
       $index = new xmldb_index('customint4');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('customint4'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);} 
	   
       $index = new xmldb_index('customint5');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('customint5'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);} 
       
	   //customintnumber
       $index = new xmldb_index('customintnumber1');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('customintnumber1'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);} 

       $index = new xmldb_index('customintnumber2');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('customintnumber2'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}   
	   
	   $index = new xmldb_index('customintnumber3');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('customintnumber3'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}
	   
	   $index = new xmldb_index('customintnumber4');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('customintnumber4'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}
	   
	   $index = new xmldb_index('customintnumber5');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('customintnumber5'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}
    }
}



?>