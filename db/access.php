<?php
$local_badiunet_capabilities = array(
'local/badiunet:config' => array(
        'riskbitmask' => RISK_PERSONAL,
        'captype' => 'read',
        'contextlevel' => CONTEXT_SYSTEM,
        'legacy' => array(
             'manager' => CAP_ALLOW
         )
    ),
    'local/badiunet:viewsystemreport' => array(
        'riskbitmask' => RISK_PERSONAL,
        'captype' => 'read',
        'contextlevel' => CONTEXT_SYSTEM,
        'legacy' => array(
             'manager' => CAP_ALLOW
         )
    ),
    'local/badiunet:viewcoursereport' => array(
        'riskbitmask' => RISK_PERSONAL,
        'captype' => 'read',
        'contextlevel' =>  CONTEXT_COURSE,
        'legacy' => array(
            'teacher' => CAP_ALLOW,
            'editingteacher' => CAP_ALLOW,
            'manager' => CAP_ALLOW
        )
    ),
    'local/badiunet:viewownenrolreport' => array(
        'riskbitmask' => RISK_PERSONAL,
        'captype' => 'read',
        'contextlevel' =>  CONTEXT_COURSE,
        'legacy' => array(
            'teacher' => CAP_ALLOW,
            'editingteacher' => CAP_ALLOW,
            'manager' => CAP_ALLOW,
            'student' => CAP_ALLOW
        )
    ),
    'local/badiunet:viewownuserreport' => array(
        'riskbitmask' => RISK_PERSONAL,
        'captype' => 'read',
        'contextlevel' =>  CONTEXT_USER,
        'legacy' => array(
            'user' => CAP_ALLOW
           
        )
    ),
	    'local/badiunet:usechat' => array(
        'riskbitmask' => RISK_PERSONAL,
        'captype' => 'write',
        'contextlevel' =>  CONTEXT_SYSTEM,
        'legacy' => array(
           'teacher' => CAP_ALLOW,
            'editingteacher' => CAP_ALLOW,
            'manager' => CAP_ALLOW,
            'student' => CAP_ALLOW,
			'user' => CAP_ALLOW
    
        )
    )
);

?>
