<?php
defined('MOODLE_INTERNAL') || die();

class local_badiunet_upgrade_to_version14 {

    function __construct() {
        $this->exec();
    }
    function  exec() {
		$this->create_table_general_data(); 
        $this->create_table_ams_enrol(); 
		$this->update_table_mod_isumarize();
		$this->update_table_gnl_sumarize();
		//$this->update_table_course();
		$this->create_table_sserver();
		
    } 

 function  create_table_general_data() {
        global $DB;
        $dbman = $DB->get_manager();     
        $table = new xmldb_table('local_badiunet_gnl_data');
        if (!$dbman->table_exists($table)) {
            //id
            $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
            $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
            $dbman->create_table($table);
        }

        //uname
        $field = new xmldb_field('uname', XMLDB_TYPE_CHAR, '100', null, XMLDB_NOTNULL,null, null, 'id');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

		//name
        $field = new xmldb_field('name', XMLDB_TYPE_CHAR, '100', null, XMLDB_NOTNULL,null, null, 'uname');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
		
		
        //value
        $field = new xmldb_field('value', XMLDB_TYPE_TEXT, null, null, null, null, 'name');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
		
		//customint
    $field = new xmldb_field('customint1',XMLDB_TYPE_INTEGER, '10',  null, null, null, null, 'value');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
    }

    $field = new xmldb_field('customint2',XMLDB_TYPE_INTEGER, '10',  null, null, null, null, 'customint1');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
    }
    $field = new xmldb_field('customint3',XMLDB_TYPE_INTEGER, '10',  null, null, null, null, 'customint2');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
    }
    $field = new xmldb_field('customint4',XMLDB_TYPE_INTEGER, '10',  null, null, null, null, 'customint3');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
    }

    $field = new xmldb_field('customint5',XMLDB_TYPE_INTEGER, '10',  null, null, null, null, 'customint4');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
	}
	$field = new xmldb_field('customint6',XMLDB_TYPE_INTEGER, '10',  null, null, null, null, 'customint5');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);	
	
	}	
	 $field = new xmldb_field('customint7',XMLDB_TYPE_INTEGER, '10',  null, null, null, null, 'customint6');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
	}	
	 $field = new xmldb_field('customint8',XMLDB_TYPE_INTEGER, '10',  null, null, null, null, 'customint7');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
	}
	$field = new xmldb_field('customint9',XMLDB_TYPE_INTEGER, '10',  null, null, null, null, 'customint8');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
	}
	$field = new xmldb_field('customint10',XMLDB_TYPE_INTEGER, '10',  null, null, null, null, 'customint9');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
	}
	$field = new xmldb_field('customint11',XMLDB_TYPE_INTEGER, '10',  null, null, null, null, 'customint10');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
	}
	$field = new xmldb_field('customint12',XMLDB_TYPE_INTEGER, '10',  null, null, null, null, 'customint11');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
	}
	$field = new xmldb_field('customint13',XMLDB_TYPE_INTEGER, '10',  null, null, null, null, 'customint12');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
	}	

	$field = new xmldb_field('customint14',XMLDB_TYPE_INTEGER, '10',  null, null, null, null, 'customint13');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
	}
	$field = new xmldb_field('customint15',XMLDB_TYPE_INTEGER, '10',  null, null, null, null, 'customint14');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
	}	
    //customchar
    $field = new xmldb_field('customchar1', XMLDB_TYPE_CHAR, '255', null, null,null, null, 'customint15');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
    }
    $field = new xmldb_field('customchar2', XMLDB_TYPE_CHAR, '255', null, null,null, null, 'customchar1');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
    }
    $field = new xmldb_field('customchar3', XMLDB_TYPE_CHAR, '255', null, null,null, null, 'customchar2');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
    }
    $field = new xmldb_field('customchar4', XMLDB_TYPE_CHAR, '255', null, null,null, null, 'customchar3');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
    }$field = new xmldb_field('customchar5', XMLDB_TYPE_CHAR, '255', null, null,null, null, 'customchar4');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
    }

	$field = new xmldb_field('customchar6', XMLDB_TYPE_CHAR, '255', null, null,null, null, 'customchar5');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
    }

	$field = new xmldb_field('customchar7', XMLDB_TYPE_CHAR, '255', null, null,null, null, 'customchar6');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
    }

	$field = new xmldb_field('customchar8', XMLDB_TYPE_CHAR, '255', null, null,null, null, 'customchar7');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
    }

	$field = new xmldb_field('customchar9', XMLDB_TYPE_CHAR, '255', null, null,null, null, 'customchar8');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
    }

	$field = new xmldb_field('customchar10', XMLDB_TYPE_CHAR, '255', null, null,null, null, 'customchar9');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
    }	
	
	 //customintnumber
    $field = new xmldb_field('customintnumber1',XMLDB_TYPE_NUMBER, '10,5',  null, null, null, null, 'customchar10');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
    }

    $field = new xmldb_field('customintnumber2',XMLDB_TYPE_NUMBER, '10,5',  null, null, null, null, 'customintnumber1');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
    }

    $field = new xmldb_field('customintnumber3',XMLDB_TYPE_NUMBER, '10,5',  null, null, null, null, 'customintnumber2');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
    }
    $field = new xmldb_field('customintnumber4',XMLDB_TYPE_NUMBER, '10,5',  null, null, null, null, 'customintnumber3');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
    }

    $field = new xmldb_field('customintnumber5',XMLDB_TYPE_NUMBER, '10,5',  null, null, null, null, 'customintnumber4');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
    }
	
    //customtext
    $field = new xmldb_field('customtext1', XMLDB_TYPE_TEXT, null, null, null, null, 'customintnumber5');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
    }
    $field = new xmldb_field('customtext2', XMLDB_TYPE_TEXT, null, null, null, null, 'customtext1');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
    }
    $field = new xmldb_field('customtext3', XMLDB_TYPE_TEXT, null, null, null, null, 'customtext2');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
    }
    $field = new xmldb_field('customtext4', XMLDB_TYPE_TEXT, null, null, null, null, 'customtext3');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
    }
    $field = new xmldb_field('customtext5', XMLDB_TYPE_TEXT, null, null, null, null, 'customtext4');
    if (!$dbman->field_exists($table, $field)) {
        $dbman->add_field($table, $field);
    }

  

       //timecreated
       $field = new xmldb_field('timecreated',XMLDB_TYPE_INTEGER, '10',  null,XMLDB_NOTNULL, null, '0', 'customtext5');
       if (!$dbman->field_exists($table, $field)) {
           $dbman->add_field($table, $field);
       }

       $field = new xmldb_field('timemodified', XMLDB_TYPE_INTEGER, '10',  null, null, null, '0', 'timecreated');
       if (!$dbman->field_exists($table, $field)) {
           $dbman->add_field($table, $field);
       }

    //add index  
       
         $index = new xmldb_index('uname');
         $index->set_attributes(XMLDB_INDEX_UNIQUE, array('uname'));
        if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}
		
		
         $index = new xmldb_index('name');
          $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('name'));
        if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}

          //customint
       $index = new xmldb_index('customint1');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('customint1'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);} 

       $index = new xmldb_index('customint2');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('customint2'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);} 
     
       $index = new xmldb_index('customint3');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('customint3'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);} 
	   
       $index = new xmldb_index('customint4');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('customint4'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);} 
	   
       $index = new xmldb_index('customint5');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('customint5'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);} 
       
	   
	   
	    $index = new xmldb_index('customint6');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('customint6'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);} 
	   
	    $index = new xmldb_index('customint7');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('customint7'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);} 
	   
	    $index = new xmldb_index('customint8');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('customint8'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);} 
	   
	    $index = new xmldb_index('customint9');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('customint9'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);} 
	   
	     $index = new xmldb_index('customint10');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('customint10'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);} 
	   
	   $index = new xmldb_index('customint11');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('customint11'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);} 

       $index = new xmldb_index('customint12');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('customint12'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);} 
     
       $index = new xmldb_index('customint13');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('customint13'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);} 
	   
       $index = new xmldb_index('customint14');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('customint14'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);} 
	   
       $index = new xmldb_index('customint15');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('customint15'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);} 
	   
       //customchar
       $index = new xmldb_index('customchar1');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('customchar1'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);} 

       $index = new xmldb_index('customchar2');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('customchar2'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);} 
	   
	    $index = new xmldb_index('customchar3');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('customchar3'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);} 
	   
	    $index = new xmldb_index('customchar4');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('customchar4'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);} 
	   
	    $index = new xmldb_index('customchar5');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('customchar5'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);} 

	   $index = new xmldb_index('customchar6');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('customchar6'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);} 
	   
	    $index = new xmldb_index('customchar7');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('customchar7'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);} 
	   
	    $index = new xmldb_index('customchar8');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('customchar8'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);} 
	   
	    $index = new xmldb_index('customchar9');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('customchar9'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);} 
	   
	     $index = new xmldb_index('customchar10');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('customchar10'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);} 
	   
    
	   //customintnumber
       $index = new xmldb_index('customintnumber1');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('customintnumber1'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);} 

       $index = new xmldb_index('customintnumber2');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('customintnumber2'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}   
	   
	   $index = new xmldb_index('customintnumber3');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('customintnumber3'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}
	   
	   $index = new xmldb_index('customintnumber4');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('customintnumber4'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}
	   
	   $index = new xmldb_index('customintnumber5');
       $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('customintnumber5'));
       if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}
    }
function  create_table_ams_enrol() {
        global $DB;
        $dbman = $DB->get_manager();     
        $table = new xmldb_table('local_badiunet_ams_enrol');
        if (!$dbman->table_exists($table)) {
            //id
            $table->add_field('id', XMLDB_TYPE_INTEGER, '10', null, XMLDB_NOTNULL, XMLDB_SEQUENCE, null);
            $table->add_key('primary', XMLDB_KEY_PRIMARY, array('id'));
            $dbman->create_table($table);
        }

		//uname
        $field = new xmldb_field('uname', XMLDB_TYPE_CHAR, '100', null, XMLDB_NOTNULL,null, null, 'id');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        //name
        $field = new xmldb_field('name', XMLDB_TYPE_CHAR, '100', null, XMLDB_NOTNULL,null, null, 'uname');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        //value
        $field = new xmldb_field('value', XMLDB_TYPE_TEXT, null, null, null, null, 'name');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
        
		//syncstatus
		$field = new xmldb_field('syncstatus',XMLDB_TYPE_INTEGER, '10',  null, XMLDB_NOTNULL, null, null, 'value');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
		//enrolid
		$field = new xmldb_field('enrolid',XMLDB_TYPE_INTEGER, '10',  null, null, null, null, 'syncstatus');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
		
		//enrolstatus
		 $field = new xmldb_field('enrolstatus', XMLDB_TYPE_CHAR, '100', null, null,null, null, 'enrolid');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
		//enrolgrade
		$field = new xmldb_field('enrolgrade',XMLDB_TYPE_NUMBER, '10,5',  null, null, null, null, 'enrolstatus');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
		//enroltimestart
		$field = new xmldb_field('enroltimestart',XMLDB_TYPE_INTEGER, '10',  null, null, null, '0', 'enrolgrade');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
		//enroltimeend
		$field = new xmldb_field('enroltimeend',XMLDB_TYPE_INTEGER, '10',  null, null, null, '0', 'enroltimestart');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
		
		//studentmdlid
		$field = new xmldb_field('studentmdlid',XMLDB_TYPE_INTEGER, '10',  null, null, null, null, 'enroltimeend');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
		
		//studentid
		$field = new xmldb_field('studentid',XMLDB_TYPE_INTEGER, '10',  null, null, null, null, 'studentmdlid');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
		
		//studentstatus
		 $field = new xmldb_field('studentstatus', XMLDB_TYPE_CHAR, '100', null, null,null, null, 'studentid');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
		
		//studentfirstname
		 $field = new xmldb_field('studentfirstname', XMLDB_TYPE_CHAR, '255', null, null,null, null, 'studentid');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
		
		//studentlastname
		 $field = new xmldb_field('studentlastname', XMLDB_TYPE_CHAR, '255', null, null,null, null, 'studentfirstname');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
		
		//studentemail
		 $field = new xmldb_field('studentemail', XMLDB_TYPE_CHAR, '255', null, null,null, null, 'studentlastname');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
		
		//studentphone
		 $field = new xmldb_field('studentphone', XMLDB_TYPE_CHAR, '255', null, null,null, null, 'studentemail');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
		
		//studentphonemobile
		 $field = new xmldb_field('studentphonemobile', XMLDB_TYPE_CHAR, '255', null, null,null, null, 'studentphone');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
		
		//teachermdlid
		$field = new xmldb_field('teachermdlid',XMLDB_TYPE_INTEGER, '10',  null, null, null, null, 'studentphonemobile');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
		
		//teacherid
		$field = new xmldb_field('teacherid',XMLDB_TYPE_INTEGER, '10',  null, null, null, null, 'teachermdlid');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
		//teacherstatus
		 $field = new xmldb_field('teacherstatus', XMLDB_TYPE_CHAR, '100', null, null,null, null, 'teacherid');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
		//teacherfirstname
		 $field = new xmldb_field('teacherfirstname', XMLDB_TYPE_CHAR, '255', null, null,null, null, 'teacherid');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
		
		//teacherlastname
		 $field = new xmldb_field('teacherlastname', XMLDB_TYPE_CHAR, '255', null, null,null, null, 'teacherfirstname');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
		
		//teacheremail
		 $field = new xmldb_field('teacheremail', XMLDB_TYPE_CHAR, '255', null, null,null, null, 'teacherlastname');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
		
		//teacherphone
		 $field = new xmldb_field('teacherphone', XMLDB_TYPE_CHAR, '255', null, null,null, null, 'teacheremail');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
		
		//teacherphonemobile
		 $field = new xmldb_field('teacherphonemobile', XMLDB_TYPE_CHAR, '255', null, null,null, null, 'teacherphone');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
		
		//managermdlid
		$field = new xmldb_field('managermdlid',XMLDB_TYPE_INTEGER, '10',  null, null, null, null, 'teacherphonemobile');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
		
		//managerid
		$field = new xmldb_field('managerid',XMLDB_TYPE_INTEGER, '10',  null, null, null, null, 'managermdlid');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
		
		//managerstatus
		 $field = new xmldb_field('managerstatus', XMLDB_TYPE_CHAR, '100', null, null,null, null, 'managerid');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
		//managerfirstname
		 $field = new xmldb_field('managerfirstname', XMLDB_TYPE_CHAR, '255', null, null,null, null, 'managerid');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
		
		//managerlastname
		 $field = new xmldb_field('managerlastname', XMLDB_TYPE_CHAR, '255', null, null,null, null, 'managerfirstname');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
		
		//manageremail
		 $field = new xmldb_field('manageremail', XMLDB_TYPE_CHAR, '255', null, null,null, null, 'managerlastname');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
		
		//managerphone
		 $field = new xmldb_field('managerphone', XMLDB_TYPE_CHAR, '255', null, null,null, null, 'manageremail');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
		
		//managerphonemobile
		 $field = new xmldb_field('managerphonemobile', XMLDB_TYPE_CHAR, '255', null, null,null, null, 'managerphone');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
		
		
		//projectmdlid
		$field = new xmldb_field('projectmdlid',XMLDB_TYPE_INTEGER, '10',  null, null, null, null, 'managerphonemobile');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
		
		//projectmdlcontenxt
		 $field = new xmldb_field('projectmdlcontenxt', XMLDB_TYPE_CHAR, '50', null, null,null, null, 'projectmdlid');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
		
		//projectid
		$field = new xmldb_field('projectid',XMLDB_TYPE_INTEGER, '10',  null, null, null, null, 'projectmdlcontenxt');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
		
		//projectstatus
		 $field = new xmldb_field('projectstatus', XMLDB_TYPE_CHAR, '100', null, null,null, null, 'projectid');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
		//projectcode
		 $field = new xmldb_field('projectcode', XMLDB_TYPE_CHAR, '100', null, null,null, null, 'projectid');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
		
		//projectname
		 $field = new xmldb_field('projectname', XMLDB_TYPE_CHAR, '255', null, null,null, null, 'projectcode');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
		//projecttype
		 $field = new xmldb_field('projecttype', XMLDB_TYPE_CHAR, '100', null, null,null, null, 'projectname');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
		
		//disciplinemdlid
		$field = new xmldb_field('disciplinemdlid',XMLDB_TYPE_INTEGER, '10',  null, null, null, null, 'projecttype');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
		
		//disciplinemdlcontenxt
		 $field = new xmldb_field('disciplinemdlcontenxt', XMLDB_TYPE_CHAR, '50', null, null,null, null, 'disciplinemdlid');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
		
		//disciplineid
		$field = new xmldb_field('disciplineid',XMLDB_TYPE_INTEGER, '10',  null, null, null, null, 'disciplinemdlcontenxt');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
		//disciplinestatus
		 $field = new xmldb_field('disciplinestatus', XMLDB_TYPE_CHAR, '100', null, null,null, null, 'disciplineid');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
		
		//disciplinecode
		 $field = new xmldb_field('disciplinecode', XMLDB_TYPE_CHAR, '100', null, null,null, null, 'disciplineid');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
		
		//disciplinename
		 $field = new xmldb_field('disciplinename', XMLDB_TYPE_CHAR, '255', null, null,null, null, 'disciplinecode');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
		//disciplinetype
		 $field = new xmldb_field('disciplinetype', XMLDB_TYPE_CHAR, '100', null, null,null, null, 'disciplinename');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
		
		//classmdlid
		$field = new xmldb_field('classmdlid',XMLDB_TYPE_INTEGER, '10',  null, null, null, null, 'disciplinetype');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
		
		//classmdlcontenxt
		 $field = new xmldb_field('classmdlcontenxt', XMLDB_TYPE_CHAR, '50', null, null,null, null, 'classmdlid');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
		
		//classid
		$field = new xmldb_field('classid',XMLDB_TYPE_INTEGER, '10',  null, null, null, null, 'classmdlcontenxt');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
		//classstatus
		 $field = new xmldb_field('classstatus', XMLDB_TYPE_CHAR, '100', null, null,null, null, 'classid');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
		
		//classcode
		 $field = new xmldb_field('classcode', XMLDB_TYPE_CHAR, '100', null, null,null, null, 'classid');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
		
		//classname
		 $field = new xmldb_field('classname', XMLDB_TYPE_CHAR, '255', null, null,null, null, 'classcode');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
		//classtype
		 $field = new xmldb_field('classtype', XMLDB_TYPE_CHAR, '100', null, null,null, null, 'classname');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
		
		 //value
        $field = new xmldb_field('description', XMLDB_TYPE_TEXT, null, null, null, null, 'classtype');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }
		
        //timecreated
        $field = new xmldb_field('timecreated',XMLDB_TYPE_INTEGER, '10',  null, null, null, '0', 'description');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        $field = new xmldb_field('timemodified', XMLDB_TYPE_INTEGER, '10',  null, null, null, '0', 'timecreated');
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

		$index = new xmldb_index('uname');
        $index->set_attributes(XMLDB_INDEX_UNIQUE, array('uname'));
        if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}
		
       //add index  
        $index = new xmldb_index('name');
        $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('name'));
        if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}
		
		$index = new xmldb_index('syncstatus');
        $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('syncstatus'));
        if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}
		
		$index = new xmldb_index('enrolid');
        $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('enrolid'));
        if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}
		
		$index = new xmldb_index('enrolstatus');
        $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('enrolstatus'));
        if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}
		
		$index = new xmldb_index('enrolgrade');
        $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('enrolgrade'));
        if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}
		
		$index = new xmldb_index('enroltimestart');
        $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('enroltimestart'));
        if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}
		
		$index = new xmldb_index('enroltimeend');
        $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('enroltimeend'));
        if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}
		
		$index = new xmldb_index('studentmdlid');
        $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('studentmdlid'));
        if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}
		
		$index = new xmldb_index('studentid');
        $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('studentid'));
        if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}
		
		$index = new xmldb_index('studentstatus');
        $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('studentstatus'));
        if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}
		
		$index = new xmldb_index('studentfirstname');
        $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('studentfirstname'));
        if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}
		
		$index = new xmldb_index('studentlastname');
        $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('studentlastname'));
        if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}
		
		$index = new xmldb_index('studentemail');
        $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('studentemail'));
        if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}
		
		$index = new xmldb_index('studentphone');
        $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('studentphone'));
        if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}
		
		$index = new xmldb_index('studentphonemobile');
        $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('studentphonemobile'));
        if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}
		
		$index = new xmldb_index('teachermdlid');
        $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('teachermdlid'));
        if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}
		
		$index = new xmldb_index('teacherid');
        $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('teacherid'));
        if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}
		
		$index = new xmldb_index('teacherstatus');
        $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('teacherstatus'));
        if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}
		
		$index = new xmldb_index('teacherfirstname');
        $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('teacherfirstname'));
        if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}
		
		$index = new xmldb_index('teacherlastname');
        $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('teacherlastname'));
        if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}
		
		$index = new xmldb_index('teacheremail');
        $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('teacheremail'));
        if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}
		
		$index = new xmldb_index('teacherphone');
        $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('teacherphone'));
        if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}
		
		$index = new xmldb_index('teacherphonemobile');
        $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('teacherphonemobile'));
        if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}
		
		$index = new xmldb_index('managermdlid');
        $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('managermdlid'));
        if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}
		
		$index = new xmldb_index('managerid');
        $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('managerid'));
        if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}
		
		$index = new xmldb_index('managerstatus');
        $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('managerstatus'));
        if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}	
		
		$index = new xmldb_index('managerfirstname');
        $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('managerfirstname'));
        if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}
		
		$index = new xmldb_index('managerlastname');
        $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('managerlastname'));
        if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}
		
		$index = new xmldb_index('manageremail');
        $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('manageremail'));
        if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}
		
		$index = new xmldb_index('managerphone');
        $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('managerphone'));
        if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}
		
		$index = new xmldb_index('managerphonemobile');
        $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('managerphonemobile'));
        if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}
		
		
		$index = new xmldb_index('projectmdlid');
        $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('projectmdlid'));
        if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}
		
		
		$index = new xmldb_index('projectmdlcontenxt');
        $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('projectmdlcontenxt'));
        if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}
		
		$index = new xmldb_index('projectid');
        $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('projectid'));
        if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}
		
		$index = new xmldb_index('projectstatus');
        $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('projectstatus'));
        if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}	
		
		$index = new xmldb_index('projectcode');
        $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('projectcode'));
        if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}
		
		$index = new xmldb_index('projectname');
        $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('projectname'));
        if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}
		
		$index = new xmldb_index('projecttype');
        $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('projecttype'));
        if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}
		
		$index = new xmldb_index('disciplinemdlid');
        $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('disciplinemdlid'));
        if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}
		
		
		$index = new xmldb_index('disciplinemdlcontenxt');
        $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('disciplinemdlcontenxt'));
        if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}
		
		$index = new xmldb_index('disciplineid');
        $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('disciplineid'));
        if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}
		
		$index = new xmldb_index('disciplinestatus');
        $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('disciplinestatus'));
        if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}	
		
		$index = new xmldb_index('disciplinecode');
        $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('disciplinecode'));
        if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}
		
		$index = new xmldb_index('disciplinename');
        $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('disciplinename'));
        if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}
		
		$index = new xmldb_index('disciplinetype');
        $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('disciplinetype'));
        if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}
		
		$index = new xmldb_index('classmdlid');
        $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('classmdlid'));
        if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}
		
		
		$index = new xmldb_index('classmdlcontenxt');
        $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('classmdlcontenxt'));
        if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}
		
		$index = new xmldb_index('classid');
        $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('classid'));
        if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}
		
		$index = new xmldb_index('classstatus');
        $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('classstatus'));
        if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}	
		
		$index = new xmldb_index('classcode');
        $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('classcode'));
        if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}
		
		$index = new xmldb_index('classname');
        $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('classname'));
        if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}
		
		$index = new xmldb_index('classtype');
        $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('classtype'));
        if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}
		
		$index = new xmldb_index('timecreated');
        $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('timecreated'));
         if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}

        $index = new xmldb_index('timemodified');
         $index->set_attributes(XMLDB_INDEX_NOTUNIQUE, array('timemodified'));
        if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}
		
    }
/*
* Happen error.  There are index that should be removed before. 
*/
 function  update_table_course() {
        global $DB;
         $dbman = $DB->get_manager();     
        $table = new xmldb_table('local_badiunet_course');
		if ($dbman->table_exists($table)) {
			
			$field = new xmldb_field('valuenumber',XMLDB_TYPE_NUMBER, '10,5',  null, null, null, null, 'valueint');
			if ($dbman->field_exists($table, $field)) {
				$dbman->change_field_type($table, $field);
			}
		}
      
   }
   
   function  update_table_mod_isumarize() {
        global $DB;
         $dbman = $DB->get_manager();     
        $table = new xmldb_table('local_badiunet_mod_isumarize');
		if ($dbman->table_exists($table)) {
			
			//uname
			$field = new xmldb_field('uname', XMLDB_TYPE_CHAR, '100', null, XMLDB_NOTNULL,null, null, 'id');
			if (!$dbman->field_exists($table, $field)) {
				$dbman->add_field($table, $field);
			}
			
			$index = new xmldb_index('uname');
			$index->set_attributes(XMLDB_INDEX_UNIQUE, array('uname'));
			if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}
		}
      
   }
   
    function  update_table_gnl_sumarize() {
        global $DB;
         $dbman = $DB->get_manager();     
        $table = new xmldb_table('local_badiunet_gnl_sumarize');
		if ($dbman->table_exists($table)) {
			
			//uname
			$field = new xmldb_field('uname', XMLDB_TYPE_CHAR, '100', null, XMLDB_NOTNULL,null, null, 'id');
			if (!$dbman->field_exists($table, $field)) {
				$dbman->add_field($table, $field);
			}
			
			$index = new xmldb_index('uname');
			$index->set_attributes(XMLDB_INDEX_UNIQUE, array('uname'));
			if(!$dbman->index_exists($table,$index)){$dbman->add_index($table,$index);}
		}
      
   }
   
    function  create_table_sserver() {
        global $DB;
		
        $dbman = $DB->get_manager();     
        $table = new xmldb_table('local_badiunet_sserver');
        if ($dbman->table_exists($table)) {
			
            //enviroment
			  $field = new xmldb_field('enviroment', XMLDB_TYPE_CHAR, '50', null, null,null, null, 'sstatus');
				if (!$dbman->field_exists($table, $field)) {
					$dbman->add_field($table, $field);
				}
				
        }
		
	}
}



?>