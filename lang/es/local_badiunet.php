<?PHP 
$string['pluginname']='Badiu.Net';
$string['badiunet:config'] = 'Configuración del plugin';
$string['badiunet:badiunet'] = 'Acceso a todos los informes';

$string['serviceurl'] ='Dirección del servicio de Plataforma Badiu.Net';
$string['serviceurl_desc'] ='Url de instalación de la plataforma Badiu.Net Ejemplo: http://www.servicio.com ';
$string['servicetoken'] ='Token de autenticación del servicio';
$string['servicetoken_desc'] ='Contraseña de autenticación de acceso a la Plataforma Badiu.Net';
$string['defaultmodulekey'] ='Clave del módulo predeterminado';
$string['defaultmodulekey_desc'] ='Define qué módulo debe cargarse al acceder al plugin. Ejemplo de clave: badiu.amdin.ticket.frontpage';

$string['autosyncuser'] ='Sincronizar usuario con la Plataforma Badiu.Net';
$string['autosyncuser_desc'] ='Gestionar la sincronización automática de los usuarios registrados en la Plataforma Moodle con la Plataforma Badiu.Net';

$string['keysyncuser'] ='Clave de control de sincronización del usuario con la Plataforma Badiu.Net';
$string['keysyncuser_desc'] ='Especificar la clave única de replicación de registro entre la Plataforma Moodle y la Plataforma Badiu.Net para evitar duplicación de registro';

$string['defaultmoduleurlparam'] ='Parámetros predeterminados de acceso al módulo';
$string['defaultmoduleurlparam_desc'] ='Pasar el parámetro que se pasará a la URL para acceder al módulo predeterminado del sistema. Ejemplo: parentid=__token_serviceid&_datasource=servicesql';

$string['syncpermission'] ='Sincronizar permiso con la Plataforma Badiu.Net';
$string['syncpermission_desc'] ='Esta configuración fuerza la sincronización de permisos en caso de que el usuario no tenga permiso de acceso';

$string['id']='Id';    
$string['yes']='Sí';
$string['no']='No';  
$string['general']='General';
$string['name']='Nombre';
$string['save']='Guardar';
$string['countrecord']='Total de registros: ';
$string['view']='Ver';
$string['edit']='Editar';
$string['remove']='Eliminar';
$string['deleteconfirm']       = '¿Realmente desea eliminar el registro "{$a}"?';
$string['addnew'] = 'Registrar nuevo';

$string['email']='Correo electrónico';
$string['username']='Nombre de usuario';
$string['idnumber']='Número de identificación';
$string['criptk1']='Clave de cifrado I';
$string['criptk1_desc']='';
$string['criptk2']='Clave de cifrado II';
$string['criptk2_desc']='';
$string['serverrmoteipallowed']='IP de servidores remotos con permiso para conectarse a Moodle';
$string['serverrmoteipallowed_desc']='';
$string['remoteipconnectionnotallowed']='IP del servidor remoto desconocido'; 


$string['enableremoteauth']='Permitir autenticación automática de usuarios registrados en el sistema externo Badiu.Net';
$string['enableremoteauth_desc']='Esta configuración permite que el usuario autenticado en la Plataforma Badiu.Net, con registro replicado en Moodle, acceda a la Plataforma Moodle sin iniciar sesión. En este caso, la autenticación se realiza en segundo plano a través de la comunicación entre la Plataforma Badiu.Net y Moodle. Al acceder al enlace de integración con Moodle en el entorno Badiu.Net, el usuario se autentica automáticamente en Moodle. Esta configuración se recomienda para integrar el inicio de sesión entre el sistema Badiu.Net y Moodle.';

$string['serviceenable'] = 'Sistema activo';
$string['serviceenable_desc'] = 'Habilita el funcionamiento del plugin en su totalidad';

$string['enablechat'] = 'Activar Chat AI';
$string['enablechat_desc'] = 'Activa el uso del chat integrado con el módulo de inteligencia artificial A2I de la plataforma Badiu.Net.';

$string['levelchatagent'] = 'Agent and prompt programming';
$string['levelchatagent_desc'] = 'This configuration determines the use of standard agent and prompt models already programmed or the application of customized models created to measure.';

$string['showchatinallpages'] = 'Mostrar chat en todo el sitio';
$string['showchatinallpages_desc'] = 'Mostrar el ícono del chat en todo el entorno de navegación de Moodle';

$string['showchatoutsidecourseforroles'] = 'Perfiles que deben ver el chat fuera del contexto del curso';
$string['showchatoutsidecourseforroles_desc'] = 'Lista de perfiles que deben ver el chat en todo el entorno de navegación, incluso fuera del contexto del curso. Complete el campo con el nombre corto de cada perfil, separándolos con comas. Ejemplo: student,editingteacher';

$string['enviroment'] = 'Modo de operación';
$string['enviroment_desc'] = 'Estos son los niveles de seguridad del sistema. Las opciones menos seguras solo deben utilizarse para pruebas o desarrollo.
<br>Modo Normal - Todas las verificaciones de seguridad activas
<br>Modo Flexible - Algunas verificaciones de seguridad reducidas para pruebas
<br>Modo Desarrollo - Verificaciones mínimas para trabajos técnicos
';

$string['messagechatdesabled']='El Chat de IA no está habilitado en el plugin BadiuNet.';
$string['messageserviceurlnotconfig']='La URL de la plataforma Badiu.Net no está configurada en el plugin BadiuNet.';
$string['messageservicetokennotconfig']='El token de acceso para la plataforma Badiu.Net no está configurado en el plugin BadiuNet.';
$string['messagemoodleinstancenotfind']='No se identificó la instancia de Moodle configurada en el plugin BadiuNet.';
$string['messagechatwithoutpermission']='No tienes permiso para acceder al Chat de Badiu A2I.';

$string['enviromentlevel1'] = 'Nivel I - Modo Normal';
$string['enviromentlevel2'] = 'Nivel II - Modo Flexible';
$string['enviromentlevel3'] = 'Nivel III - Modo Desarrollo';

$string['levelsharechatagent'] = 'Use standard agent and prompt models';
$string['levelcustomchatagent'] = 'Use customized agents and prompts';

$string['remoteaccess']='Acceso remoto';

$string['badiunet:viewsystemreport']='Acceder al informe general a través de la Plataforma Badiu.Net';
$string['badiunet:viewcoursereport']='Acceder al informe del curso a través de la Plataforma Badiu.Net';
$string['badiunet:viewownenrolreport']='Acceder a mis informes del curso a través de la Plataforma Badiu.Net';
$string['badiunet:viewownuserreport']='Acceder a mis informes a través de la Plataforma Badiu.Net';
$string['badiunet:usechat'] = 'Acceso al chat de inteligencia artificial A2I en la plataforma Badiu.Net';

$string['sserver']='Plataforma de servicio';
$string['sserveredit']='Registrar / editar plataforma de servicio';
$string['sserveradd']='Registrar plataforma de servicio';
$string['servicekeyinstance']='Clave de identificación de la instancia de servicio';
$string['servicekeyinstanceshort']='Clave de identificación';
$string['sservername']='Nombre de la plataforma de servicio';
$string['managesserver']='Gestionar plataforma de servicio';
$string['security']='Seguridad';
$string['otherconfig']='Otras configuraciones';
$string['syncuser']='Sincronización de usuario';
$string['dconfig']='Configuración general';
$string['dconfig_desc'] ='Utilice este campo para definir configuraciones adicionales. Utilice el formato json para registrar parámetros';
$string['description']='Descripción';
$string['addsuccess']='Registro realizado con éxito';
$string['editsuccess']='Actualización realizada con éxito';
$string['serviceurl']='Url';
$string['serviceurlnotvalid']='Url no válido';
$string['servicestatus']='Estado del servicio';
$string['servicestatusnotenabled']='No habilitado';
$string['servicestatusenable']='Habilitado';

$string['servicestatussetenable']='Habilitar';
$string['servicestatussetdesable']='Deshabilitar';

$string['requiredfield']='Campo obligatorio';

$string['defaultoken']='Token predeterminado';

$string['systemdtype']='Sistema';
$string['badiugc2']='Badiu GC2';
$string['defaultsystem']='Sistema predeterminado';

$string['proxy']='Proxy';
$string['proxyenable']='Habilitar proxy';
$string['proxyrole']='Reglas del proxy';

$string['serviceenableinfo']='<div class="card"> 
  <div class="card-header">Activar servicio de Badiu MReport</div>
  <div class="card-body">
    <p class="card-text">
	
	BADIU MReport - SISTEMA DE GESTIÓN DE INFORMES DE MOODLE<br /><br />
Copyright (C) 2023 Lino Vaz Moniz y equipo Badiu (https://app.badiu.com.br)<br />
Badiu MReport es una plataforma en línea para extraer informes de Moodle. Al habilitar el servicio, aceptas los términos de uso del modelo de cuenta gratuita. Al activar la cuenta, se establecerá comunicación con la plataforma de servicio en línea, tu Moodle se registrará y automáticamente estará disponible el servicio de informes.
<br /><br />Ver más información detallada acerca de los términos de uso del servicio Badiu MReport en:<br />
<a href="https://app.badiu.com.br/mreport/license" target="_blank">https://app.badiu.com.br/mreport/license</a>

<br /><br />
Lee los términos especificados en el enlace. Si estás de acuerdo, haz clic en el enlace Continuar para activar el servicio.
	</p>
    <a href="{$a->urlenableservice}" class="btn btn-primary">Continuar</a>
  </div>
</div>';


$string['cardaccessmreportlocaltitle']='Informes - Acceso en Moodle';
$string['cardaccessmreportlocaldescription']='Acceda a los informes de MReport directamente en su Moodle. El acceso se realiza a través de la interfaz de Moodle. Este consume el servicio de la plataforma online Badiu.Net.';

$string['cardaccessmreportremotetitle']='Informe - Acceso en la Plataforma Badiu.Net';
$string['cardaccessmreportremotedescription']='Acceda a los informes a través del sitio web de la plataforma online Badiu.Net. El acceso se realiza a través de la interfaz de MReport online, que consume el servicio de Moodle. Esta opción puede proporcionar una mejor experiencia de navegación y visualización de datos.';
$string['cardaccesschata2ititle']='Inteligencia Artificial - Badiu A2I';
$string['cardaccesschata2idescription']='Acceso al chat conectado con el modelo de lenguaje integrado a la base de datos de la Plataforma Moodle. Badiu A2I es una plataforma que integra inteligencia artificial con reglas de negocio específicas, combinando MReport y modelos de lenguaje de gran tamaño (LLM) a través de diversos prompts y Generación Aumentada por Recuperación (RAG).';

$string['panelmaneger']='Panel de Gestión';
$string['badiua2i']='Badiu A2I';
?>