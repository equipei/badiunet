<?PHP 
$string['pluginname']='Badiu.Net';
$string['badiunet:config'] = 'Plugin configuration';
$string['badiunet:badiunet'] = 'Access to all reports';

$string['serviceurl'] ='Badiu.Net Platform service address';
$string['serviceurl_desc'] ='Url of the Badiu.Net Platform installation Example: http://www.servico.com.br ';
$string['servicetoken'] ='Service authentication token';
$string['servicetoken_desc'] ='Authentication password for accessing the Badiu.Net Platform';
$string['defaultmodulekey'] ='Default module key';
$string['defaultmodulekey_desc'] ='Defines which module should be loaded when accessing the plugin. Example of a key: badiu.amdin.ticket.frontpage';

$string['autosyncuser'] ='Synchronize user with Badiu.Net Platform';
$string['autosyncuser_desc'] ='Manage automatic synchronization of users registered on the Moodle Platform with the Badiu.Net Platform';

$string['keysyncuser'] ='User synchronization control key with Badiu.Net Platform';
$string['keysyncuser_desc'] ='Specify the unique replication key for registration between Moodle Platform and Badiu.Net Platform to avoid duplicate registration';

$string['defaultmoduleurlparam'] ='Default module access parameters';
$string['defaultmoduleurlparam_desc'] ='Pass the parameter that will be passed to the URL to access the default system module. Example: parentid=__token_serviceid&_datasource=servicesql';

$string['syncpermission'] ='Synchronize permission with Badiu.Net Platform';
$string['syncpermission_desc'] ='This configuration forces permission synchronization if the user does not have access permission';

$string['id']='Id';    
$string['yes']='Yes';
$string['no']='No';  
$string['general']='General';
$string['name']='Name';
$string['save']='Save';
$string['countrecord']='Total records: ';
$string['view']='View';
$string['edit']='Edit';
$string['remove']='Delete';
$string['deleteconfirm']       = 'Do you really want to delete the "{$a}" record?';
$string['addnew'] = 'Register new';

$string['email']='E-mail';
$string['username']='Login';
$string['idnumber']='Identification number';
$string['criptk1']='Encryption key I';
$string['criptk1_desc']='';
$string['criptk2']='Encryption key II';
$string['criptk2_desc']='';
$string['serverrmoteipallowed']='IP of remote servers with permission to connect to Moodle';
$string['serverrmoteipallowed_desc']='';
$string['remoteipconnectionnotallowed']='IP of the unknown remote server'; 


$string['enableremoteauth']='Allow automatic authentication of users logged in to the external Badiu.Net system';
$string['enableremoteauth_desc']='This configuration allows users authenticated on the Badiu.Net Platform, with registration replicated in Moodle, to access the Moodle Platform without logging in. In this case, authentication occurs in the background via communication between the Badiu.Net Platform and Moodle. When accessing the Moodle integration link in the Badiu.Net environment, the user is automatically authenticated in Moodle. This configuration is recommended for integrating login between the Badiu.Net system and Moodle.';

$string['serviceenable'] = 'System active';
$string['serviceenable_desc'] = 'Enables the functioning of the plugin as a whole';

$string['enablechat'] = 'Activate AI Chat';
$string['enablechat_desc'] = 'Activates the use of the chat integrated with the Badiu.Net platform\'s AI module A2I.';

$string['levelchatagent'] = 'Programación de agentes y prompts';
$string['levelchatagent_desc'] = 'Esta configuración determina el uso de modelos estándar de agentes y prompts ya programados o la aplicación de modelos personalizados creados a medida.';

$string['showchatinallpages'] = 'Show chat on the entire site';
$string['showchatinallpages_desc'] = 'Display the chat icon throughout the Moodle navigation environment';

$string['showchatoutsidecourseforroles'] = 'Roles that should see the chat outside the course context';
$string['showchatoutsidecourseforroles_desc'] = 'List of roles that should see the chat throughout the navigation environment, even outside the course context. Fill in the field with the short name of each role, separating them with commas. Example: student,editingteacher';

$string['enviroment'] = 'Operating mode';
$string['enviroment_desc'] = 'These are the system\'s security levels. Less secure options should only be used for testing or development.
<br>Normal Mode - All security checks active
<br>Flexible Mode - Some security checks reduced for testing
<br>Development Mode - Minimal checks for technical work
';
$string['enviromentlevel1'] = 'Level I - Normal Mode';
$string['enviromentlevel2'] = 'Level II - Flexible Mode';
$string['enviromentlevel3'] = 'Level III - Development Mode';

$string['levelsharechatagent'] = 'Usar modelos estándar de agentes y prompts';
$string['levelcustomchatagent'] = 'Usar agentes y prompts personalizados';

$string['messagechatdesabled']='The AI Chat is not enabled in the BadiuNet plugin.';
$string['messageserviceurlnotconfig']='The Badiu.Net platform URL is not configured in the BadiuNet plugin.';
$string['messageservicetokennotconfig']='The access token for the Badiu.Net platform is not configured in the BadiuNet plugin.';
$string['messagemoodleinstancenotfind']='The Moodle instance configured in the BadiuNet plugin was not identified.';
$string['messagechatwithoutpermission']='You do not have permission to access the Badiu A2I Chat.';

$string['remoteaccess']='Remote access';

$string['badiunet:viewsystemreport']='Access general report through Badiu.Net Platform';
$string['badiunet:viewcoursereport']='Access course report through Badiu.Net Platform';
$string['badiunet:viewownenrolreport']='Access my course reports through Badiu.Net Platform';
$string['badiunet:viewownuserreport']='Access my reports through Badiu.Net Platform';
$string['badiunet:usechat'] = 'Access to the A2I artificial intelligence chat on the Badiu.Net platform';

$string['sserver']='Service platform';
$string['sserveredit']='Register / edit service platform';
$string['sserveradd']='Register service platform';
$string['servicekeyinstance']='Service instance identification key';
$string['servicekeyinstanceshort']='Identification key';
$string['sservername']='Service platform name';
$string['managesserver']='Manage service platform';
$string['security']='Security';
$string['otherconfig']='Other configurations';
$string['syncuser']='User synchronization';
$string['dconfig']='General configuration';
$string['dconfig_desc'] ='Use this field to define additional settings. Use JSON format to register parameters';
$string['description']='Description';
$string['addsuccess']='Registration completed successfully';
$string['editsuccess']='Update completed successfully';
$string['serviceurl']='Url';
$string['serviceurlnotvalid']='Invalid url';
$string['servicestatus']='Service status';
$string['servicestatusnotenabled']='Not enabled';
$string['servicestatusenable']='Enabled';

$string['servicestatussetenable']='Enable';
$string['servicestatussetdesable']='Disable';

$string['requiredfield']='Required field';

$string['defaultoken']='Default token';

$string['systemdtype']='System';
$string['badiugc2']='Badiu GC2';
$string['defaultsystem']='Default system';

$string['proxy']='Proxy';
$string['proxyenable']='Enable proxy';
$string['proxyrole']='Proxy rules';

$string['serviceenableinfo']='<div class="card"> 
  <div class="card-header">Activate Badiu MReport service</div>
  <div class="card-body">
    <p class="card-text">
	
	BADIU MReport - MOODLE REPORT MANAGEMENT SYSTEM<br /><br />
Copyright (C) 2023 Lino Vaz Moniz and Badiu team (https://app.badiu.com.br)<br />
Badiu MReport is an online platform for extracting reports from Moodle. By enabling the service, you agree to the terms of use for the free account model. When activating the account, communication with the online service platform will be established, your Moodle will be registered, and the report service will automatically become available.
<br /><br />See more detailed information about Badiu MReport service terms of use at:<br />
<a href="https://app.badiu.com.br/mreport/license" target="_blank">https://app.badiu.com.br/mreport/license</a>

<br /><br />
Read the terms specified in the link. If you agree, click the Continue link to activate the service.

	</p>
    <a href="{$a->urlenableservice}" class="btn btn-primary">Continue</a>
  </div>
</div>';


$string['cardaccessmreportlocaltitle']='Reports - Access in Moodle';
$string['cardaccessmreportlocaldescription']='Access the MReport reports directly in your Moodle. Access is made through the Moodle interface. This consumes the service of the online platform Badiu.Net.';

$string['cardaccessmreportremotetitle']='Report - Access on the Badiu.Net Platform';
$string['cardaccessmreportremotedescription']='Access the reports through the website of the online platform Badiu.Net. Access is made through the online MReport interface, which consumes Moodle\'s service. This option can provide a better browsing and data visualization experience.';
$string['cardaccesschata2ititle']='Artificial Intelligence - Badiu A2I';
$string['cardaccesschata2idescription']='Access to the chat connected to the language model integrated with the Moodle Platform database. Badiu A2I is a platform that integrates artificial intelligence with specific business rules, combining MReport and large language models (LLMs) through various prompts and Retrieval-Augmented Generation (RAG).';

$string['panelmaneger']='Management Dashboard';
$string['badiua2i']='Badiu A2I';
?> 