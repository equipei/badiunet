<?PHP 
$string['pluginname']='Badiu.Net';
$string['badiunet:config'] = 'Configuração do plugin';
$string['badiunet:badiunet'] = 'Acesso a todos os relatórios';

$string['serviceurl'] ='Endereço do serviço do Plataforma Badiu.Net';
$string['serviceurl_desc'] ='Url da instalação da plataforma Badiu.Net Exemplo: http://www.servico.com.br ';
$string['servicetoken'] ='Token de autenticação do serviço';
$string['servicetoken_desc'] ='Senha de autenticação de acesso à  Plataforma Badiu.Net';
$string['defaultmodulekey'] ='Chave do módulo padrão';
$string['defaultmodulekey_desc'] ='Define qual módulo deve ser carregado ao acessar o plugin. Exemplo de chave: badiu.amdin.ticket.frontpage';



$string['autosyncuser'] ='Sincronizar usuário com a Plataforma Badiu.Net';
$string['autosyncuser_desc'] ='Gerenciar sincronização automática dos usuários cadastrados na Plataforma Moodle com a Plataforma Badiu.Net';

$string['keysyncuser'] ='Chave de controle de sincronização do usuário com a Plataforma Badiu.Net';
$string['keysyncuser_desc'] ='Especificar a chave única de replicação de cadastro entre Plataforma Moodle e a Plataforma Badiu.Net para evitar duplicação de cadastro';

$string['defaultmoduleurlparam'] ='Parâmetros padrão de acesso ao módulo';
$string['defaultmoduleurlparam_desc'] ='Passe o parâmetro que será passado ao URL para acessar moódulo padrão do sistema. Exemplo: parentid=__token_serviceid&_datasource=servicesql';

$string['syncpermission'] ='Sincronizar permissão com a Plataforma Badiu.Net';
$string['syncpermission_desc'] ='Essa configuração força a sincronização de permissão caso o usuário não tiver permissão de acesso';

$string['id']='Id';    
$string['yes']='Sim';
$string['no']='Não';  
$string['general']='Geral';
$string['name']='Name';
$string['save']='Salvar';
$string['countrecord']='Total de registro: ';
$string['view']='Visualizar';
$string['edit']='Editar';
$string['remove']='Excluir';
$string['deleteconfirm']       = 'Você realmente deseja excluir o registro "{$a}"?';
$string['addnew'] = 'Cadastrar novo';

$string['email']='E-mail';
$string['username']='Login';
$string['idnumber']='Número de identificação';
$string['criptk1']='Chave de criptografia I';
$string['criptk1_desc']='';
$string['criptk2']='Chave de criptografia II';
$string['criptk2_desc']='';
$string['serverrmoteipallowed']='Ip de servidores remote com permissão para conectar ao Moodle';
$string['serverrmoteipallowed_desc']='';
$string['remoteipconnectionnotallowed']='Ip do servidor do servidor remoto desconhecido'; 

$string['enableremoteauth']='Permitir autenticação automática de usuários logado em sistema externo Badiu.Net';
$string['enableremoteauth_desc']='Essa configuração  permite que usuário autenticado na Plataforma Badiu.Net, com cadastro replicado no Moodle, acessam  a Plataforma Moodle sem logar. Neste caso, a autenticação ocorre em segundo plano via comunicação entre Plataforma Badiu.Net e Moodle. Ao acessar link de integração com Moodle no ambiente Badiu.Net, o usuário é autenticado automaticamente no Moodle. Essa configuração é recomendado para integrar login entre sistema Badiu.Net  e Moodle.';

$string['serviceenable'] ='Sistema ativo';
$string['serviceenable_desc'] ='Habilita o funcionamento do plugin como um todo';

$string['enablechat'] ='Ativar AI Chat';
$string['enablechat_desc'] ='Ativa o uso do chat integrado ao módulo  de inteligência artificial A2I da plataforma Badiu.Net.';

$string['levelchatagent'] ='Programação de agentes e prompts';
$string['levelchatagent_desc'] ='Esta configuração determina o uso de modelos padrão de agentes e prompts já programados ou a aplicação de modelos personalizados criados sob medida.';

$string['showchatinallpages'] ='Exibir chat em todo o site';
$string['showchatinallpages_desc'] ='Exibir o ícone do chat em todo o ambiente de navegação no Moodle';

$string['showchatoutsidecourseforroles'] ='Perfis que devem visualizar o chat fora do contexto do curso';
$string['showchatoutsidecourseforroles_desc'] ='Lista de perfis que devem visualizar o chat em todo o ambiente de navegação, mesmo fora do contexto do curso. Preencha o campo com o nome breve (shortname) de cada perfil, separando-os com vírgulas. Exemplo: student,editingteacher';


$string['enviroment'] ='Modo de operação';
$string['enviroment_desc'] ='Trata-se de níveis de segurança do sistema. Opções menos seguras devem ser usadas apenas para testes ou desenvolvimento. 
<br>Modo Normal - Todas as verificações de segurança ativas
<br>Modo Flexível - Algumas verificações de segurança reduzidas para testes
<br>Modo Desenvolvimento - Verificações mínimas para trabalhos técnicos
';
$string['enviromentlevel1'] ='Nível I - Modo normal';
$string['enviromentlevel2'] ='Nível II - Modo flexível';
$string['enviromentlevel3'] ='Nível III - Modo desenvolvimento';

$string['levelsharechatagent'] ='Usar modelo padrão de agentes e prompts';
$string['levelcustomchatagent'] ='Usar agentes e prompts customizados';

$string['messagechatdesabled']='O Chat AI não está habilitado no plugin BadiuNet.';
$string['messageserviceurlnotconfig']='A URL da plataforma Badiu.Net não está configurada no plugin BadiuNet.';
$string['messageservicetokennotconfig']='O token de acesso à plataforma Badiu.Net não está configurado no plugin BadiuNet.';
$string['messagemoodleinstancenotfind']='A instância do Moodle configurada no plugin BadiuNet não foi identificada.';
$string['messagechatwithoutpermission']='Você não tem permissão para acessar o Chat da Badiu A2I.';

$string['remoteaccess']='Acesso remoto';

$string['badiunet:viewsystemreport']='Acessar relatório geral pela Plataforma Badiu.Net';
$string['badiunet:viewcoursereport']='Acessar relatório do curso pela Plataforma Badiu.Net';
$string['badiunet:viewownenrolreport']='Acessar meus relatórios do curso pela Plataforma Badiu.Net';
$string['badiunet:viewownuserreport']='Acessar meus relatórios pela Plataforma Badiu.Net';
$string['badiunet:usechat']='Acesso ao chat de inteligência artificial A2I da plataforma Badiu.Net';


$string['sserver']='Plataforma de serviço';
$string['sserveredit']='Cadastrar / editar plataforma de serviço';
$string['sserveradd']='Cadastrar plataforma de serviço';
$string['servicekeyinstance']='Chave de indentificação da instância de serviço';
$string['servicekeyinstanceshort']='Chave de indentificação';
$string['sservername']='Nome da plataforma de serviço';
$string['managesserver']='Gerenciar plataforma de serviço';
$string['security']='Segurança';
$string['otherconfig']='Outras configurações';
$string['syncuser']='Sincronização de usuário';
$string['dconfig']='Configuração geral';
$string['dconfig_desc'] ='Use esse campo para definir configurações adicionais. Use formato json para cadastrar parâmetros';
$string['description']='Descrição';
$string['addsuccess']='Cadastro efetuado com sucesso';
$string['editsuccess']='Atualização efetuado com sucesso';
$string['serviceurl']='Url';
$string['serviceurlnotvalid']='Url inválido';
$string['servicestatus']='Status do serviço';
$string['servicestatusnotenabled']='Não habilitado';
$string['servicestatusenable']='Habilitado';

$string['servicestatussetenable']='Habilitar';
$string['servicestatussetdesable']='Dasabilitar';

$string['requiredfield']='Campo obrigatório';

$string['defaultoken']='Token padrão';

$string['systemdtype']='Sistema';
$string['badiugc2']='Badiu GC2';
$string['defaultsystem']='Sistema padrão';

$string['proxy']='Proxy';
$string['proxyenable']='Habilitar proxy';
$string['proxyrole']='Regras do proxy';

$string['serviceenableinfo']='<div class="card"> 
  <div class="card-header">Ativar serviço do Badiu MReport</div>
  <div class="card-body">
    <p class="card-text"> 
	
	BADIU MReport - SISTEMA DE GESTÃO DE RELATÓRIOS DO MOODLE<br /><br />
Copyright (C) 2023  Lino Vaz Moniz e equipe Badiu (https://app.badiu.com.br)<br />
Badiu MReport é uma plataforma online de extração de relatórios do Moodle. Ao habilitar o serviço, você aceita os termos sobre o uso do serviço no modelo conta gratuita. Ao ativar a conta, será feita uma comunicação com a plataforma de serviço online, seu Moodle será registrado e automaticamente o serviço de relatório ficará disponível.
<br /><br />Veja informações mais  detalhadas sobre os termos de uso do serviço do Badiu MReport em:<br />
<a href="https://app.badiu.com.br/mreport/license" target="_blank">https://app.badiu.com.br/mreport/license</a>

<br /><br />
Leia os termos especificados no link. Caso aceite, clique no link Continuar, para ativar o serivço.

	</p>
    <a href="{$a->urlenableservice}" class="btn btn-primary">Continuar</a>
  </div>
</div>';


$string['cardaccessmreportlocaltitle']='Relatórios - Acesso no Moodle';
$string['cardaccessmreportlocaldescription']='Acesse os relatórios do MReport diretamente no seu Moodle. O acesso é realizado pela interface do Moodle. Este consome o serviço da plataforma online Badiu.Net.';

$string['cardaccessmreportremotetitle']='Relatório - Acesso na Plataforma Badiu.Net';
$string['cardaccessmreportremotedescription']='Acesse os relatórios pelo site da plataforma online Badiu.Net. O acesso é realizado pela interface do MReport online, que consome serviço do Moodle. Essa opção pode proporcionar uma melhor experiência de navegação e visualização de dados.';

$string['cardaccesschata2ititle']='Inteligência Artificial - Badiu A2I';
$string['cardaccesschata2idescription']='Acesso ao chat conectado com modelo de linguagem integrado à base de dados da Plataforma Moodle. O Badiu A2I é uma plataforma que integra inteligência artificial com regras de negócios específicas, combinando o MReport e modelos de linguagem de grande porte (LLM) por meio de diversos prompts e Geração Aumentada por Recuperação (RAG).';
$string['panelmaneger']='Painel Gerencial';
$string['badiua2i']='Badiu A2I';
?>