<?php  
      
defined('MOODLE_INTERNAL') || die;

defined('MOODLE_INTERNAL') || die;
require_once("$CFG->dirroot/local/badiunet/lib/formutil.php"); 
$util= new local_badiunet_formutil();
$yesnotoptions = $util->option_yes_not();
if ($hassiteconfig) {
   
    $settings = new admin_settingpage('local_badiunet', get_string('pluginname', 'local_badiunet'));
    $ADMIN->add('localplugins', $settings);
   
//field serviceurl
    $settings->add(new admin_setting_configtext('local_badiunet/serviceurl', 
		get_string('serviceurl', 'local_badiunet'), get_string('serviceurl_desc', 'local_badiunet'), ''));
   
	//field servicetoken
    $settings->add(new admin_setting_configtext('local_badiunet/servicetoken', 
		get_string('servicetoken', 'local_badiunet'), get_string('servicetoken_desc', 'local_badiunet'), ''));
		
//field serviceenable
   $settings->add(new admin_setting_configselect('local_badiunet/serviceenable',
           get_string('serviceenable', 'local_badiunet'), get_string('serviceenable_desc', 'local_badiunet'), 1, $yesnotoptions));
 

 //field enablechat
   $settings->add(new admin_setting_configselect('local_badiunet/enablechat',
           get_string('enablechat', 'local_badiunet'), get_string('enablechat_desc', 'local_badiunet'), 1, $yesnotoptions));


   $settings->add(new admin_setting_configselect('local_badiunet/levelchatagent',
           get_string('levelchatagent', 'local_badiunet'), get_string('levelchatagent_desc', 'local_badiunet'), 200, $util->option_levelchatagent()));

 $settings->add(new admin_setting_configselect('local_badiunet/showchatinallpages',
           get_string('showchatinallpages', 'local_badiunet'), get_string('showchatinallpages_desc', 'local_badiunet'), 0, $yesnotoptions));

 $settings->add(new admin_setting_configtext('local_badiunet/showchatoutsidecourseforroles', 
		get_string('showchatoutsidecourseforroles', 'local_badiunet'), get_string('showchatoutsidecourseforroles_desc', 'local_badiunet'), 'student,editingteacher,manager'));
	
	
//field enviroment
   $settings->add(new admin_setting_configselect('local_badiunet/enviroment',
           get_string('enviroment', 'local_badiunet'), get_string('enviroment_desc', 'local_badiunet'), 'level1', $util->option_enviromentlevel()));


 
	//field defaultmodule
    $settings->add(new admin_setting_configtext('local_badiunet/defaultmodulekey', 
		get_string('defaultmodulekey', 'local_badiunet'), get_string('defaultmodulekey_desc', 'local_badiunet'), ''));

    //field defaultmoduleurlparam
    $settings->add(new admin_setting_configtextarea('local_badiunet/defaultmoduleurlparam',get_string('defaultmoduleurlparam', 'local_badiunet'), get_string('defaultmoduleurlparam_desc', 'local_badiunet'),''
        , PARAM_RAW, 60, 3));
	      
     $settings->add(new admin_setting_configtextarea('local_badiunet/criptk1',get_string('criptk1', 'local_badiunet'), get_string('criptk1_desc', 'local_badiunet'),''
        , PARAM_RAW, 60, 3));
     
      $settings->add(new admin_setting_configtextarea('local_badiunet/criptk2',get_string('criptk2', 'local_badiunet'), get_string('criptk2_desc', 'local_badiunet'),''
        , PARAM_RAW, 60, 3));
      
        $settings->add(new admin_setting_configtextarea('local_badiunet/serverrmoteipallowed',get_string('serverrmoteipallowed', 'local_badiunet'), get_string('serverrmoteipallowed_desc', 'local_badiunet'),''
        , PARAM_RAW, 20, 3));
		
			
	$settings->add(new admin_setting_configselect('local_badiunet/enableremoteauth',
            get_string('enableremoteauth', 'local_badiunet'), get_string('enableremoteauth_desc', 'local_badiunet'), 0, $yesnotoptions));
	
			
    $yesnotoptions = $util->option_yes_not();
	$settings->add(new admin_setting_configselect('local_badiunet/autosyncuser',
            get_string('autosyncuser', 'local_badiunet'), get_string('autosyncuser_desc', 'local_badiunet'), 0, $yesnotoptions));
       
     
    $keysyncuseroptions = $util->option_keysync_user();
	$settings->add(new admin_setting_configselect('local_badiunet/keysyncuser',
            get_string('keysyncuser', 'local_badiunet'), get_string('keysyncuser_desc', 'local_badiunet'), 'username', $keysyncuseroptions));
   
  
		
	$settings->add(new admin_setting_configtextarea('local_badiunet/dconfig',get_string('dconfig', 'local_badiunet'), get_string('dconfig_desc', 'local_badiunet'),''
        , PARAM_RAW, 60, 3));
}
