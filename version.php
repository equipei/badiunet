<?PHP 
defined('MOODLE_INTERNAL') || die;

$plugin->version   = 2025010100;         // The current plugin version (Date: YYYYMMDDXX)
$plugin->requires  = 2012112900;         // Requires this Moodle version
$plugin->component = 'local_badiunet'; // Full name of the plugin (used for diagnostics)
$plugin->release = '1.4.4';

?>
