<?php

require_once("../../../config.php");
require_once("$CFG->dirroot/local/badiunet/lib/pluginconfig.php");
require_once("$CFG->dirroot/local/badiunet/lib/util.php");
require_once("$CFG->dirroot/local/badiunet/chat/lib.php");
   require_login();
$context = context_system::instance();
if (!has_capability('local/badiunet:usechat', $context)) {echo get_string('messagechatwithoutpermission', "local_badiunet");exit;}

$PAGE->set_context($context);
$PAGE->set_url('/local/badiunet/fcservice/chat/index.php');
$PAGE->set_pagelayout('standard');
$PAGE->set_title(get_string('badiua2i', "local_badiunet"));
$PAGE->set_heading(get_string('badiua2i', "local_badiunet"));




 
//get param config
	$plugin=new local_badiunet_pluginconfig('local_badiunet');
	$util=new local_badiunet_util();
	$pconfig=$plugin->getAll(false);		
	$enablechat=$util->getVlueOfArray($pconfig,'enablechat');
	if(!$enablechat){badiunet_chat_showerror(get_string('messagechatdesabled', "local_badiunet"));}
	 
	$serviceurl=$util->getVlueOfArray($pconfig,'serviceurl');
	if(empty($serviceurl)){badiunet_chat_showerror(get_string('messageserviceurlnotconfig', "local_badiunet"));}
	$levelshare=$util->getVlueOfArray($pconfig,'levelchatagent');
	$moduleinstance=null;
	$servicetoken=$util->getVlueOfArray($pconfig,'servicetoken');
	if(empty($servicetoken)){badiunet_chat_showerror(get_string('messageservicetokennotconfig', "local_badiunet"));}
	$p=explode("|",$servicetoken);
	if(isset($p[4])){$moduleinstance=$p[4];}
    if(empty($moduleinstance)){badiunet_chat_showerror(get_string('messagemoodleinstancenotfind', "local_badiunet"));}
	$localendoipoint=$CFG->wwwroot.'/local/badiunet/chat/sync.php';
	$appchat=$serviceurl.'/ai/assistant/app/chat/default.js';
	$appviewchat=$serviceurl.'/ai/assistant/app/view/chat/default';
	
	$chatlib=new local_badiunet_chat_lib();
	$badiucontextdata=$chatlib->makeContextData();
	if(empty($badiucontextdata)){$badiucontextdata='""';}
	else if(is_array($badiucontextdata)){
		$badiucontextdata=json_encode($badiucontextdata);
	}else{$badiucontextdata='""';}
	
	$ch = curl_init($appviewchat);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$contentappviewchat = curl_exec($ch);
	curl_close($ch);
	
	$ch = curl_init($appchat);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$contentappchat = curl_exec($ch);
	curl_close($ch);
	
	//init var
	$jsinitparamenter='this.assistantchat.client = {"modulekey":"badiu.moodle.mreport","moduleinstance":"'.$moduleinstance.'","localendoipoint": "'.$localendoipoint.'","levelshare": "'.$levelshare.'","context": '. $badiucontextdata.'};';
	
	$contentappchat=str_replace("_BADIU_AI_ASSISTANT_INIT_PARAMETER",$jsinitparamenter,$contentappchat);
	echo $OUTPUT->header();
	echo 	$contentappviewchat;
	echo $contentappchat; 
	
	echo $OUTPUT->footer();

function badiunet_chat_showerror($msg){
	global $OUTPUT;
	echo $OUTPUT->header();
	$html="<div class=\"alert alert-danger\" role=\"alert\">
		$msg
		</div>";
	echo $html;
	echo $OUTPUT->footer();
	exit;
	
}
?>
