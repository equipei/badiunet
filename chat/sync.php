<?php
/**
 * Library of interface functions and constants for module badiunet
 *
 * @package     local_badiunet
 * @copyright   2024 Lino Vaz Moniz <linovazmoniz@gmail.com>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

require_once("../../../config.php");
require_once("$CFG->dirroot/local/badiunet/chat/service.php");
class local_badiunet_chat_sync {
	function __construct() {
		new local_badiunet_chat_service();
	}
	
}
new local_badiunet_chat_sync();