<?php
/**
 * Library of interface functions and constants for module badiunet
 *
 * @package     local_badiunet
 * @copyright   2024 Lino Vaz Moniz <linovazmoniz@gmail.com>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();
require_once("$CFG->dirroot/local/badiunet/lib/util.php");
require_once("$CFG->dirroot/local/badiunet/fcservice/maccess.php");
require_once("$CFG->dirroot/local/badiunet/lib/templetecript.php");
require_once("$CFG->dirroot/local/badiunet/lib/role.php");

class local_badiunet_chat_lib {
	
	function badiunet_login() {
		$maccess=new local_badiunet_maccess();
		$logued=$maccess->isLogged();
		if(!$logued){$maccess->login();}
	}
	function badiunet_relogin() {
		$maccess=new local_badiunet_maccess();
		
		$sdata=$maccess->get();
		$util=new local_badiunet_util();
		$timeexpire=$util->getVlueOfArray($sdata, 'timeexpire');
		$tolerance=15*60;
		if(($timeexpire+$tolerance)>=time()){$maccess->login();}
	}
	
	function get_sessionid() {
		
		$this->badiunet_login();
		$this->badiunet_relogin();
		$maccess=new local_badiunet_maccess();
		$sdata=$maccess->get();
		
		$util=new local_badiunet_util();
		$tkey=$util->getVlueOfArray($sdata, 'tkey');
		$tcript=new local_badiunet_templatecript();
		$tkey=$tcript->encode('s1',$tkey);
		return $tkey;
	}
	  
  public function isloggedin(){
      global $USER;

     if(!isset($USER)){ return false; }
     if($USER->id==0 ||  $USER->id==1){return false;}
     if($USER->id >= 2 ){return true;}
     return false;
  }


/**
 * Checks if a user has the required roles to access a specific resource or functionality.
 *
 * This function verifies whether the given user (or the current logged-in user if no user ID is provided)
 * has one or more roles that match the required roles. It supports roles provided as a string or an array.
 *
 * @global object $USER The global Moodle user object representing the currently logged-in user.
 * @param string|array $roles A single role or an array of roles required for access.
 * @param int|null $userid The ID of the user to check roles for. Defaults to the currently logged-in user.
 * @return bool True if the user has at least one of the required roles, false otherwise.
 */
public function hasRoleToAccess($roles, $userid = null) {
    global $USER;

    // If no roles are provided, access is denied.
    if (empty($roles)) {
        return false;
    }

    // Use the currently logged-in user if no user ID is provided.
    if (empty($userid)) {
        $userid = $USER->id;
    }

    // Instantiate the role handler for the user.
    $rolelib = new local_badiunet_role($userid);
    $useroles = $rolelib->get();

    // If the user has no roles, access is denied.
    if (empty($useroles)) {
        return false;
    }

    // Utility to handle string-to-array conversion.
    $util = new local_badiunet_util();
    $roles = $util->castStringToArray($roles);

    // Ensure roles are in array format.
    if (!is_array($roles)) {
        return false;
    }

    // Special handling for admin role.
    if ($useroles === 'admin') {
        if (in_array('admin', $roles, true)) {
            return true;
        }
    }

    // Convert user roles to an array if they are not already.
    $useroles = $util->castStringToArray($useroles);
    if (!is_array($useroles)) {
        return false;
    }

    // Check if the user has any of the required roles.
    foreach ($useroles as $r) {
        if (in_array($r, $roles, true)) {
            return true;
        }
    }

    // If no roles match, access is denied.
    return false;
}
 
 /**
 * Generates contextual data about the current page in Moodle.
 *
 * This method analyzes the current context to determine whether it is
 * a system, course, or activity context. It then returns an array containing
 * relevant information such as URL, URI, type, name, and instance ID.
 *
 * @global object $CFG    Moodle configuration object.
 * @global object $COURSE Current course object.
 * @global object $PAGE   Page object containing current context.
 *
 * @return array Associative array containing:
 *               - 'url' (string): Base URL of the Moodle site.
 *               - 'uri' (string): Cleaned URI using local utility.
 *               - 'type' (string|null): Context type ('system', 'course', or 'activity').
 *               - 'name' (string|null): Name of the course or activity (if applicable).
 *               - 'instance' (int|null): ID of the course, activity, or system.
 *
 * Example of returned array:
 * [
 *     'url' => 'https://moodlesite.com',
 *     'uri' => '/course/view.php?id=2',
 *     'type' => 'course',
 *     'name' => 'Course Full Name',
 *     'instance' => 2
 * ]
 */
 public function makeContextData(){
	 
	 global $CFG,$COURSE,$PAGE;
	 $util=new local_badiunet_util();
	 $uri= $util->clean_uri();
	 $url= $CFG->wwwroot;
	 $type=null;
	 $name=null;
	 $instance=null;
	
  
    // Check if it's the system context
    if (empty($COURSE->id) || $COURSE->id == SITEID) {
		$type='system';
		$instance=$COURSE->id;
		$name=$COURSE->fullname;
    }

    // Check if it's the course context
    if (isset($COURSE->id) && $COURSE->id != SITEID && strpos($PAGE->url->out(), '/course/view.php') !== false) {
		$type='course';
		$instance=$COURSE->id;
		$name=$COURSE->fullname;
    }

    // Check if it's the activity context
    if (isset($PAGE->cm->id)) {
		$type='activity';
		$instance=$PAGE->cm->id;
		$name=$PAGE->cm->name;
	 }
	 
	 $param=array();
	 $param['url']=$url;
	 $param['uri']=$uri;
	 $param['type']=$type;
	 $param['name']=$name;
	 $param['instance']=$instance;
	
	 return  $param;
 }
}