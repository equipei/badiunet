<?php
/**
 * Library of interface functions and constants for module badiunet
 *
 * @package     local_badiunet
 * @copyright   2024 Lino Vaz Moniz <linovazmoniz@gmail.com>
 * @license     http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

defined('MOODLE_INTERNAL') || die();
require_once("$CFG->dirroot/local/badiunet/lib/util.php");
require_once("$CFG->dirroot/local/badiunet/lib/response.php");
require_once("$CFG->dirroot/local/badiunet/chat/lib.php");
class local_badiunet_chat_service {
	private $param = null;
	 private $key;
	 private $util = null;
	 private $response = null;
	 
	function __construct() {
        $this->util=new local_badiunet_util();
        $this->response=new local_badiunet_response();
        $this->init();
		//global $DB;
		//$user = $DB->get_record('user', array('id'=>'2'));
		//$r=complete_user_login($user);
		
        $this->exec();
       
    }
	public function withoutLogin() {
			$chatlib=new local_badiunet_chat_lib();
			$isloggedin=$chatlib->isloggedin();
			if (!$isloggedin){$this->response->danied('badiu.ai.assistant.clientchat.useranonymous','Anonymous user in client system. Not logued in');}
			
		
	}

		
	
	public function init() {
           $param=file_get_contents('php://input');
		  
           $oparam=$this->util->getJson($param,true);
           $this->key=$this->util->getVlueOfArray($oparam,'_key');
           $this->setParam($oparam);
		 
    }
	public function exec() {
		$this->validateKey();
		$this->withoutLogin();
		$key=$this->key;
		$this->$key();
		
	}
	public function validateKey() {
		if(empty($this->key)){
			$this->response->danied('badiu.ai.assistant.clientchat.error.paramkeyrequired',"Param key should not be empty");
		}
		$startsWith = 'badiu.ai.assistant.clientchat.';
		if(!substr($this->key, 0, strlen($startsWith)) === $startsWith){
			$this->response->danied('badiu.ai.assistant.clientchat.error.paramkeyprefixnotvalid',"Prefix of key $this->key is not valid");
		}
		$this->key=  str_replace("badiu.ai.assistant.clientchat.","",$this->key);
		
		if (!method_exists($this, $this->key)) {
			$this->response->danied('badiu.ai.assistant.clientchat.error.paramkeynotvalid',"Key $this->key is not valid");
		}
	}
	public function sessionid() {
		$chatlib=new local_badiunet_chat_lib();
		$sessionid=$chatlib->get_sessionid();
		if(empty($sessionid)){
			$this->response->danied('badiu.ai.assistant.clientchat.error.sessionidisnull',"Without system sessionid");
		}
		
		$this->response->accept($sessionid);
	}
    function getParam() {
        return $this->param;
    }

    function setParam($param) {
        $this->param = $param;
    }
    function getKey() {
        return $this->key;
    }

    function setKey($key) {
        $this->key = $key;
    }
}