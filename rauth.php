<?php
require("../../config.php");
require_once("$CFG->dirroot/local/badiunet/lib/util.php");
require_once("$CFG->dirroot/local/badiunet/lib/netlib.php");
require_once("$CFG->dirroot/local/badiunet/lib/httpquerystring.php");
require_once("$CFG->dirroot/local/badiunet/fcservice/maccess.php");
require_once("$CFG->dirroot/local/badiunet/fcservice/factorycontent.php");
require_once("$CFG->dirroot/local/badiunet/lib/templetecript.php");

new local_badiunet_remote_auth();


class local_badiunet_remote_auth {
    private $requesttoken=null;
    private $username=null;
	private $userid=null;
    private $urltarget=null;
	private $paramiscripted=false;
    function __construct() {
          $this->exec();
    }
    
    function exec() {
        //validate text token 
       $this->validateToken();
        //verify token online
        $this->checkRemote();
        //auth user
        $this->execLogin();
    }
    
    function printContent($content) {
        global $PAGE,$OUTPUT;
         $context = context_system::instance();
        $PAGE->set_context($context);
        $PAGE->set_url('/local/badiunet/fcservice/index.php');
        $PAGE->set_pagelayout('standard');
        $PAGE->set_title('Badiu.Net');
        $PAGE->set_heading('Badiu.Net');
        
        echo $OUTPUT->header();
        echo $content;
        echo $OUTPUT->footer();
        exit;
    }
        
    function checkSecure(){
		$appkeyinstance = optional_param('_appservicekeyinstance',NULL,PARAM_TEXT);
		$netlib = new local_badiunet_netlib($appkeyinstance);
		
		//check system is enable
		if(!$this->netlib->getServiceenable()){$this->printContent('Plugin badiunet is not enabled');}
			 
		//chekc remote ip
		$remoteip=$this->util->getClientIp();
		
			 
		if(empty($remoteip) && $this->netlib->getEnviroment()=='level1'){$this->printContent('Remote ip is empty');}
		$isipallowed=$this->netlib->isIpAllowed($remoteip);
             
		if(!$isipallowed  && $this->netlib->getEnviroment()=='level1'){$this->printContent("Remote server IP does not have access permission");}
		else if(!$isipallowed  && $this->netlib->getEnviroment()=='level2'){$this->printContent("Remote server IP $remoteip does not have access permission");}
		
		$plugin=new local_badiunet_pluginconfig('local_badiunet');  
		$enableremoteauth=$plugin->getValue('enableremoteauth');
		if(!$enableremoteauth){$this->printContent('Remote auth in plugin badiunet is not enabled');}
	}
	function validateToken(){
        $msg=null;
        $this->requesttoken=required_param('_tokenauth',PARAM_TEXT); 
        $appkeyinstance = optional_param('_appservicekeyinstance',NULL,PARAM_TEXT);
        $this->requesttoken=base64_decode($this->requesttoken);
      
        $tcript=new local_badiunet_templatecript($appkeyinstance);
        $this->requesttoken=$tcript->decode($this->requesttoken); 
        $this->paramiscripted=$tcript->getIscripted();
        if(strlen($this->requesttoken)<120){$msg='Token remoto invalido';}
       if(!empty($msg)){$this->printContent($msg); }
       
    }
    
    function checkRemote(){
		$appkeyinstance = optional_param('_appservicekeyinstance',NULL,PARAM_TEXT);
        $util = new local_badiunet_util();
        $netlib = new local_badiunet_netlib($appkeyinstance);
        $tcript = new local_badiunet_templatecript();
		
		
		
		$sservicid=$netlib->getModuleInstance();
		$criptrequestoken=$this->requesttoken;
		if($this->paramiscripted){$criptrequestoken=$tcript->encode('s1',$this->requesttoken);}
		
		$reqperam=$this->makeParamToRemoteRequest();
        $httpqs=new local_badiunet_httpquerystring();
        $httpqs->add('_service','badiu.moodle.core.lib.remoteaccess');
        $httpqs->add('_function','remoteAccessAuth'); 
        $httpqs->add('_tokenauth',$criptrequestoken);
		$httpqs->add('_param',$reqperam);
        $data=$httpqs->getParam();
		$url = $netlib->getUrlService();
		
        $response= $util->request($url, $data);
	
        if(!$util->isResponseError($response)){
            $message=$util->getVlueOfArray($response, 'message');
            $status= $util->getVlueOfArray($response, 'status');
            $message=$tcript->decode($message);   
            if($status=='accept' && !empty($message)){ 
                $this->username=$util->getVlueOfArray($message, 'username');    
				$this->userid=$util->getVlueOfArray($message, 'userid');
                $this->urltarget=$util->getVlueOfArray($message, 'urltarget');
             }
          else if($status=='danied' ){ 
				$info=$util->getVlueOfArray($response, 'info');    
				$message=$util->getVlueOfArray($response, 'message');   
				$msg="Error code: $info <br /> Message: $message";
				$this->printContent($msg); 
		  } 
        }else{
            $this->printContent('Remote access failed. '.$response);    
        }
       
    } 
	 function makeParamToRemoteRequest() {
			 global $CFG;
			 $tcript = new local_badiunet_templatecript();
			$reqparam=array();
			$options=array();
        
			$reqparam['moodleurl'] = $CFG->wwwroot;
			$reqparam['moodleversiontxt'] = $CFG->release;
			$reqparam['moodleversionnumber'] = $CFG->version;
			$reqparam['moodledbtype'] = $CFG->dbtype;
			$reqparam['client']=array('browser'=>$_SERVER['HTTP_USER_AGENT'],'ip'=>$_SERVER["REMOTE_ADDR"],'moodleurl'=>$CFG->httpswwwroot);
			
			$reqparam=json_encode($reqparam);
			if($this->paramiscripted){$reqparam=$tcript->encode('s1',$reqparam);}
			return $reqparam;
		 }
	
     function execLogin(){
		 
		 $user =null;
		 global  $DB,$CFG;
		 if(!empty($this->userid)){
             $user = $DB->get_record('user', array('id'=>$this->userid));   
         }
		 
         if(empty($this->username) && empty($user)){
            $this->printContent('Username get by webservice is empty');   
         }
         
		 
        if(empty($user)){ $user = $DB->get_record('user', array('username'=>$this->username));}
         if(empty($user) || empty($user->id)){
              $this->printContent('Username get by webservice is not valid');
         }
         $resultath=complete_user_login($user);
         if(empty($resultath)){ $this->printContent('Login failed');}
         if(isset($_SESSION['_local_badiunet_maccess_session'])){unset($_SESSION['_local_badiunet_maccess_session']);}
         if(empty($this->urltarget)){$this->urltarget='';}
         redirect($CFG->httpswwwroot.$this->urltarget);
         exit;
     }
        
   
    function getRequesttoken() {
        return $this->requesttoken;
    }

    function getUsername() {
        return $this->username;
    }

    function getUrltarget() {
        return $this->urltarget;
    }

    function setRequesttoken($requesttoken) {
        $this->requesttoken = $requesttoken;
    }

    function setUsername($username) {
        $this->username = $username;
    }

    function setUrltarget($urltarget) {
        $this->urltarget = $urltarget;
    }

 // Getter for paramiscripted
    public function getParamiscripted() {
        return $this->paramiscripted;
    }

    // Setter for paramiscripted
    public function setParamiscripted($paramiscripted) {
        $this->paramiscripted = $paramiscripted;
    }
	
}

?>
