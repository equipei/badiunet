<?php
require_once("$CFG->dirroot/local/badiunet/lib/response.php");
require_once("$CFG->dirroot/local/badiunet/synchttp/paramlib.php");
require_once("$CFG->dirroot/local/badiunet/lib/netlib.php");
class local_badiunet_locallib  {

	 /**
     * @var object
     */
    private $paramlib;
 	
		 /**
     * @var array
     */
    private $error=null;
    function __construct() {
		$paramlib=new local_badiunet_paramlib();
		$this->paramlib=$paramlib;
                $this->paramlib->init();
	 
    }
	
	public function get_data() {
		$data=null;
		if($this->paramlib->getType()=='S'){
			$data=$this->get_row();
		}else if($this->paramlib->getType()=='M'){
			$data=$this->get_rowS();
		}else{
            //webservice
            $data= $this->execWebservice();
        }
		return $data;
    }
	
	public function get_row() {
			global $DB, $CFG;
			$sql=$this->paramlib->getQuery();
            $r=$DB->get_record_sql($sql);
	        return $r; 

    }
	public function get_rows() {
			global $DB, $CFG;
			$sql=$this->paramlib->getQuery();
			$x=$this->paramlib->getPagex();
			$y=$this->paramlib->getPagey();
			if($x>0){
				$x=$x*$y;
			}
			
            $r=$DB->get_records_sql($sql,null,$x,$y);
	        return $r; 

    }
	public function export() { 
          
             $response=new local_badiunet_response();
             $response->setCriptyidentify($this->paramlib->getTcript()->getIdentify());  
            
             try {
                $data =$this->get_data();
                $error=$this->getError();
                if(!empty($error) && isset($error['info']) && isset($error['message']) ){
                    $response->danied($error['info'],$error['message']);
                }
                $response->accept($data); 
             } catch (Exception $ex) {
                $response->danied('badiu.moodle.net.error.general',$ex);
            }
		
    }

    public function execWebservice() {
        global $CFG;
        require_once("$CFG->dirroot/local/badiunet/lib/webservicedata.php");
      
        $lwsd=new local_badiunet_webservicedata($this->paramlib->getParam());
         $response=new local_badiunet_response();
        $response->setCriptyidentify($this->paramlib->getTcript()->getIdentify());  
        $lwsd->setResponse($response);
        $responsews=$lwsd->exec();
        $this->setError($lwsd->getError()); 
        
        return $responsews;
    }
  public function getParamlib() {
        return $this->paramlib;
    }

    public function setParamlib($paramlib) {
        $this->paramlib = $paramlib;
    }

    public function getError() {
        return $this->error;
    }

    public function setError($error) {
        $this->error = $error;
    }
}
