<?php 
require_once("../../../config.php");
require_once("$CFG->dirroot/local/badiunet/lib/templetecript.php");
require_once("$CFG->dirroot/local/badiunet/lib/util.php");
require_once("$CFG->dirroot/local/badiunet/lib/response.php");
require_once("$CFG->dirroot/local/badiunet/lib/utildata.php");
 class local_badiunet_expfile {
    
    function __construct() {
          $this->exec();
    }
    
    function exec() {
        $response=new local_badiunet_response();
        $keyauth = required_param('_keyautdbcheckk',PARAM_TEXT);
        $valueauth = required_param('_valueautdbcheckv',PARAM_TEXT);
        $keyauthadtional = required_param('_keyautdbcheckkadtional',PARAM_TEXT);
        $valueauthadtional = required_param('_valueautdbcheckvadtional',PARAM_TEXT);
        $keygetfileconfig = required_param('_keyautdbcheckkfconfig',PARAM_TEXT);
        

       //check first token
        $fkvalue=$this->getValueByKey($keyauth);
        if($fkvalue!=$valueauth){
            $response->danied('badiu.moodle.net.error.downloadfileauthkey1failure','Autentication of key1 failure');
        }
        $this->deletKey($keyauth);

        //check second token
        $fkvalue=$this->getValueByKey($keyauthadtional);
        if($fkvalue!=$valueauthadtional){
            $response->danied('badiu.moodle.net.error.downloadfileauthkey2failure','Autentication of key2 failure');
        }
        $this->deletKey($keyauthadtional);

        //get file config
        $fconfig=$this->getValueByKey($keygetfileconfig);
        $this->deletKey($keygetfileconfig);

        if(empty($fconfig)){
            $response->danied('badiu.moodle.net.error.downloadfileconfempty','File config param is empty');
        }
        $utildata=new local_badiunet_utildata();
        $util=new local_badiunet_util();
        $fconfig=$util->getJson($fconfig);
        
        $contextid=$utildata->getVaueOfArray($fconfig,'contextid');
        $component=$utildata->getVaueOfArray($fconfig,'component');
        $filearea=$utildata->getVaueOfArray($fconfig,'filearea');
        $itemid=$utildata->getVaueOfArray($fconfig,'itemid');
        $filepath=$utildata->getVaueOfArray($fconfig,'filepath');
        $filename=$utildata->getVaueOfArray($fconfig,'filename');

        if(empty($contextid) || empty($component) || empty($filearea) || empty($filepath) || empty($filename)  ){
            $response->danied('badiu.moodle.net.error.downloadfileconfitemempty','File config has item empty');
        }

         //validate param file limit only file stored in local_badiunet
        if($component!="local_badiunet"){
            $response->danied('badiu.moodle.net.error.downloadfilecomponentnotlocalbadiunet','Try to get file outside compnent local_badiunet');
        }
       
         $fs = get_file_storage();
        $file = $fs->get_file($contextid, $component, $filearea, $itemid, $filepath, $filename);
        send_stored_file($file, 86400, 0, true, array());
    }

    /**
     * Get value stored in table local_badiunet_data
     * @return string
     */
    function getValueByKey($key) {
        global $CFG,$DB;
        $sql="SELECT value FROM {$CFG->prefix}local_badiunet_data WHERE name=:name ";
        $param=array('name'=>$key);
        $row = $DB->get_record_sql($sql,$param);
        if(empty($row)){return null;}
        
        return $row->value;
     }

     /**
     * Delete record by key in table  local_badiunet_data.
      */
    function deletKey($key) {
        return null;
        global $DB;
        $param=array('name'=>$key);
        $result=$DB->delete_records("local_badiunet_data", $param);
        return $result;
    }
}
new local_badiunet_expfile();
//http://d1.badiu21.com.br/moodle/3.5.1/local/badiunet/synchttp/expfile.php?_keyautdbcheckk=x&_valueautdbcheckv=125&_keyautdbcheckkadtional=4&_valueautdbcheckvadtional=126&_keyautdbcheckkfconfig=uuj
?>
