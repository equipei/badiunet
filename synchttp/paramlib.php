<?php
require_once("$CFG->dirroot/local/badiunet/lib/conn.php");
require_once("$CFG->dirroot/local/badiunet/lib/response.php");
require_once("$CFG->dirroot/local/badiunet/lib/response.php");
require_once("$CFG->dirroot/local/badiunet/lib/templetecript.php");
require_once("$CFG->dirroot/local/badiunet/lib/util.php");
class local_badiunet_paramlib  {
    
    private $query;
    private $type;
    private $pagex;
    private $pagey;
    private $token;
    private $key;

    private $netlib;
    private $tcript = null;
    private $util = null;
    private $param = null;

    function __construct() {
        $this->connlib=new local_badiunet_conn();
        $this->tcript=new local_badiunet_templatecript();
        $this->util=new local_badiunet_util();
        $this->netlib=new local_badiunet_netlib();
        $this->init();
        $this->chek();
       
    }
	
	public function init() {
           $param=file_get_contents('php://input');
          
           if($this->isParamCripted($param)){
               $param=$this->tcript->decode($param);
            }
          
           $oparam=$this->util->getJson($param,true);
           
           $this->query=$this->util->getVlueOfArray($oparam,'query');
           $this->type=$this->util->getVlueOfArray($oparam,'type');
           $this->pagex=$this->util->getVlueOfArray($oparam,'offset');
           $this->pagey=$this->util->getVlueOfArray($oparam,'limit');
           $this->token=$this->util->getVlueOfArray($oparam,'token');
           if(empty($this->token)){$this->token=$this->util->getVlueOfArray($oparam,'_token');}
           $this->key=$this->util->getVlueOfArray($oparam,'_key');
           $this->setParam($oparam);
           $this->changePrefixe(); 
        
    }
    public function chek() {
            
             $response=new local_badiunet_response();
			 
			 //check system is enable
			 if(!$this->netlib->getServiceenable()){$response->danied('badiu.moodle.net.service.desabled','Plugin badiunet is not enabled');}
			 
			 //chekc remote ip
			 $remoteip=$this->util->getClientIp();
		
			 
			 if(empty($remoteip) && $this->netlib->getEnviroment()=='level1'){$response->danied('badiu.moodle.net.error.server.remoteaddr.empty','Remote ip is empty');}
			 $isipallowed=$this->netlib->isIpAllowed($remoteip);
             
			 if(!$isipallowed  && $this->netlib->getEnviroment()=='level1'){$response->danied('badiu.moodle.net.error.server.remoteaddr.notallowed',"Remote server IP does not have access permission");}
			 else if(!$isipallowed  && $this->netlib->getEnviroment()=='level2'){$response->danied('badiu.moodle.net.error.server.remoteaddr.notallowed',"Remote server IP $remoteip does not have access permission");}
			 
			 if(empty($this->token)){ $response->danied('badiu.moodle.net.error.param.token.empty','Param token can not be null');}
             if(!$this->existToken($this->token)){ $response->danied('badiu.moodle.net.error.param.token.is.notvalid','Param token do not exist in database');}
             if(!empty($this->key)){return null;}
             if(empty($this->type)){ $response->danied('badiu.moodle.net.error.param.type.empty','Param type can not be null');}
             if($this->type!='S' && $this->type!='M'){ $response->danied('badiu.moodle.net.error.param.type.is.notvalid','Param type should has value S for exec single SQL result or M to exec SQL that returns list of rows');}
            
             if(empty($this->query)){ $response->danied('badiu.moodle.net.error.param.query.empty','Param query can not be null');}
             if(empty($this->pagex)){$this->pagex=0;}
             if(empty($this->pagey)){$this->pagey=10;}
        }
        
     public function changePrefixe() {
         if(!empty($this->query)){
             global $CFG;
            
             $this->query=str_replace("{_pfx}",$CFG->prefix,$this->query);
             
         }
    }
     function existToken($token) {
         $exist=false;
         $defaulttoken=$this->netlib->getToken();
         if($token==$defaulttoken){$exist=true;}
        return $exist;
     }
    function isParamCripted($txt) {
        
        $result=$this->util->getJson($txt,true);
        if(is_array($result)){return false;}
        return true;
    }
 public function getQuery() {
        return $this->query;
    }

    public function setQuery($query) {
        $this->query = $query;
    }

	 public function getType() {
        return $this->type;
    }

    public function setType($type) {
        $this->type = $type;
    }
	
	 public function getPagex() {
        return $this->pagex;
    }

    public function setPagex($pagex) {
        $this->pagex = $pagex;
    }
	
	 public function getPagey() {
        return $this->pagey;
    }

    public function setPagey($pagey) {
        $this->pagey = $pagey;
    }
    
    function getToken() {
        return $this->token;
    }

    function setToken($token) {
        $this->token = $token;
    }
 

    function getTcript() {
        return $this->tcript;
    }

    function setTcript($tcript) {
        $this->tcript = $tcript;
    }

    function getNetlib() {
        return $this->netlib;
    }

    function setNetlib($netlib) {
        $this->netlib = $netlib;
    }

    function getParam() {
        return $this->param;
    }

    function setParam($param) {
        $this->param = $param;
    }
    function getKey() {
        return $this->key;
    }

    function setKey($key) {
        $this->key = $key;
    }
}
