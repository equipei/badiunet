# Moodle Plugin: local_badiunet

## Plugin Name
BadiuNet

## Description
BadiuNet is a client system for the Badiu.Net Platform, providing a seamless connection between Moodle and the Badiu.Net reporting service. This plugin enables Moodle administrators to access advanced reporting features provided by the Badiu.Net platform.

## Plugin Type
Local

## Release Information
- **Version:** 1.4.3
- **Moodle Compatibility:** Moodle 2.7 or higher (tested with Moodle 3.x and 4.x versions)
- **Packaging Date:** 2024-11-30

## Features
- Acts as a client for the Badiu.Net platform.
- Facilitates advanced reporting integration with Moodle.
- Easy installation and activation process.
- Compatible with both local and online Moodle installations (note: offline installations are not supported).

## Author
- **Name:** Lino Vaz Moniz  
- **Email:** linovazmoniz@gmail.com  
- **Website:** [https://app.badiu.com.br/mreport](https://app.badiu.com.br/mreport)

---

## Installation Guide

### Prerequisites
- Moodle version 2.7 or higher (recommended for versions 3.x and 4.x).
- Administrator access to your Moodle site.

### Steps for Installation

1. **Download the Plugin**
   - Access the plugin repository: [BadiuNet Plugin Git Repository](https://bitbucket.org/equipei/badiunet/src/master/).
   - Download the plugin as a compressed file.

2. **Extract the Files**
   - Extract the downloaded file.
   - Locate the root folder containing the plugin files.
   - Rename the root folder to `badiunet`.

3. **Place the Plugin in the Moodle Directory**
   - Copy the `badiunet` folder to the Moodle `local` plugins directory:
     ```
     MOODLE_DIR_INSTALL/local/
     ```

4. **Install the Plugin**
   - Log in to Moodle as an administrator.
   - Navigate to **Site administration > Notifications**.
   - Moodle will detect the new plugin automatically.
   - Follow the on-screen instructions to complete the installation by clicking the "Update Moodle database" button.

5. **Activate the Service**
   - Visit the plugin page after installation:
     ```
     MOODLE_URL/local/badiunet
     ```
   - On the first access, activate the service to generate a secure key for communication with the Badiu.Net platform.
   - Once activated, the available reports will be displayed.

---

## Notes
- For local or intranet installations, the plugin may not function correctly due to the lack of an internet connection.
- Ensure that the Moodle server has access to the Badiu.Net platform for proper functionality.

## Support and Feedback
For comments, questions, or suggestions, please visit the [Badiu Community Forum](https://comunidade.badiu.com.br/mod/forum/discuss.php?d=458).

---

## License
This plugin is licensed under the terms of the GPL v3 license. For more information, refer to the license file included in the plugin package.
